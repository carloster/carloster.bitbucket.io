<?php 

// header('Content-Type: text/plain');
error_reporting(E_ALL);

$conn = oci_connect("elq_report", "reporting12", "192.168.200.3/ELQRUBP");

if (!$conn) {
    $m = oci_error();
    trigger_error(htmlentities($m['message']), E_USER_ERROR);
}
// formato de fecha año-mes-dia hora24:min:seg
$stid = oci_parse($conn, 'SELECT v_event.SITE_NAME,TO_CHAR(v_event.UPDATE_TIME, \'YYYY-MM-DD HH24:MI:SS\') as "FECHA", v_event.NUMERIC_VALUE FROM V_EVENT WHERE v_event.TAG_NAME = \'FLOW_VAL\' and v_event.SITE_TYPE = \'FLUMEGATE R\' ORDER BY \'TAG_ID\'');
oci_execute($stid, OCI_DESCRIBE_ONLY); // Use OCI_DESCRIBE_ONLY if not fetching rows


$ncols = oci_num_fields($stid);
echo "<table>";

for ($i = 1; $i <= $ncols; $i++) {
    $column_name  = oci_field_name($stid, $i);
    $column_type  = oci_field_type($stid, $i);
    $column_size  = oci_field_size($stid, $i);

    echo "<tr>";
    echo "<td>$column_name</td>";
    echo "<td>$column_type</td>";
    echo "<td>$column_size</td>";
    echo "</tr>\n";
}

echo "</table>\n";

// Outputs:
//    Name           Type       Length
//    NUMBER_COL    NUMBER        22
//    VARCHAR2_COL  VARCHAR2       1
//    CLOB_COL      CLOB        4000
//    DATE_COL      DATE           7

oci_free_statement($stid);
oci_close($conn);

// $currentLocale= setlocale(LC_ALL, 'es_CL.UTF-8');
// $tiempo = "03-Dic-2019 09:24:27.473 AM";
// $tiempon = strtotime($tiempo);

// var_dump($tiempon);


// $tiempo_arr = explode(' ', $tiempo);
// $fecha = $tiempo_arr[0];
// $hora = $tiempo_arr[1];
// $meridiano = $tiempo_arr[2];

// var_dump($fecha);
// var_dump($hora);
// var_dump($meridiano);


// $fecha_raw = date_create($tiempo);
// $fecha_formateada = strftime("%Y-%m-%d", $fecha);

// var_dump($fecha_formateada);
// return;


 ?>