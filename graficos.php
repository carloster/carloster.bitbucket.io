<?php 

// MODULO GRAFICOS HISTORICOS
// la lista se lee desde include/headers_graficos.php
// se carga la lista
// se generan los radiales a partir de la lista
// al hacer click en cada radial, se genera la vista mensual de cada valor y aparecen los otros botones selectores de graficos
// los graficos se cargan por un iframe (/include/ver_historia.php)
  include 'include/login/session.php';


if (isset($_GET['tabla'])) {
	$tabla = $_GET['tabla'];
} else{
	$tabla = 'cmd';
}


 ?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php 

	$incluye_highcharts=true;
require_once('include/header.php');
require_once('include/headers_graficos.php');


	?>
	<title>Plataforma de apoyo para la gestión hídrica del río elqui y sus afluentes</title>
</head>
<body>
  <!-- modal intro (trigger + html) -->
  <script>
    $(document).ready(function() {
      // $('#modalIntro').modal('show');
    });
  </script>


<!-- modalIntro -->
<div class="modal fade" id="modalIntro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Visualizador de gráficos históricos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<p>Este módulo es el visualizador de gráficos históricos.</p>
<p>En esta sección de la plataforma, usted puede ser capaz de ver los valores almacenados en la base de datos, procesados según los usos de mayor utilidad, como son los promedios y las probabilidades de excedencia. Por defecto, al abrir este módulo, estará elegido el valor de caudal en la ubicación de "Río Elqui en La Serena", que genera el gráfico de caudal medio mensual durante todo el período que se registraron datos.</p>
<p> Para comenzar a ver otras localidades, elija una desde el menú de la izquierda cuando haya cerrado esta ventana, y para cambiar de gráfico, elija uno de los botones que se encuentran al centro en la parte superior de la pantalla.</p>
<p>Para volver al menú principal, haga click en el botón "Inicio" que se encuentra en la parte superior izquierda de la pantalla, y para salir de la plataforma, haga click en el botón salir, al lado de su nombre.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- fin modal intro (trigger + html) -->

	<?php 
	$inicio = false;
	$usar_db = false;
	$banner="PROGESHI/Elqui - Gráficos Históricos";
		require_once('include/banner.php');
	?>

	<div class="container-fluid text-center">
		<div class="row">
			<div class="col-3 align-left">
			<form action="">
				<h3>Caudales</h3>
				<?php for ($i=0; $i < $cant_caudales; $i++): ?>
				<p class="text-sm-left"><input type="radio" name="grafico" data-clase="caudales_temporada" value="<?php echo $headers_caudales[$i]['nombre_db'] ?>" <?php echo $headers_caudales[$i]['nombre_db'] == 'rio_turbio_varillar' ? 'checked="checked"' : '' ?> ><?php echo $headers_caudales[$i]['formal'] ?><br></p>
				<?php endfor; ?>
				<hr>
				<h3>Precipitaciones</h3>
				<?php for ($i=0; $i < $cant_precip; $i++): ?>
				<p class="text-sm-left"><input type="radio" name="grafico" data-clase="precip_temporada" value="<?php echo $headers_precip[$i]['nombre_db'] ?>"><?php echo $headers_precip[$i]['formal'] ?><br></p>
				<?php endfor; ?>
				<hr>
				<h3>Situación Embalses</h3>
				<?php for ($i=0; $i < $cant_situacion; $i++): ?>
				<p class="text-sm-left"><input type="radio" name="grafico" data-clase="situacion" value="<?php echo $headers_situacion[$i]['nombre_db'] ?>"><?php echo $headers_situacion[$i]['formal'] ?><br></p>
				<?php endfor; ?>
			</form>
			</div>

			<div class="col-9">
				<div class="row">
					<div class="col-2">
						<button type="button" class="btn btn-cien btn-paleta btn-block subgrafico" id="mensual" data-dismiss="modal">Vista mensual</button>
					</div>
					<div class="col-2">
						<button type="button" class="btn btn-cien btn-paleta btn-block subgrafico" id="temporada" data-dismiss="modal">Vista temporada</button>
					</div>
					<div class="col-2">
						<button type="button" class="btn btn-cien btn-paleta btn-block subgrafico" id="excedencia" data-dismiss="modal">Probabilidad de excedencia volumen temporada</button>
					</div>
					<div class="col-2">
						<button type="button" class="btn btn-cien btn-paleta btn-block subgrafico" id="excedencia_mes" data-dismiss="modal">Caudal con probabilidad de excedencia del 85%</button>
					</div>
					<div class="col-2"></div>
				</div>
				<div class="container-fluid graficos m-0 px-0 py-3" style="height: 100%; width: 1200px; overflow: hidden; border: 0 none;">
					<iframe src="include/ver_historia.php" id="frame" frameborder="0" style="height: 100%; width: 1200px; overflow: hidden; border: 0 none;"></iframe>
				</div>
			</div>
		</div>

	</div>

<?php require_once('include/footer.php'); ?>
<script>
	let tipo_tabla
	let nombre_db
$(document).ready(function() {
// mostrar u ocultar botones...
	$('input[type=radio]').on('click',function(event) {
		nombre_db = $(this).val();
		tipo_tabla = $(this).data('clase');

		if (tipo_tabla === 'caudales_temporada') {
			$("#excedencia").css({"display" : "inherit"});
			$("#temporada").css({"display" : "inherit"});
			$("#temporada").text('Vista temporada');
			$("#excedencia_mes").css({"display" : "inherit"});
			$("#excedencia").text('Probabilidad de excedencia volumen temporada');
			$("#mensual").text('Vista mensual');
		} else if (tipo_tabla === 'precip_temporada') {
			$("#excedencia").css({"display" : "inherit"});
			$("#temporada").css({"display" : "inherit"});
			$("#excedencia_mes").css({"display" : "none"});
			$("#mensual").text('Vista mensual Precipitación acumulada');
			$("#temporada").text('Vista temporada Precipitación acumulada');
			$("#excedencia").text('Probabilidad de excedencia de Precipitación acumulada');
		}
		else {
			$("#temporada").css({"display" : "none"});
			$("#excedencia").css({"display" : "none"});
			$("#excedencia_mes").css({"display" : "none"});
			$("#mensual").text('Volumen acumulado al 31 de Agosto');
			// volumen al 31 de agosto
			// caudales de afluentes
		}


		$( '#frame' ).attr( 'src','include/ver_historia.php?nombre_db=' + nombre_db);
	});

	// cambiar de grafico
	$('.subgrafico').on('click', function(event) {
		// tipo_tabla = $(this).data('clase'); // toma el valor en el evento click ^^^^
		event.preventDefault();
		let grafico = $(this).attr('id');
		console.warn('include/ver_historia.php?nombre_db=' + nombre_db + '&subgrafico=' + grafico + '&tabla=' + tipo_tabla);
		$( '#frame' ).attr( 'src','include/ver_historia.php?nombre_db=' + nombre_db + '&subgrafico=' + grafico + '&tabla=' + tipo_tabla);

		

	});
});


</script>
<style>
	.btn-cien{
		height: 100%;
	}
</style>
</body>
</html>
