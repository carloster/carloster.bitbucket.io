<!DOCTYPE html>
<html lang="es">
<head>
	<?php 

	// esta es la pagina principal
error_reporting(~E_ALL);

session_destroy();

$acceso_index = true;
include 'include/header.php';

session_start();

	// por aqui se valida el usuario
if (isset($_POST['usuario'])) {
	include 'include/login/conexion_db_usuarios.php';


	if (isset($_POST['usuario']) && $_POST['usuario'] != '' ) { //caso: usuario entra por index.php y se logea
			$_SESSION['usuario'] = $_POST['usuario'];
	} else{ //caso: usuario bypassea index.php o no ingresa usuario
	    	$_SESSION['fail'] = 'no_user';
			$check_fail = true;
	}
	if(!isset($check_fail)) {
		try{
			$pdo = new PDO($dsn, $user, $pass, $options);

			$stmt = $pdo->prepare("SELECT * FROM usuarios WHERE usuario = ?");
		    $stmt->execute(array($_SESSION['usuario']));
		    $user_login = $stmt->fetch();
			$cant_usuarios = $stmt->rowCount();

			if ($cant_usuarios > 0) {
				$login_user = $user_login['usuario'];
		        if (password_verify($_POST['pass'], $user_login['pass'])) { // aqui es donde el usuario existe y es valido
					$_SESSION['nombre'] = $user_login['nombre'];
					// para verificar si es admin (condicion solamente puede ser editada directamente en la base de datos)
					if ($user_login['admin'] == true) {
						$_SESSION['admin'] = true;
					} else{
						$_SESSION['admin'] = false;
					}
					// redirigir al menu
					header('Location: menu.php');
					return;
		        } else {
		        	// error, actualizar pagina
		        	$_SESSION['fail'] = 'password';
					$check_fail = true;
		        }

			} else{
		    	$_SESSION['fail'] = 'unknown_user';
				$check_fail = true;
			}
		} catch (\PDOException $e){

			throw new \PDOException($e->getMessage(), (int)$e->getCode());
		}
	}
} elseif (isset($_SESSION['fail']) && $_SESSION['fail'] == '') {
		$check_fail = true;
	} else{
		$check_fail = false;
	}

	if (isset($check_fail)) {
		// tipos de error
		if (isset($_SESSION['fail']) && $_SESSION['fail'] != '') {
			switch ($_SESSION['fail']) {
				case 'no_user':
				$string_error = "ERROR 0: No se ingresó un usuario. <br><br>";
				break;

				case 'unknown_user':
				$string_error = "ERROR 1: El usuario ingresado es desconocido. <br><br>";
				break;

				case 'password':
				$string_error = "ERROR 2: Contraseña incorrecta. <br><br>";
				break;

				case 'error_session':
				$string_error = "ERROR 3: Error en la sesión, por favor reingrese con su usuario y contraseña. <br>";
				break;
			}
		} else{
			$string_error = "ERROR DEL SISTEMA: Llame al informático! 🤓<br>";
		}
		session_destroy();
	}

	?>
	<style>
		html {
		  height: 100%;
		  overflow: hidden;
		}
<?
// comentario: ok, por aca y en otros php hay css repartido en vez de estar en la hoja principal (style_sasi.scss precompilado), se debe a que varias clases y elementos comparten nombre con distintas propiedades en distintos archivos, AHORA, si, se puede cambiar y dejar todo en el scss, pero quedaria el despelote de scroll, asi que, EN MI HUMILDE OPINION, es mejor dejar las propiedades unicas de cada php autocontenidas.
?>
		*{
			font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
		}
		body{
			/*background: linear-gradient(to right, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('img/laguna_main1.jpg');*/
			/*background: url();*/
			background-repeat: no-repeat;
			background-size: cover;
			background-color: white;
			min-height: 100%;
			width: 100%;
			/*opacity: 0.6*/
		}
		.container-fluid{
			width: 100%;
			min-height: 100%;
		}

		.franja{
			background-color: #17555f;
		}

		.verde{
			color: #00104b;
		}

		.verde:hover{
			color: #17555f;

		}

	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Plataforma de apoyo para la gestión hídrica del río elqui y sus afluentes</title>
</head>
<body class="overlay animated fadeIn slower h-100">
<!-- modalError -->

<!-- modal de error -->
<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Error de acceso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

<?php echo $string_error; ?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<?php if (isset($_SESSION['fail']) && $_SESSION['fail'] != ''): ?>
			<script>
			$(document).ready(function() {
				// invocar modal de error
				$('#modalError').modal('show');
			});
			</script>
<?php endif; ?>
<!-- fin modal error (trigger + html) -->


<!-- splash -->
<div class="container-fluid h-100">
	<div class="row h-100">
		<div class="col-3 rgba-stylish-strong bg-white">
			<div class="row h-25">
				
			</div>
			<div class="row text-center">
				<div class="col">
					<img src="img/logos/jvre.png" class="img-jvre-index">
				</div>
			</div>
		</div>
		<div class="col">
			<div class="row h-50">
				<div class="col">
					<div class="row h-50">
						<div class="col">
					<img class="float-right m-2 img-logos-index" src="img/logos/th/banner_sm.png">
						</div>
					</div>
					<div class="row h-50">
						<div class="col">
					<h1 class="h1 animated fadeInRight slow titulo-splash-index mt-2">
						PLATAFORMA DE APOYO PARA LA GESTIÓN HÍDRICA DEL RÍO ELQUI Y SUS AFLUENTES
					</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row h-50 franja">
				<div class="col">
					<div class="row h-25"></div>
					<div class="row h-50">
					<div class="col-4">
					</div>
				    <div class="col-4">
						<input type="button" data-toggle="modal" data-target="#modalLogin" value="Iniciar Sesión" class="btn btn-xl btn-white btn-index btn-rounded border border-white btn-lg animated fadeInLeft slow verde">
				    </div>
				    <div class="col-4"></div>
						
					</div>
					<div class="row h-25"></div>
				</div>
			</div>
		</div>		
	</div>
</div>
<!-- fin splash -->

<!-- modalLogin -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Le damos la bienvenida a la Plataforma de Gestión Hídrica</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

<!-- Default form login -->
<form class="text-center border border-light p-5" action="index.php" method="post">

    <p class="h4 mb-4">Acceder</p>
    <!-- Email -->
    <input type="user" name="usuario" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="usuario">
    <!-- Password -->
    <input type="password" name="pass" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="contraseña">
    <!-- Sign in button -->
    <button class="btn btn-info btn-block my-4" type="submit">Ingresar</button>
</form>
<!-- Default form login -->

      </div>
    </div>
  </div>
</div>

<!-- fin modal login -->
<?php require_once('include/footer.php'); ?>
</body>
</html>
