<?php 


// modulo estimador de desmarque
// aprox. 1150 lineas
// es casi todo html y js
// los arboles de decisiones estan en /js/rop.js y /js/arboles.js
// cada input posee atributos "data" que los defini como tales para poder sobrecargar el html para facilitar la ejecucion de los scripts de jquery usando estos "data" como banderines (if data-tabla === pp_acum)
// en los input number, los "step" van en 0.01 para que admita decimales, y el minimo es 0 (no hay precipitacion negativa, o que llueva pa arriba en la 4ta region por lo menos)



  include 'include/login/session.php';
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <?php 
  $incluye_rop = true;
require_once('include/header.php');
  ?>
  <title>Regla operacional JVRE</title>

<script src="/js/dropbox.js"></script>
<!-- script para abrir el valor que genera el modelo weap y guarda en una carpeta de dropbox -->
<script>
    let temporada
    let anio_temporada
    let anio_1
    let anio_2
let bajarDbx = function (){

let dbx = new Dropbox.Dropbox({ fetch, accessToken: 'Z4-A5E_F0zAAAAAAAAAAWhR7PJRvgAxAxSLNR_7iA8I3mYmjYHVQcRgAR2YNHgmE' });


dbx.filesDownload({path: '/streamflow.csv'})
    .then(function (response) {
        let parent = document.getElementById('volumen_afluente');
        let tempnode;
        let blob = response.fileBlob;
        let reader = new FileReader();
        let temp = '';
        let resultado;
        reader.addEventListener("loadend", function() {
            temp = reader.result;
            temp = temp.split(",");
            resultado = parseFloat(temp[1]);
            resultado = resultado/1000000
            resultado = resultado.toFixed(2)
            // console.log(resultado); // will print out file content
            parent.value = resultado;
        });
        reader.readAsText(blob);
        // console.log(resultado);
        $("#vol_remoto").prop('disabled', false);
        $("#loader").empty();
    })
    .catch(function (error) {
        console.log(error)    })

}
</script>

</head>

<body>

  <?php 
  $inicio = false;
  $banner="PROGESHI/Elqui - Estimador de Desmarques";
  require_once('include/banner.php');
  // eeeeeeeeeeh..............hay un cron que saca la precip acumulada de la estacion de pisco elqui ceazamet, pero no va a correr todos los dias (y solo sirve entre el 1 de enero y 31 de agosto), asi que queda a discrecion del computin a cargo si corre o si el usuario se mete a la pagina de ceazamet: http://www.ceazamet.cl/index.php?pag=mod_sensor&e_cod=8&s_cod=55
  $val_ppElqui = file_get_contents('cron/webservices/ppElqui.txt');


  ?>
<script>

$(function () {
$('[data-toggle="popover"]').popover()
})
    // data-toggle="popover" title="máximo registrado en la base de datos: 100" id="precipitacion_laguna"


</script>
  <div class="container">
    <div class="row text-center">
      <div class="col p-1">
        <button class="btn btn-block btn-warning" id="arbolMet" data-toggle="modal" data-target="#modalMet">Arbol<br>Meteorologico</button>
      </div>
      <div class="col p-1">
        <button class="btn btn-block btn-primary" id="arbolHidro" data-toggle="modal" data-target="#modalHidro">Arbol<br>Hidrologico</button>
      </div>
      <div class="col p-1">
        <button class="btn btn-block btn-success" id="arbolHidroMet" data-toggle="modal" data-target="#modalHidroMet">Arbol<br>Hidrometeorologico</button>
      </div>
      <div class="col p-1">
        <button class="btn btn-block btn-cyan" id="modalReglaOp" data-toggle="modal" data-target="#reglaOp">Regla<br>Operacional</button>
      </div>
    </div>
<div class="row">
  
  <form action="decision_desmarque.php" method="GET" class="form col">
    <ul class="nav nav-tabs md-tabs nav-justified primary-color" role="tablist">
      <li class="nav-item waves-effect waves-light">
        <a class="nav-link active" data-toggle="tab" href="#met" role="tab">
          <i class="fas fa-cloud-sun pr-2"></i>
          Variables Meteorológicas
          <span class="border border-warning text-info float-right">Paso 1</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#hidro" role="tab">
          <i class="fas fa-water pr-2"></i>
          Variables Hidrológicas
          <span class="border border-warning text-primary float-right">Paso 2</span>
        </a>
      </li>
    </ul>

    <div class="tab-content" id="myTabContent">

  <hr>
  <div class="tab-pane fade show active" id="met" role="tabpanel" aria-labelledby="met-tab">

  <div class="form-row">

    <div class="col-md-3 mb-3">
       <i>Datos Embalse La Laguna</i><br>
    </div>
    <div class="col-md-4 mb-3">
      <label for="precipitacion_laguna">Precipitación equivalente acumulada <b>[mm]</b><br><i>Estación JVRE</i> <i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Precipitación equivalente (acumulada)" data-content="
        Es la sumatoria de la precipitación líquida en mm y la equivalencia en mm. de la nieve caída en la Estación La Laguna embalse de la JVRE; entre el 1 de enero de anio_1 y el 31 de agosto de anio_1.
        "></i> <br></label>
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.1" min="0" class="form-control" data-trigger="hover" data-content="Datos no disponibles." data-fullname="Precipitación equivalente acumulada [mm] en Estación JVRE" data-placement="top" title="Registro histórico de PROGESHI/Elqui" data-tabla="ppeq" data-class="pp" data-toggle="popover" id="precipitacion_laguna" name="precipitacion_laguna" placeholder="PP Total Equivalente" required>
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

    <div class="col-md-3 mb-3">
      <label for="altura_nieve_laguna">Altura de nieve <b>[cm]</b><br><i>Estación JVRE</i><i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Altura de nieve (acumulada)" data-content="
        Es la sumatoria de la altura de nieve caída en cm, en la Estación La Laguna embalse de la JVRE; entre el 1 de enero de anio_1 y el 31 de agosto de anio_1."></i><br></label>
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="1" min="0" class="form-control" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-fullname="Altura de nieve [cm] en Estación JVRE" data-placement="top" title="Registro histórico de PROGESHI/Elqui" data-class="nieve" data-tabla="alt_nieve" id="altura_nieve_laguna" name="altura_nieve_laguna" placeholder="Altura nieve estación JVRE" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

  </div>
<hr>
  <div class="form-row">
    <div class="col-md-2 mb-3">
      <i>Datos Precipitación Acumulada<br>de la temporada <span class="span_temporada">2019/20</span> <b>[mm]</b></i>
    </div>
    <div class="col-md-3">
      <label for="precipitacion_cochiguaz">Estación Cochiguaz (DGA)<i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Precipitación acumulada" data-content="
        Es la sumatoria de lluvia registrada en mm, en la estación Cochiguaz de la DGA; entre el 1 de enero de anio_1 y el 31 de agosto de anio_1."></i></label>
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" id="precipitacion_cochiguaz" name="precipitacion_cochiguaz" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-placement="top" title="Registro histórico de PROGESHI/Elqui" data-tabla="pp_acum" data-fullname="Datos Precipitación Acumulada de la temporada 2019/20 [mm] en Estación Cochiguaz (DGA)" data-estacion="cochiguaz" data-class="pp" placeholder="PP Total Cochiguaz DGA" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>
        <div class="col-md-3">
      <label for="precipitacion_ortiga">Estación La Ortiga (DGA)<i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Precipitación acumulada" data-content="
        Es la sumatoria de lluvia registrada en mm, en la estación La Ortiga de la DGA; entre el 1 de enero de anio_1 y el 31 de agosto de anio_1."></i></label>
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-class="pp" data-placement="top" title="Registro histórico de PROGESHI/Elqui" data-tabla="pp_acum" data-fullname="Datos Precipitación Acumulada de la temporada 2019/20 [mm] en Estación La Ortiga (DGA)" data-estacion="la_ortiga" id="precipitacion_ortiga" name="precipitacion_ortiga" placeholder="PP Total La Ortiga DGA" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>
        <div class="col-md-4 primary-color">
      <label for="precipElqui"><span class="text-white">Estación Pisco Elqui (CEAZA)</span><i class="far fa-question-circle text-white fa-2x" data-trigger="hover" data-toggle="popover" title="Precipitación acumulada" data-content="
      Es la sumatoria de lluvia registrada en mm, en la estación Pisco Elqui del CEAZA; entre el 1 de enero de anio_1 y el 31 de agosto de anio_1.
      "></i> <i class="fas fa-2x fa-exclamation-triangle" data-trigger="hover" data-toggle="popover" title="Recomendación" data-content="
      Esta medición se utiliza cuando no existen datos de las estaciones Cochiguaz y/o La Ortiga.
      "></i> </label>
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.1" min="0" data-class="pp" class="form-control" id="precipElqui" name="precipElqui" data-toggle="popover" data-trigger="hover" data-fullname="Datos Precipitación Acumulada de la temporada 2019/20 [mm] en Estación Pisco Elqui (CEAZA)" data-content="Datos no disponibles." data-placement="top" title="Registro histórico de PROGESHI/Elqui" data-tabla="ppElqui" placeholder="PP Total Pisco Elqui CEAZA">
        <script>

          $("#precipElqui").change(function(event) {
// plan b, por si no hay datos de ortiga y cochi, se usa pisco elqui para interpolar datos
            let pp = parseFloat($(this).val());
            let ppOrtigaRaw = ((1.28*pp)+4.42);
            let ppCochiRaw = ((1.01*pp)+2.78);
            ppOrtiga = ppOrtigaRaw.toFixed(2);
            ppCochi = ppCochiRaw.toFixed(2);
            $("#precipitacion_cochiguaz").val(ppCochi);
            $("#precipitacion_ortiga").val(ppOrtiga);
          });
        </script>
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

  </div>

</div>
  <div class="tab-pane fade" id="hidro" role="tabpanel" aria-labelledby="hidro-tab">

  <div class="form-row">
    
    <div class="col-md-3 mb-3">
      <i>Modelo WEAP Río La Laguna</i><br>
      <i>en Entrada Embalse La Laguna</i><br>
    </div>
 
    <div class="col-md-2 mb-3">
    </div>
   
    <div class="col-md-6 mb-3">
      <label for="volumen_afluente">Volumen temporada afluente embalse La Laguna <b>[Millones de m<sup>3</sup>]</b><i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Volumen Afluente Total de la temporada" data-content="
        Es la sumatoria del volumen mensual en Mmᶟ, afluente al embalse La Laguna, proyectado por el modelo WEAP; entre los meses de septiembre de anio_1 y agosto de anio_2."></i></label>
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.001" min="0" data-class="vol_laguna" class="form-control" id="volumen_afluente" name="volumen_afluente" data-toggle="popover" data-trigger="hover" data-fullname="Volumen temporada afluente embalse La Laguna [Millones de m3]" data-content="Datos no disponibles." data-placement="top" title="Registro histórico de PROGESHI/Elqui" data-tabla="vtemporada"  placeholder="Volumen Total Temporada" required="required"> <br>
      Obtener dato del Modelo Hidrológico <button id="vol_remoto">Obtener</button><span id="loader"></span>
<script>
  $(document).ready(function() {
    $("#vol_remoto").on('click', function(e) {
      // animacion pal dropbox..
      event.preventDefault();
    $("#loader").append('Por favor, espere...<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>');
    $("#vol_remoto").attr('disabled', 'disabled');

    bajarDbx();
    });
  });
</script>
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

    <div class="col-md-2 mb-3">
    </div>
  </div>

<hr>

    <div class="form-row">
      <div class="col-md-4 mb-3">
        <b>Caudal Medio Mensual [m<sup>3</sup>/s]</b><i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Caudal Medio Mensual" data-content="
        Es el promedio de la cantidad de agua que se mueve en un mes.
        "></i><br><i>Temporada Actual</i>
      </div>

      <div class="col-md-4 mb-3">
        <b>Mayo</b>
      </div>

      <div class="col-md-4 mb-3">
        <b>Agosto</b>
      </div>
    </div>

  <div class="form-row">
    
    <div class="col-md-4 mb-3">
      <i>Río Elqui en Algarrobal</i><i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Caudal Medio Mensual" data-content="Es el caudal medio mensual en mᶟ/s de la estación río Elqui en Algarrobal de la DGA, correspondiente a los meses de mayo de anio_1 y agosto de anio_1.
        "></i>
    </div>
    
    <div class="col-md-4 mb-3">
      <!-- <label for="rea_mayo">Mayo</label> -->
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" data-class="caudal" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-placement="top" data-fullname="Caudal Medio Mensual [m3/s] en Río Elqui en Algarrobal Mayo" title="Registro histórico de PROGESHI/Elqui" data-tabla="cmm" data-mes="5" data-rio="rea" id="rea_mayo" name="rea_mayo" placeholder="CMM Algarrobal Mayo" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

    <div class="col-md-4 mb-3">
      <!-- <label for="rea_agosto">Agosto</label> -->
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" data-class="caudal" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-placement="top" data-fullname="Caudal Medio Mensual [m3/s] en Río Elqui en Algarrobal Agosto" title="Registro histórico de PROGESHI/Elqui" data-tabla="cmm" data-mes="8" data-rio="rea" id="rea_agosto" name="rea_agosto"  placeholder="CMM Algarrobal Agosto" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

  </div>

  <div class="form-row">

    <div class="col-md-4 mb-3">
      <i>Río La Laguna en entrada Embalse La Laguna</i><i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Caudal Medio Mensual" data-content="Es el caudal medio mensual en mᶟ/s de la estación río La Laguna en entrada embalse La Laguna de la JVRE, correspondiente a los meses de mayo de anio_1 y agosto de anio_1."></i>
    </div>

    <div class="col-md-4 mb-3">
      <!-- <label for="rle_mayo">Mayo</label> -->
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-placement="top" title="Registro histórico de PROGESHI/Elqui" data-class="caudal" data-tabla="laguna" data-mes="5" data-fullname="Caudal Medio Mensual [m3/s] en Río La Laguna en entrada Embalse La Laguna Mayo" data-rio="rllee" id="rle_mayo" name="rle_mayo" placeholder="CMM Entrada La Laguna Mayo" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

    <div class="col-md-4 mb-3">
      <!-- <label for="rle_agosto">Agosto</label> -->
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-placement="top" data-fullname="Caudal Medio Mensual [m3/s] en Río La Laguna en entrada Embalse La Laguna Agosto" data-class="caudal" title="Registro histórico de PROGESHI/Elqui" data-tabla="laguna" data-mes="8" data-rio="rllee" id="rle_agosto" name="rle_agosto" placeholder="CMM Entrada La Laguna Agosto" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

  </div>
  <div class="form-row">

    <div class="col-md-4 mb-3">
      <i>Río Turbio en Varillar</i><i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Caudal Medio Mensual" data-content="Es el caudal medio mensual en mᶟ/s de la estación río Turbio en Varillar, correspondiente a los meses de mayo de anio_1 y agosto de anio_1."></i>
    </div>

    <div class="col-md-4 mb-3">
      <!-- <label for="rtv_mayo">Mayo</label> -->
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-placement="top" data-class="caudal" title="Registro histórico de PROGESHI/Elqui" data-tabla="cmm" data-mes="5" data-rio="rtv" id="rtv_mayo" name="rtv_mayo" data-fullname="Caudal Medio Mensual [m3/s] en Río Turbio en Varillar Mayo" placeholder="CMM Rio Turbio Mayo" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

    <div class="col-md-4 mb-3">
      <!-- <label for="rtv_agosto">Agosto</label> -->
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-placement="top" data-class="caudal" data-fullname="Caudal Medio Mensual [m3/s] en Río Turbio en Varillar Agosto" title="Registro histórico de PROGESHI/Elqui" data-tabla="cmm" data-mes="8" data-rio="rtv" id="rtv_agosto" name="rtv_agosto" placeholder="CMM Rio Turbio Agosto" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

  </div>

  <div class="form-row">

    <div class="col-md-4 mb-3">
      <i>Río Claro en Rivadavia</i><i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Caudal Medio Mensual" data-content="Es el caudal medio mensual en mᶟ/s de la estación río Claro en Rivadavia, correspondiente a los meses de mayo de anio_1 y agosto de anio_1."></i>
    </div>

    <div class="col-md-4 mb-3">
      <!-- <label for="rcr_mayo">Mayo</label> -->
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-placement="top" data-class="caudal" title="Registro histórico de PROGESHI/Elqui" data-tabla="cmm" data-mes="5" data-fullname="Caudal Medio Mensual [m3/s] en Río Claro en Rivadavia Mayo" data-rio="rcr" id="rcr_mayo" name="rcr_mayo" placeholder="CMM Rio Claro Mayo" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

    <div class="col-md-4 mb-3">
      <!-- <label for="rcr_agosto">Agosto</label> -->
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" class="form-control" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-placement="top" data-fullname="Caudal Medio Mensual [m3/s] en Río Claro en Rivadavia Agosto" title="Registro histórico de PROGESHI/Elqui" data-tabla="cmm" data-class="caudal" data-mes="8" data-rio="rcr" id="rcr_agosto" name="rcr_agosto" placeholder="CMM Rio Claro Agosto" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

  </div>

  <!-- <div class="form-row col-md"> -->
    <hr>
  <!-- </div> -->
  <div class="form-row">

    <div class="col-md-4 mb-3">
      <b>Volumen de Embalses <u>al 31 de agosto</u><br><b>[Millones de m<sup>3</sup>]</b></b><i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Volumen de embalses" data-content="
        Es la cantidad de agua que existe dentro de un embalse al día 31 de agosto de anio_1."></i>
    </div>

    <div class="col-md-4 mb-3">
      <label for="volumen_laguna">Embalse La Laguna<i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Volumen de embalse La Laguna" data-content="Es el volumen almacenado en el embalse La Laguna en Mmᶟ al día 31 de agosto de anio_1, según datos de la JVRE."></i><i class="fas fa-2x fa-exclamation-triangle" id="avisoLaguna" data-trigger="hover" data-toggle="popover" title="Aviso" data-content="El embalse posee una capacidad máxima de 40 [Millones de m3]."></i> </label>
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" max="41" class="form-control" id="volumen_laguna" name="volumen_laguna" data-toggle="popover" data-fullname="Volumen Embalse La Laguna al 31 de Agosto" data-trigger="hover" data-content="Datos no disponibles." data-placement="bottom" title="Registro histórico de PROGESHI/Elqui" data-class="vol_laguna" data-tabla="laguna_31_agosto" placeholder="Volumen La Laguna" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

    <div class="col-md-4 mb-3">
      <label for="volumen_puclaro">Embalse Puclaro<i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Volumen de embalse Puclaro" data-content="Es el volumen almacenado en el embalse Puclaro en Mmᶟ al día 31 de agosto de anio_1, según datos de la JVRE."></i><i class="fas fa-2x fa-exclamation-triangle" id="avisoPuclaro" data-trigger="hover" data-toggle="popover" title="Aviso" data-content="El embalse posee una capacidad máxima de 210 [Millones de m3]."></i> </label>
      <input type="number" oninvalid="this.setCustomValidity('Por favor ingrese un número igual o mayor a 0.')" oninput="setCustomValidity('')" lang="en" pattern="[0-9]+(\.[0-9]{0,2})?%?" step="0.01" min="0" max="211" class="form-control" id="volumen_puclaro" name="volumen_puclaro" data-toggle="popover" data-trigger="hover" data-content="Datos no disponibles." data-fullname="Volumen Embalse Puclaro al 31 de Agosto" data-placement="bottom" title="Registro histórico de PROGESHI/Elqui" data-tabla="puclaro_31_agosto" data-class="vol_puclaro" placeholder="Volumen Puclaro" required="required">
      <div class="invalid-feedback">
        Por favor, ingrese los datos.
      </div>
    </div>

  </div>
</div>
<input type="hidden" name="temporada" id="temporada">
</div>
<!-- <div class="calculadora"> -->
  <input type="submit" role="button" name="enviar" value="enviar" id="enviar">
  </form>
</div>

</div>
<hr>
<div class="container-fluid h-25">
  <!-- graficos procedurales -->
      <div class="row text-center justify-content-center h-100" style="min-height: 100%;">
      <div class="col" style="min-height: 100%;">
        <iframe src="include/calcular_caudales.php?tabla=ppeq&punto=69.8" id="frame" style="height: 100%; width: 1200px; overflow: hidden; border: 0 none;"></iframe>
      </div>
    </div>
</div>
<!-- Modal resultados -->
<div class="modal fade" id="desmarques" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true">

  <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
  <div class="modal-dialog  modal-dialog-scrollable modal-xl modal-dialog-centered" role="document">


    <div class="modal-content">
      <div class="modal-header texto-paleta">
        <h5 class="modal-title" id="exampleModalLongTitle">Resultados</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="show" id="hay_dm">
          <div class="row">
            <div class="col text-center">
              <h2>Volumen temporada <span class="span_temporada">2019/20</span> proyectado</h2>
              <hr>
            </div>
          </div>
          <div class="row">
            <div class="col text-center"><span class="vol_mod_hidro">Modelo Río La Laguna</span></div>
            <div class="col text-center"><span class="vol_met">Arbol Meteorológico</span></div>
            <div class="col text-center"><span class="vol_hidro">Arbol Hidrológico</span></div>
            <div class="col text-center"><span class="vol_hidromet">Arbol Hidrometeorológico</span></div>
          </div>
          <div class="row mt-2">
            <div class="col text-center"><span class="highlight-paleta"><span class="dato_Vt1"></span> <b>[Mm<sup>3</sup>]</b></span></div>
            <div class="col text-center"><span class="highlight-paleta"><span class="dato_Vt2"></span> <b>[Mm<sup>3</sup>]</b></span></div>
            <div class="col text-center"><span class="highlight-paleta"><span class="dato_Vt3"></span> <b>[Mm<sup>3</sup>]</b></span></div>
            <div class="col text-center"><span class="highlight-paleta"><span class="dato_Vt4"></span> <b>[Mm<sup>3</sup>]</b></span></div>
          </div>
          <div class="row">
            <div class="col text-center">
              <hr>
              <h2>Desmarque propuesto para la temporada  <span class="span_temporada">2019/20</span></h2>
              <hr>
            </div>
          </div>
          <div class="row">
            <div class="col text-center">Modelo Río La Laguna</div>
            <div class="col text-center">Arbol Meteorológico</div>
            <div class="col text-center">Arbol Hidrológico</div>
            <div class="col text-center">Arbol Hidrometeorológico</div>
          </div>
          <div class="row mt-2">
            <div class="col text-center"><span class="resultados"><span class="dato_dm1"></span></span></div>
            <div class="col text-center"><span class="resultados"><span class="dato_dm2"></span></span></div>
            <div class="col text-center"><span class="resultados"><span class="dato_dm3"></span></span></div>
            <div class="col text-center"><span class="resultados"><span class="dato_dm4"></span></span></div>
          </div>
          <div class="row">
            <div class="col text-center">
              <hr>
              <h2>Volumen Embalse Puclaro proyectado al término de la temporada <span class="span_temporada">2019/20</span></h2>
              <hr>
            </div>
          </div>
          <div class="row">
            <div class="col text-center">Modelo Río La Laguna</div>
            <div class="col text-center">Arbol Meteorológico</div>
            <div class="col text-center">Arbol Hidrológico</div>
            <div class="col text-center">Arbol Hidrometeorológico</div>
          </div>
          <div class="row mt-2">
            <div class="col text-center"><span class="highlight-paleta"><span class="dato_vp1"></span> <b>[Mm<sup>3</sup>]</b></span></div>
            <div class="col text-center"><span class="highlight-paleta"><span class="dato_vp2"></span> <b>[Mm<sup>3</sup>]</b></span></div>
            <div class="col text-center"><span class="highlight-paleta"><span class="dato_vp3"></span> <b>[Mm<sup>3</sup>]</b></span></div>
            <div class="col text-center"><span class="highlight-paleta"><span class="dato_vp4"></span> <b>[Mm<sup>3</sup>]</b></span></div>
          </div>
        </div>
        <div class="hide" id="no_hay_dm">
          Error: Desmarques no se calcularon correctamente (¿rellenó todos los datos de forma correcta?)
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-paleta" data-dismiss="modal">Cerrar</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>

</div>
<!-- fin modal resultados -->
<script>

  $(document).ready(function() {

    // TODO: RELLENAR CON INPUTS VALIDOS DESDE DB
    // pero el mauricio pidio dejar los inputs vacios

// fondear el popover de los '?'
    $('.pop').popover().click(function () {
        setTimeout(function () {
            $('.pop').popover('hide');
        }, 2000);
    });

// calcular desmarque
    $('#enviar1').on('click', function(event) {
      let volEmbalses;
      volEmbalses = 0;
      let volumen_afluente = parseFloat($("#volumen_afluente").val());
      let altura_nieve_laguna = parseFloat($("#altura_nieve_laguna").val());
      let precipitacion_laguna = parseFloat($("#precipitacion_laguna").val());
      let precipitacion_cochiguaz = parseFloat($("#precipitacion_cochiguaz").val());
      let precipitacion_ortiga = parseFloat($("#precipitacion_ortiga").val());
      let rea_mayo = parseFloat($("#rea_mayo").val());
      let rea_agosto = parseFloat($("#rea_agosto").val());
      let rcr_mayo = parseFloat($("#rcr_mayo").val());
      let rcr_agosto = parseFloat($("#rcr_agosto").val());
      let rtv_mayo = parseFloat($("#rtv_mayo").val());
      let rtv_agosto = parseFloat($("#rtv_agosto").val());
      let rle_mayo = parseFloat($("#rle_mayo").val());
      let rle_agosto = parseFloat($("#rle_agosto").val());
      let volumen_laguna = parseFloat($("#volumen_laguna").val());
      let volumen_puclaro = parseFloat($("#volumen_puclaro").val());
      let SumaEmb = parseFloat($("#SumaEmb").val());

      function validarNumero (val) {
        let sujeto = parseInt(val,10)
        if (isNaN(sujeto)){
          return false
        } else if (sujeto === '') {
          return false
        } else {
          return true
        }
      }

      let arrayValores = new Array()
      let arrayNombres = new Array()
      let flagAbort = false
      $('input[type=number]').each(function(element,index, array){
          let esLegal = validarNumero($(this).val())
        let esCmet = $(this).attr('id')
        if (esLegal == false && esCmet !== 'precipElqui') {
          alert("por favor, ingrese un valor numérico positivo en:\n " + $(this).attr('data-fullname') )
          flagAbort = true
        }
          arrayNombres.push($(this).prop('id'))
          if ($(this).val().length > 0) {arrayValores.push($(this).val())}
      })
    })
})

// dale (cualquier) color al desmarque
  function estiloDesmarque(dm){
    var umbral, clase;
    dm = parseFloat(dm);
    umbral = 37.5;
    umbral = dm - umbral;

    clase = 'text-danger';

    if (umbral > 12.5) clase = 'text-success'; else
    if ((umbral > 7.5) && (umbral < 12.5)) clase = 'text-warning';

    return clase;
  }
</script>
  <script>
    $(document).ready(function() {
      // funcion que saca los maximos de la db
      $('input[type=number]').each( function(index, element) {
        let tabla = $(this).data("tabla");
        let $this = $(this)
        let string;
        // console.info(tabla);
        if (tabla === 'pp_acum') {
          let estacion = $(this).data("estacion");
          // console.info(estacion);
          // console.info(tabla);
          $.get('include/maximos.php', { tabla: tabla, estacion: estacion }, function(data) {
            let datos = data.split(',');
            let fecha_max = parseFloat(datos[0])
            let valor = parseFloat(datos[1])
            string = "máximo registrado en la base de datos: " + valor + " en " + fecha_max
          // console.log(
          //           $this.attr('data-content',string));
          });
            // console.warn($this)
        } 
        else if (tabla === 'cmm') {
          let mes = $(this).data("mes");
          let rio = $(this).data("rio");
        // console.info(mes);
        // console.info(rio);
          $.get('include/maximos.php', { tabla: tabla, rio: rio, mes: mes }, function(data) {
            let datos = data.split(',');
            let fecha_max = parseFloat(datos[0])
            let valor = parseFloat(datos[1])
            string = "máximo registrado en la base de datos: " + valor + " en " + fecha_max

          $this.attr('data-content',string);
            });
        // console.info(string);
        }
        else if (tabla.length == 0) {
          console.warn('no hay grafico asociado...');
        }
        else {
          $.get('include/maximos.php', { tabla: tabla }, function(data) {
            let datos = data.split(',');
            let fecha_max = parseFloat(datos[0])
            let valor = parseFloat(datos[1])
            string = "máximo registrado en la base de datos: " + valor + " en " + fecha_max
          // console.log(
          //           $this.attr('data-content',string));
          $this.attr('data-content',string);
          });
        // console.info(string);
        }
        // statements
      });
    });
      $('input[name=tabla]').on('focus', function(event) {
    
      let opcion = $(this).val();
      if (opcion === 'mayo') {
        tabla = 'mayo';
        titulo = "Caudal medio de los meses de mayo";
      } else if (opcion === 'agosto') {
        tabla = 'agosto';
        titulo = "Diferencia de caudales Agosto-Mayo y Promedio de caudal en temporadas Mayo~Agosto";
      }else if (opcion === 'total') {
        tabla = 'total';
        titulo = "Caudales promedio mensuales (registro histórico de PROGESHI/Elqui no agrupado)";
      } else{
        tabla = 'grupo';
        titulo = "Caudales promedio mensuales (registro histórico de PROGESHI/Elqui agrupado por mes)";
      }
      $( '#frame' ).attr( 'src','include/calcular_caudales.php?tabla=' + tabla);
    });

// funcion generar graficos al enfocar la cajita
      $('input[type=number]').focus(function(event) {
        let tabla = $(this).data("tabla");
        let punto = $(this).val();
        if (tabla === 'pp_acum') {
          let estacion = $(this).data("estacion");
          $( '#frame' ).attr( 'src','include/calcular_caudales.php?tabla=' + tabla + '&estacion=' + estacion + '&punto=' + punto);
        } 
        else if (tabla === 'cmm') {
          let mes = $(this).data("mes");
          let rio = $(this).data("rio");
          $( '#frame' ).attr( 'src','include/calcular_caudales.php?tabla=' + tabla + '&rio=' + rio + '&mes=' + mes + '&punto=' + punto);
        }
        else if (tabla.length == 0) {
          console.warn('no hay grafico asociado...');
        }
        else {
          $( '#frame' ).attr( 'src','include/calcular_caudales.php?tabla=' + tabla + '&punto=' + punto);
        }
      });

      // funcion regenerar el grafico al agregar valor
      $('input[type=number]').on('input',function(event) {
        let tabla = $(this).data("tabla");
        let punto = $(this).val();
        if (tabla === 'pp_acum') {
          let estacion = $(this).data("estacion");
          $( '#frame' ).attr( 'src','include/calcular_caudales.php?tabla=' + tabla + '&estacion=' + estacion + '&punto=' + punto);
        } 
        else if (tabla === 'cmm') {
          let mes = $(this).data("mes");
          let rio = $(this).data("rio");
          $( '#frame' ).attr( 'src','include/calcular_caudales.php?tabla=' + tabla + '&rio=' + rio + '&mes=' + mes + '&punto=' + punto);
        }
        else if (tabla.length == 0) {
          console.warn('no hay grafico asociado...');
        }
        else {
          $( '#frame' ).attr( 'src','include/calcular_caudales.php?tabla=' + tabla + '&punto=' + punto);
        }
      });

// aviso capacidad maxima embalses
      $('input[type=number]').on('change',function(event) {
        let ident = $(this).attr('id');
        let punto = parseFloat($(this).val());
        // console.log(punto);
        if ((ident === 'volumen_puclaro') && (punto > 210.0)) {
          $("#avisoPuclaro").popover({
            delay:{
              "show": 100,
              "hide": 1000
            },
            trigger: "click"
          });
          $("#avisoPuclaro").popover('show');
          $(this).val(210);
        } else if (( ident === 'volumen_laguna') && (punto > 40.0)) {
          $("#avisoLaguna").popover('show');
          $("#avisoLaguna").popover({
            delay:{
              "show": 100,
              "hide": 1000
            },
            trigger: "click"
          });
          $(this).val(40);
        }
      });



  // modal intro (trigger + html)
    $(document).ready(function() {
      $('#modalIntro').modal('show');
      $("#modalIntro").on('hidden.bs.modal',function () {
        // modal de elegir temporada no se cierra a menos que se elija temporada
        $('#modalTemporada').modal({backdrop: 'static', keyboard: false})  
        $("#modalTemporada").modal('show');
      })
    });
  </script>


<!-- modalTemporada -->
<div class="modal fade" id="modalTemporada" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-scrollable" role="document">
    <div class="modal-content modal-content-temp">
      <div class="modal-header texto-paleta">
        <h5 class="modal-title" class="exampleModalLabel">Seleccionar temporada</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col">Seleccionar temporada:</div>
          <div class="col">
            <select name="temporadas" id="temporadas">
              <option value="2019/20">2019/20</option>
              <option value="2020/21">2020/21</option>
              <option value="2021/22">2021/22</option>
              <option value="2022/23">2022/23</option>
              <option value="2023/24">2023/24</option>
              <option value="2024/25">2024/25</option>
              <option value="2025/26">2025/26</option>
              <option value="2026/27">2026/27</option>
              <option value="2027/28">2027/28</option>
              <option value="2028/29">2028/29</option>
              <option value="2029/30">2029/30</option>
            </select>
          </div>

          <!-- <div class="col"><button type="button" class="close" data-dismiss="modal" aria-label="Close">Seleccionar</button></div> -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="selTemporada" value="Seleccionar"  class="btn btn-paleta" data-dismiss="modal">Seleccionar</button>
      </div>
    </div>
  </div>
</div>

          <script>

            $(document).ready(function() {

// funcion editar años mostrados segun temporada elegida
              $("#selTemporada").on('click', function(event) {
                //este evento cambia el texto de los años para otras temporadas (dicese, años) (es netamente cosmetico)
                event.preventDefault();
                let str_temp
                temporada = $("#temporadas").val();
                anio_temporada = parseInt(temporada.substring(0, 4));
                anio_1 = anio_temporada.toString(); //se puede concatenar strings y numeros en js, pero nos vamos a la segura..
                anio_2 = (anio_temporada + 1).toString()  ;
                $("span.span_temporada").html(temporada);
                $("#temporada").val(temporada);

                $("[data-content]").each(function(index, el) {

                  str_temp = $(this).data('content');
                  if (str_temp.includes('anio_1') || str_temp.includes('anio_2')) {
                    str_temp = str_temp.replace(/anio_1/g,anio_1);
                    str_temp = str_temp.replace(/anio_2/g,anio_2);
                    // console.log($(this).data('bs.popover'))

                    let popover = $(this).data('bs.popover')
                    popover.config.content = str_temp;
                    popover.element.dataset.content = str_temp; //me demore 2 horas para encontrar esta wea, al ojimetro
                    // ....sirve siempre hacer un consolelog del objeto
                    popover.setContent();
                  }

                });
              });
            });
          </script>

<!-- modalIntro -->
<div class="modal fade" id="modalIntro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content modal-content-intro">
      <div class="modal-header texto-paleta">
        <h5 class="modal-title" class="exampleModalLabel">Estimador de Desmarques</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<p>Este módulo es el estimador de desmarques.</p>

El estimador de desmarques es la herramienta principal de la plataforma de gestión hídrica, parte de la suite de productos generados dentro del marco del proyecto “Diseño de un sistema de gestión hídrica para la Junta de Vigilancia del Río Elqui y sus Afluentes, para mejorar la eficiencia en el uso del recurso hídrico bajo escenarios de cambio climático”. <br><br>
Esta herramienta es la implementación de una nueva metodología de cálculo de desmarques planteada por el laboratorio PROMMMRA de la facultad de agronomía de la universidad de la serena, que tiene el fin de determinar la cantidad ideal de agua que la junta de vigilancia del río elqui debiese repartir para la temporada deseada. <br><br>

Consejo: Al hacer click en <i class="fas fa-2x fa-exclamation-triangle" data-trigger="hover" data-toggle="popover" title="Recomendación" data-content="Este ícono va a entregar recomendaciones adicionales al dato que estén asociadas."></i> o en este ícono <i class="far fa-question-circle fa-2x"  data-trigger="click" data-toggle="popover" title="Ejemplo" data-content="aquí va a haber información adicional."></i> Podrá encontrar información adicional al dato que esté asociada.

<p>Para volver al menú principal, haga click en el botón "Inicio" que se encuentra en la parte superior izquierda de la pantalla, y para salir de la plataforma, haga click en el botón salir, al lado de su nombre.</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-paleta" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- modales de botones arboles -->
<!-- modalMet -->
<div class="modal fade" id="modalMet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content modal-content-arbol">
      <div class="modal-header texto-paleta">
        <h5 class="modal-title" class="exampleModalLabel">Árbol Meteorológico</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="imgArbol" src="img/arbol/met.png">




      </div>
      <div class="modal-footer">
        <a class="btn btn-primary" href="img/arbol/met.png" target="_blank" role="button">Abrir en nueva pestaña</a>
        <button type="button" class="btn btn-paleta" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- fin modal met (trigger + html) -->


<!-- modalHidro -->
<div class="modal fade" id="modalHidro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content modal-content-arbol">
      <div class="modal-header texto-paleta">
        <h5 class="modal-title" class="exampleModalLabel">Árbol Hidrológico</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <img class="imgArbol" src="img/arbol/hidro.png">


      </div>
      <div class="modal-footer">
        <a class="btn btn-primary" href="img/arbol/hidro.png" target="_blank" role="button">Abrir en nueva pestaña</a>
        <button type="button" class="btn btn-paleta" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



<!-- modalHidroMet -->
<div class="modal fade" id="modalHidroMet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content modal-content-arbol">
      <div class="modal-header texto-paleta">
        <h5 class="modal-title" class="exampleModalLabel">Árbol Hidrometeorológico</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="imgArbol" src="img/arbol/hidromet.png">

      </div>
      <div class="modal-footer">
        <a class="btn btn-primary" href="img/arbol/hidromet.png" target="_blank" role="button">Abrir en nueva pestaña (recomendado)</a>
        <button type="button" class="btn btn-paleta" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<!-- reglaOp -->
<div class="modal fade" id="reglaOp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content modal-content-arbol">
      <div class="modal-header texto-paleta">
        <h5 class="modal-title" class="exampleModalLabel">Regla Operacional</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="imgArbol" src="img/arbol/regla.png" alt="">
      </div>
      <div class="modal-footer">
        <a class="btn btn-primary" href="img/arbol/regla.png" target="_blank" role="button">Abrir en nueva pestaña</a>
        <button type="button" class="btn btn-paleta" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- <script src="js/less.min.js"></script> -->
<style>
  .h-25{
    height: 45% !important;
  }

  .modal-content{
    height: 700px;
  }

  .modal-content-intro{
    height: 600px;
  }

  .modal-content-temp{
    height: 250px;
  }

  .modal-content-arbol{
    height: 950px;
  }

  .resultados{
    font-size: 2rem;
    border: 1px solid #17555f;
    padding: 5px;
    margin-top: 4px;
    border-radius: 8px;
  }
.waves-effect {
    position: relative;
    cursor: pointer;
    overflow: hidden;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
  }

  .md-tabs .nav-item.open .nav-link, .md-tabs .nav-link.active {
    background-color: rgba(0,0,0,.2);
    color: #aaa;
    transition: all 1s;
    border-radius: .25rem;
}.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #fff;
    background-color: #17555f !important;
    border-color: #dee2e6 #dee2e6 #fff;
}
.md-tabs .nav-link {
    color: #fff;
    background-color: #aaa;
    transition: all .4s;
    border: 0;
        border-top-color: currentcolor;
        border-right-color: currentcolor;
        border-bottom-color: currentcolor;
        border-left-color: currentcolor;
}
.nav-tabs .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
}
.nav-link {
    display: block;
    padding: .5rem 1rem;
}
a {
    cursor: pointer;
    text-decoration: none;
    color: #007bff;
    transition: all .2s ease-in-out;
}
a {
    color: #007bff;
    text-decoration: none;
    background-color: transparent;
}

.fa-exclamation-triangle{
  color: #FFB82B;
}

.fa-question-circle{
  color: #17555f;
  /*background-color: #FFB82B;*/
}

.info-color{
  background-color: #17555f;
  color: white;
}

.btn-primary{
  /*background-color: #17555f !important;*/
  color: white;
}

.primary-color{
  background-color: #17555f !important;
  color: white;
}

.imgArbol{
  width: 100%;
}

.text-center{
  text-align: center !important;
  -moz-text-align: -moz-center !important;
  -webkit-text-align: -webkit;
}


</style>
</body>
</html>
