<?php
  include 'include/login/session.php';


  // modulo monitor de graficos
  // un tema probar esta cuestioncita....
  // la bd de la junta es oracle 12c, por lo que, si migras este codigo a tu tarro, y quieres conectarte a la bd de la junta, necesitas instalar todos los paquetes de oracle 12c (sdk, sqlplus, etc etcetc)
  // si migras la bd, el timestamp es un tema, por lo que se pueden gastar horas en convertir ts para probar (ojo ahi)
  // se leen los datos de las compuertas en /data/csv/getCompuertas.php (linea 153 aprox.)
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php 

    $incluye_highcharts=true;
require_once('include/header.php');
// require_once('data/leer_db.php');
    ?>
    <title>Plataforma de apoyo para la gestión hídrica del río elqui y sus afluentes</title>
    <style>
        #frame{
            text-align: center;
        }
        .link-popup{
            color: #007bff !important;
        }
        span{
          color: black;
        }
    </style>
</head>
<body>
  <!-- modal intro (trigger + html) -->
  <script>
    $(document).ready(function() {
      // $('#modalIntro').modal('show');
    });
  </script>


<!-- modalIntro -->
<div class="modal fade" id="modalIntro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Visor de compuertas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

<p>Este módulo es el monitor de compuertas.</p>
<p>En esta sección de la plataforma, usted será capaz de visualizar el último valor registrado mediante telemetría de las compuertas conectadas al sistema SCADA de la Junta de Vigilancia del Río Elqui y sus Afluentes, mediante un gráfico de barras.</p>
<p>Al hacer click en el nombre de la compuerta, podrá desplegarse un gráfico que ilustra los valores promedio del caudal en los últimos 3 días.</p>
<p>Para volver al menú principal, haga click en el botón "Inicio" que se encuentra en la parte superior izquierda de la pantalla, y para salir de la plataforma, haga click en el botón salir, al lado de su nombre.</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- fin modal intro (trigger + html) -->

<!-- modalGrafico -->
<div class="modal fade" id="modalGrafico" tabindex="-1" role="dialog" aria-labelledby="etiquetaModal"
  aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header texto-item">
        <h5 class="modal-title" id="etiquetaModal">Serie histórica</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="position: relative; top: 50%; transform: translateY(-60%); text-align: center;">
        <!-- <iframe src="/ignore/getCompuertas_old.php" id="frame" style="height: 600px; width: 1200px; overflow: hidden; border: 0 none;"></iframe> -->
        <iframe src="data:image/gif;base64,R0lGODlhFAAUAPMIAIeHhz8/P1dXVycnJ8/Pz7e3t5+fn29vb////wAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFBwAIACwAAAAAFAAUAEAEUxDJSatFxtwaggWAdIyHJAhXoRYSQUhDPGx0TbmujahbXGWZWqdDAYEsp5NupLPkdDwE7oXwWVasimzWrAE1tKFHErQRK8eL8mMUlRBJVI307uoiACH5BAUHAAgALAEAAQASABIAAAROEMkpS6E4W5upMdUmEQT2feFIltMJYivbvhnZ3R0A4NMwIDodz+cL7nDEn5CH8DGZh8MtEMBEoxkqlXKVIgQCibbK9YLBYvLtHH5K0J0IACH5BAUHAAgALAEAAQASABIAAAROEMkpjaE4W5spANUmFQX2feFIltMJYivbvhnZ3d1x4BNBIDodz+cL7nDEn5CH8DGZAsFtMMBEoxkqlXKVIgIBibbK9YLBYvLtHH5K0J0IACH5BAUHAAgALAEAAQASABIAAAROEMkpAaA4W5vpOdUmGQb2feFIltMJYivbvhnZ3Z0g4FNRIDodz+cL7nDEn5CH8DGZgcCNQMBEoxkqlXKVIgYDibbK9YLBYvLtHH5K0J0IACH5BAUHAAgALAEAAQASABIAAAROEMkpz6E4W5upENUmAQD2feFIltMJYivbvhnZ3V0Q4JNhIDodz+cL7nDEn5CH8DGZg8GtUMBEoxkqlXKVIggEibbK9YLBYvLtHH5K0J0IACH5BAUHAAgALAEAAQASABIAAAROEMkphaA4W5tpCNUmHQf2feFIltMJYivbvhnZ3d0w4BMAIDodz+cL7nDEn5CH8DGZBMLNYMBEoxkqlXKVIgoFibbK9YLBYvLtHH5K0J0IACH5BAUHAAgALAEAAQASABIAAAROEMkpQ6A4W5vpGNUmCQL2feFIltMJYivbvhnZ3R1B4NNxIDodz+cL7nDEn5CH8DGZhcINAMBEoxkqlXKVIgwGibbK9YLBYvLtHH5K0J0IACH5BAUHAAcALAEAAQASABIAAANCeLo6wzA6FxkhbaoQ4L3ZxnXLh0EjWZ4RV71VUcCLIByyTNt2PsO8m452sBGJBsNxkUwuD03lAQBASqnUJ7aq5UYSADs=" id="frame" style="height: 400px; width: 1000px; overflow: hidden; border: 0 none;"></iframe>
        <!-- <div id="container_modal" style="width:600px; height:800px;"></div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-paleta" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- fin modal grafico (trigger + html) -->

    <?php 
    $inicio = false;
    $banner="PROGESHI/Elqui - Monitor de Compuertas";
    require_once('include/banner.php');
    ?>

    <div class="container text-center">
        <div class="row">
            <div class="col">
                <div id="container" style="width:auto; height:800px;"></div>
            </div>
        </div>
    </div>

<?php require_once('include/footer.php'); ?>
<script>
let nombre_formal
let nombreid
let nombre_plain
let compuerta

Highcharts.setOptions({
    lang: {
    thousandsSep: ','
  }
})

let main = Highcharts.chart('container', {
    chart: {
        type: 'bar',
        height: 800
    },
    title: {
        text: 'Monitoreo de Caudales'
    },
    legend: {
        enabled: false
    },
    subtitle: {
        text: 'Información actualizada cada 10 minutos'
    },
  //     tooltip: {
  //   style: {
  //     pointerEvents: 'all'
  //   },
  //   useHTML: true,
  //   pointFormatter() {
  //       console.log(point.x)
  //     return "<a href='https://highcharts.com' target='_blank'>Click Me</a>"
  //   }
  // },
    data: {
        // csvURL: window.location.origin + '/data/csv/getCompuertas.php',
        csvURL: window.location.origin + '/ignore/getCompuertas_old.php',
        itemDelimiter: ',',
        lineDelimiter: '\n',
        // decimalPoint: '.',
        // beforeParse: function(csv){
        //  console.log(csv);
        // },
        enablePolling: true //SETEAR COMO TRUE PARA LEER EN TIEMPO REAL, AJUSTAR DATAREFRESHDATE PARA CADA ACTUALIZACION
        ,dataRefreshRate: 600
    },
    plotOptions: {
        bar: {
            colorByPoint: true
        },
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.y:,.2f} [Lt/s]'
            }
        }
    },
    xAxis: {
        title:{
            text: 'Compuerta'
        },
        type: 'category',
            labels: {
                enabled: true,
                formatter: function() {
                    nombre_formal = reformatearNombre(this.value)
                    nombre_plain = nombreClase(this.value)
                    nombreid = nombre_plain.replace(" ", "_")
                    nombredb = nombreClase(this.value)
                    // console.log(typeof this.value)
                    // this.value es el nombre de la compuerta, el resto de "this" son propiedades de objeto que no son necesarias
                    // return '<span>' + nombre_formal + '</span>'; // cambiar por visualizador historico de cada grafico
                    return '<span title="haga click para ver la serie temporal de la compuerta."><a class="link-popup modal_historia" data-nombre_plain="' + nombre_plain + '" id="' + nombreid + '" data-toggle="modal" data-target="#modalGrafico">' + nombre_formal + '</a></span>'; // cambiar por visualizador historico de cada grafico
                },
                useHTML: true,
                style: {
                    color: 'white'
            }
        }
    }
    ,yAxis: {
        title:{
            text: 'Caudal [Lt/s]'
        }
    }
});

let plain_temp

$(document).ready(function() {

    $('#modalGrafico').on('shown.bs.modal', function (e) {
        e.preventDefault();
        plain_temp = $(e.relatedTarget).data('nombre_plain') //esto toma el elemento que generó el formatter de highcharts (y que incluye el id para seleccionar la compuerta correcta)
        $('#frame').attr( 'src','/data/csv/getRubiconHistorico.php?nombre=' + plain_temp);
        // console.warn(plain_temp);
    });

    $('#modalGrafico').on('hidden.bs.modal', function (e) {
        $('#frame').attr( 'src','/img/lightbox/preloader.gif');
        // console.warn(plain_temp);
    });

});

function reformatearNombre(string) {
    nombre = string.toLowerCase()
    nombre = nombre.charAt(0).toUpperCase() + nombre.slice(1)
    return nombre
}
function nombreClase(string) {
    nombre = string.replace(/\s/g,"%20")
    return nombre
}
</script>
</body>
</html>
