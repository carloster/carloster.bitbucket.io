<?php 

// esto deberia correrse una vez al mes dentro de la temporada (ene-ago), o por ahi, no es trascendental dejarlo como cron (a menos que estemos en una temporada con superavit de lluvias..)
// 
// 
$temporada = date('Y');
$string = 'http://www.ceazamet.cl/ws/pop_ws.php?fn=GetSerieSensor&p_cod=ceazamet&s_cod=55&fecha_inicio=' . $temporada . '-01-01&fecha_fin=' . $temporada . '-09-01&user=anon@host.com&interv=mes&encabezado=0';
echo $string;
echo "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br>\n";
echo "\nGuardando precipitacion acumulada en pisco elqui....<br>\n";
echo "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<br>\n";


$datos_pisco_raw = file_get_contents($string);

$datos_pisco_array = explode("\n", $datos_pisco_raw);

// 0: sensor
// 1: fecha
// 2: min
// 3: prom
// 4: max
// 5: data_pc

$acumulado = 0;
$cant_lineas = count($datos_pisco_array);

for ($i=0; $i < $cant_lineas; $i++) { 
	$linea_temp = explode(",", $datos_pisco_array[$i]);
	// print_r($linea_temp);
	foreach ($linea_temp as $key => $val) {
		if ($key == 3) {
			$acumulado = $acumulado + $val;
		}
	}
}

file_put_contents('webservices/ppElqui.txt', $acumulado);
echo $acumulado;

?>