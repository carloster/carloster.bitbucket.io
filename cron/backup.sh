#!/bin/bash

anio=$(date +%Y)
mes=$(date +%m)
dia=$(date +%d)
PGPASSWORD=aguasuelo

# dumpear db plataforma
# formato nombre: db_añomesdia.zip
pg_dump -U postgres datos > datos/datos.bak
pg_dump -U postgres datos > datos/datos.sql
pg_dump -U postgres datos > datos/datos.tar

pg_dump -U postgres usuarios > datos/usuarios.bak
pg_dump -U postgres usuarios > datos/usuarios.sql
pg_dump -U postgres usuarios > datos/usuarios.tar

# zipear carpeta plataforma
# formato nombre: geshi_añomesdia.zip
zip -r /var/www/html/geshi_"$anio""$mes""$dia".zip /var/www/html/carloster.bitbucket.io/

# conexion al host (protocolo, formato, etc dependen de CICULS)
# scp los archivos al host
# fin