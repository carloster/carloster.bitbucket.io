
 <!DOCTYPE html>
<html class="h-100" lang="es">
<head>
	<?php 
	// menu principal
	require_once('include/header.php');
  include 'include/login/session.php';
	// $ver_modal = 'no';
	if(!isset($_SESSION["modal_menu_visto"])) {
		$_SESSION["modal_menu_visto"] = 'si';
		$ver_modal = 'si';
	} elseif (isset($_SESSION["modal_menu_visto"])) {
		$ver_modal = 'no';
		$_SESSION["modal_menu_visto"] = 'no';
	}
	?>
	<title>Plataforma de apoyo para la gestión hídrica del río elqui y sus afluentes</title>
</head>
<body class="h-100">
  <!-- modal intro (trigger + html) -->
  <script>
    $(document).ready(function() {

    	<?php //if($ver_modal == 'si') : ?>
	      $('#modalIntro').modal('show');
		<?php //endif; ?>
    });
  </script>


<!-- modalIntro -->
<!-- es necesario un modal intro, falta texto -->
<div class="modal fade" id="modalIntro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Le damos la bienvenida a la Plataforma de Gestión Hídrica</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="container">
      		<div class="row">
      			<div class="col text-center">
					<p class="margen_satanico">Bienvenido a la plataforma de gestión hídrica. En este menú principal, puede acceder a los siguientes 3 módulos:</p>
      			</div>
      		</div>
      		<div class="row container-menu">
				<div class="col-4"></div>

      			<div class="col-4 text-center-moz bg-white rounded border m-1">
      				<div class="prebox">
						<img src="/img/menu/calc.png" class="img_modal">
						<h3>Estimador de Desmarques</h3>
						<p>El módulo "Estimador de Desmarques" permite el cálculo del desmarque mediante la regla operacional, que determina la cantidad de agua que cada accionista tiene derecho a consumo durante la temporada siguiente.</p>
      				</div>
      			</div>
				<div class="col-4"></div>

  			</div>
      				
      			<div class="row container-menu">
      				<div class="col-1"></div>
      				<div class="col-4 text-center-moz bg-white rounded border m-1">
      					<div class="prebox">
							<img src="/img/menu/hist.png" class="img_modal">
							<h3>Gráficos<br>Históricos</h3>
							<p>El módulo "Gráficos Históricos" procesa valores de precipitación y caudal, para entregar promedios mensuales o de temporadas, probabilidades de excedencia, y registros de volumenes embalsados del Embalse Puclaro y Embalse La Laguna.</p>
      					</div>
      				</div>
      				
      				<div class="col-2"></div>
      				<div class="col-4 text-center-moz bg-white rounded border m-1">
  						<div class="prebox">
							<img src="/img/menu/dish.png" class="img_modal">
							<h3>Monitor de Compuertas Rubicon</h3>
							<p>El módulo "Monitor de Compuertas" entrega mediante un gráfico de barras, la última lectura del caudal de las compuertas con telemetría disponible, asociado al promedio de los últimos días.</p>
		      			</div>
      				</div>
  					<div class="col-1"></div>
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- fin modal intro (trigger + html) -->

	<?php 
	$inicio = true;
  include 'include/login/session.php';
	$banner="Plataforma de Gestión Hídrica (PROGESHI/Elqui) - JVRE";
	require_once('include/banner.php');
	?>
<!-- Jumbotron  style="background-color: #3B8AEB;"-->
	<div class="container container-menu text-center rgba-stylish-strong h-100">
	<div class="card card-image h-20" style="background-color: white;">
	<div class="row h-100 m-0">
		<div class="col-3 rgba-stylish-strong bg-white h-100">
			<div class="row text-center">
				<div class="col">
					<img src="img/logos/jvre.png" class="img-jvre-menu">
				</div>
			</div>
		</div>
		<div class="col h-100">
			<div class="row h-25">
				<div class="col">
					<img class="float-right mb-2 banner" src="img/logos/th/banner_sm.png">
				</div>
			</div>
			<div class="row h-75 franja">
				<div class="col">
					<h1 class="h1 animated fadeInDown slow titulo_menu m-2">
						PLATAFORMA DE APOYO PARA LA GESTIÓN HÍDRICA DEL RÍO ELQUI Y SUS AFLUENTES
					</h1>
				</div>
			</div>
		</div>		
	</div>
	</div>
<!-- Jumbotron -->
		<div class="row items_main text-center h-40">
<!-- si es admin, cambiar el esquema de los modulos.... -->

<?php if ($es_admin == true): ?>
			<div class="col-1"></div>

			<div class="col-4">
<?php else: ?>
			<div class="col-3"></div>

			<div class="col-6">
<?php endif; ?>
				<a href="desmarque.php" class="custom-card">
					<div class="card align-items-center">
						<img src="img/menu/calc.png" class="card-img-top imagen_main" alt="Estimador de Desmarque">
						<div class="card-body">
							<h5 class="card-title">Estimador de Desmarques</h5>
							<!-- <p class="card-text mb-3">Implementación Online de la Regla Operacional de desmarque para la JVRE.</p> -->
							<?php 
							// <a href="desmarque.php" class="btn btn-paleta btn-rounded">Ingresar</a>
							 ?>
							<a href="desmarque.php" class="btn btn-paleta btn-rounded">Ingresar</a>
						</div>
					</div>
				</a>
			</div>
<?php if ($es_admin == true): ?>
			<div class="col-2"></div>

			<div class="col-4">
				<a href="admin/index.php" class="custom-card">
					<div class="card align-items-center">
						<img src="img/menu/monitor.png" class="card-img-top imagen_main" alt="Panel de administracion de datos">
						<div class="card-body">
							<h5 class="card-title">Administración</h5>
							<p class="card-text mb-3">Administrador de la base de datos <br> del sistema.</p>
							<a href="admin/index.php" class="btn btn-paleta btn-rounded">Ingresar</a>

						</div>
					</div>
				</a>
			</div>
<?php endif; ?>

<?php if ($es_admin == true): ?>
			<div class="col-1"></div>
<?php else: ?>
			<div class="col-3"></div>
<?php endif; ?>

		</div>

		<div class="row items_main text-center h-39">
			<div class="col-1"></div>

			<div class="col-4">
				<?php 
				 ?>
				<a href="graficos.php" class="custom-card">
					<div class="card align-items-center">
						<img src="img/menu/hist.png" class="card-img-top imagen_main" alt="Gráficos Históricos">
						<div class="card-body">
							<h5 class="card-title">Gráficos Históricos</h5>
							<!-- <p class="card-text">Visualización de precipitaciones, caudales y situaciones de los embalses a lo largo del tiempo.</p> -->
							<a href="graficos.php" class="btn btn-paleta btn-rounded">Ingresar</a>
							<?php 
							 ?>
						</div>
					</div>
					<?php 
					 ?>
				</a>
			</div>
			<div class="col-2"></div>

			<div class="col-4">
				<a href="monitor.php" class="custom-card">
					<div class="card align-items-center">
						<img src="img/menu/dish.png" class="card-img-top imagen_main" alt="Monitor de Compuertas">
						<div class="card-body">
							<h5 class="card-title">Monitor de Compuertas</h5>
							<!-- <p class="card-text">Monitoreo en tiempo real y lecturas históricas de caudal de Compuertas RUBICON.</p> -->
							<a href="monitor.php" class="btn btn-paleta btn-rounded">Ingresar</a>
							<!-- <a href="informe.php" class="btn btn-paleta btn-rounded">Ver informe de volumen</a> -->
						</div>
					</div>
					<?php 
					 ?>
				</a>
			</div>


			<div class="col-1"></div>

		</div>
	</div>

	<!-- fin modulos -->

<?php require_once('include/footer.php'); ?>
<style>
	body{
		background:#EFEFEF url(img/back-gradient.jpg) repeat-x left top;
	}
	.jumbotron{
		min-width: 100%;
		padding: 0;
	}

	.offline{
		color: rgba(255,255,255,0.5);
		background-color: rgba(255,255,255,0.5);
	}

	.logos{
		height: 100%;
		width: auto;
	}

	.banner{
		width: 30%;
	}
	.btn-paleta{
		background-color: #17555f;
		color: white;
		text-shadow: 1px 1px #000;
	}

	.btn-paleta:hover{
		color: #ffc000;
	}

	.img_modal{
		width: 70px;
		height: 70px;
		padding-top: 10px;
	}

	p{
		text-align: justify;
	}

	.modal-body{
		text-align: center;
	}
</style>
</body>
</html>
