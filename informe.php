<?php
  include 'include/login/session.php';
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php 

    $incluye_highcharts=true;
require_once('include/header.php');



// require_once('data/leer_db.php');



// header('Content-Type: text/plain');
error_reporting(E_ALL);

// require_once('../../include/conexion_db_rubicon.php');

$conn = oci_connect("elq_report", "reporting12", "192.168.200.3/ELQRUBP");

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

// SITE_NAME
// NUMERIC_VALUE
// UPDATE_TIME

$anio_ahora = date('Y');
$mes_ahora = 9;
// $mes_ahora = date('m');
// $dia_ahora = date('d');

// $ahora = time();
// $tres_dias_atras = time() - (72 * 3600);

$string_csv = "<div class='row text-center'><div class='col'><span>Compuerta</span></div><div class='col'><span>Volumen [Millones de m<sup>3</sup>]</span></div></div>";
// $string_compuertas = '';
// $string_valores = '';


$currentLocale= setlocale(LC_ALL, 'es_CL.utf8');
// $fecha = new DateTime();

$query = "SELECT max(NUMERIC_VALUE) as volumen, SITE_NAME as compuerta  FROM v_event_log where
tag_name='FLOW_ACU_TOTAL'
and extract(year from v_event_log.event_time) =" . $anio_ahora . "
and extract(month from v_event_log.event_time) = " . $mes_ahora . "
group by SITE_NAME
";

$handle = oci_parse($conn, $query);

oci_execute($handle);

while($row = oci_fetch_array($handle, OCI_DEFAULT)) {

// print_r($row);
    // $nombre = $row["SITE_NAME"];
    // $tiempo = $row['UPDATE_TIME'];
    // $fecha = DateTime::createFromFormat('d-M-Y G:i:s.u',$tiempo);
    // $fecha_formateada = strftime("%Y-%m-%d %H:%M:%S", $fecha->getTimestamp());
    // $fecha = $row["year"] . "-" . $row["month"] . "-" . $row["day"] . " " . $row["hour"] . ":00:00";
    $nombre_compuerta = $row["COMPUERTA"];
    $valor = $row["VOLUMEN"] / 1000000;
    $valor = number_format($valor,2,$dec_point = "," , $thousands_sep = ".");

    $string_csv .= "<div class='row text-center border border-primary'><div class='col'><span>" . $nombre_compuerta . "</span></div><div class='col'><span>" . $valor . " [Mm<sup>3</sup>]</span></div></div>";
    // $string_csv .= $fecha . "," . $valor  ."\n" ;

}

// $string_fechas = rtrim($string_compuertas,",");
// $string_valores = rtrim($string_valores,",");

// echo $string_fechas;
// echo $string_csv;

// print_r($datos_array);



    ?>
    <title>Plataforma de apoyo para la gestión hídrica del río elqui y sus afluentes</title>
    <style>
        .link-popup{
            color: #007bff !important;
        }
    </style>
</head>
<body>
  <!-- modal intro (trigger + html) -->
  <script>
    $(document).ready(function() {
      // $('#modalIntro').modal('show');
    });
  </script>


<!-- modalIntro -->
<div class="modal fade" id="modalIntro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Visor de compuertas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía un hidalgo de los de lanza en astillero, adarga antigua, rocín flaco y galgo corredor. <br>
 <br>
Una olla de algo más vaca que carnero, salpicón las más noches, duelos y quebrantos los sábados, lantejas los viernes, algún palomino de añadidura los domingos, consumían las tres partes de su hacienda. <br>
 <br>
El resto della concluían sayo de velarte, calzas de velludo para las fiestas, con sus pantuflos de lo mesmo, y los días de entresemana se honraba con su vellorí de lo más fino. <br>
 <br>
Tenía en su casa una ama que pasaba de los cuarenta, y una sobrina que no llegaba a los veinte, y un mozo de campo y plaza, que así ensillaba el rocín como tomaba la podadera. <br>
 <br>
Frisaba la edad de nuestro hidalgo con los cincuenta años; era de complexión recia, seco de carnes, enjuto de rostro, gran madrugador y amigo de la caza. <br>
 <br>
Quieren decir que tenía el sobrenombre de Quijada, o Quesada, que en esto hay alguna diferencia en los autores que deste caso escriben; aunque por conjeturas verosímiles se deja entender que se llamaba Quijana. <br>
 <br>
Pero esto importa poco a nuestro cuento: basta que en la narración dél no se salga un punto de la verdad. <br>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- fin modal intro (trigger + html) -->

<!-- modalGrafico -->
<div class="modal fade" id="modalGrafico" tabindex="-1" role="dialog" aria-labelledby="etiquetaModal"
  aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="etiquetaModal">Serie histórica</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- fin modal grafico (trigger + html) -->

    <?php 
    $inicio = false;
    $banner="PROGESHI/Elqui - Monitor de Compuertas";
    require_once('include/banner.php');
    ?>

    <div class="container text-center">
        <div class="row">
            <div class="col">
                <?php echo $string_csv; ?>
            </div>
        </div>
    </div>

<?php require_once('include/footer.php'); ?>

</body>
</html>