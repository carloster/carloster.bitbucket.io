<?php 
include '../include/login/session.php';
if ($es_admin == true):
include '../include/login/conexion_db_usuarios.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
require_once('../include/header.php');

$inicio = false;
$pag_admin = true;
$banner="PROGESHI/Elqui - Administración de usuarios";
require_once('../include/banner.php');


// echo $query;


if (isset($_GET['agregar'])) {
	$usuario_a_agregar = $_GET['agregar'];
	$query = "INSERT INTO usuarios (usuario,nombre,pass) values (:usuario,:nombre,crypt(:pass,gen_salt('bf',8)));";

	$usuario = [
		'usuario' => $_GET['nuevo_usuario'],
		'nombre' => $_GET['nuevo_nombre'],
		'pass' => $_GET['nuevo_pass']
	];

	try {
		$pdo = new PDO($dsn, $user, $pass, $options);
		$stmt3 = $pdo->prepare($query);
		$stmt3->execute($usuario);
		echo "<h3 class='green-text'>Usuario Agregado exitosamente..</h3>";
	} catch (\PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
}

if (isset($_GET['borrar'])) {
	$usuario_a_borrar = $_GET['borrar'];
	$query = "DELETE FROM usuarios WHERE usuario =:usuario;";

	try {
		$pdo = new PDO($dsn, $user, $pass, $options);
		$stmt2 = $pdo->prepare($query);
		$stmt2->bindValue(':usuario',$usuario_a_borrar);
		$stmt2->execute();
		echo "<h3 class='red-text'>Usuario borrado exitosamente..</h3>";
	} catch (\PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
}

try {
	$pdo = new PDO($dsn, $user, $pass, $options);
	$query = "SELECT usuario, nombre FROM usuarios ORDER BY usuario ASC;";
	$usuarios = array();
	$nombres = array();

	$stmt = $pdo->prepare($query);
	$stmt->execute();
	// devolver datos

	foreach ($stmt as $row) {
		array_push($usuarios, $row['usuario']);
		array_push($nombres, $row['nombre']);
		// foreach ($row as $val) {
		// 	echo $val;
		// }
	}
} catch (\PDOException $e) {
	throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

$cant_usuarios = count($usuarios); //va a ser la misma cantidad de usuarios que de nombres (en la db no pueden ser nulos)
// print_r($usuarios);
// print_r($nombres);





?>

<div class="container">
<div class="row h-20">
	<div class="col-1"></div>
	<div class="col-10">
		<div class="row my-3 text-center">
			<div class="col">Usuario</div>
			<div class="col">Nombre para mostrar</div>
			<div class="col">Editar usuario</div>
			<div class="col">Borrar usuario</div>
		</div>
	</div>
	<div class="col-1"></div>
</div>
<hr><hr>
	<div class="row text-center">
		<div class="col-1"></div>		
		<div id="filas" class="col-10">
			
<?php //mostrar usuarios ?>
<?php for($i = 0; $i < $cant_usuarios; $i++): ?>
	<div class="row">
		<div class="col">
			<p> <?php echo $usuarios[$i] ?></p>
		</div>
		<div class="col">
			<p><?php echo $nombres[$i] ?></p>
		</div>
		<div class="col">
			<a href="editar_usuario.php?usuario=<?php echo $usuarios[$i] ?>">Editar</a>
		</div>
		<div class="col">
			<a href="usuarios.php?borrar=<?php echo $usuarios[$i] ?>">Borrar</a>
		</div>
	</div>
	<hr>
<?php endfor; ?>
<div id="boton_fila">
	
	<button id="agregar_fila">Agregar Usuario</button>
</div>

		</div>
		<div class="col-1"></div>		
	</div>
<!-- asignar crud para cada usuario -->
<!-- agregar usuario con perfil admin? -->


</div>

<script>
	$(document).ready(function() {
		$("#agregar_fila").on('click', function(event) {
			event.preventDefault();
			let fila_header = "\
				<div class='row'>\
		<div class='col'>\
			<p> Usuario </p>\
		</div>\
		<div class='col'>\
			<p>Nombre para mostrar</p>\
		</div>\
		<div class='col'>\
			<p>Contraseña</p>\
		</div>\
		<div class='col'>\
		</div>\
	</div>\
		"
			let una_fila = "\
				<form action='/admin/usuarios.php' method='get'>\
				<div class='row'>\
		<div class='col'>\
			<p> <input type='text' name='nuevo_usuario'> </p>\
		</div>\
		<div class='col'>\
			<p><input type='text' name='nuevo_nombre'></p>\
		</div>\
		<div class='col'>\
			<p><input type='text' name='nuevo_pass'></p>\
		</div>\
		<div class='col'>\
			<input type='submit' value='Agregar' name='agregar'>\
		</div>\
	</div>\
				</form>\
		"
		$("#filas").append(fila_header);
		$("#filas").append(una_fila);
		$("#boton_fila").empty();

			/* Act on the event */
		});
	});
</script>

<?php else: //si no es_admin ?>
<p>Error 503 (acceso prohibido): usuario no es admin.</p>
<?php endif; //fin if es_admin ?>