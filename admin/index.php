<!DOCTYPE html>
<html lang="es">
<head>
<?php
include '../include/login/session.php';
require_once('../include/header.php');

?>
<div class="container container-menu rgba-stylish-strong text-center h-100">

    <?php 
    $inicio = false;
    $fondo = 'blanco';
    $banner="PROGESHI/Elqui - Panel de Administración del sistema";
    require_once('../include/banner.php');
if ($es_admin == true):

    ?>

	<div class="row items_main text-center h-40">
		<div class="col-1"></div>
		<div class="col-4">
			<a href="datos.php" class="custom-card">
				<div class="card align-items-center">
					<img src="/img/menu/calc.png" class="card-img-top imagen_main" alt="Mediciones de datos">
					<div class="card-body">
						<h5 class="card-title">Mediciones de datos</h5>
						<p class="card-text mb-3">Permite visualizar, editar, agregar y borrar mediciones de datos en la base de datos del sistema.</p>
						<a href="datos.php" class="btn btn-paleta btn-rounded">Abrir nueva pestaña</a>
					</div>
				</div>
			</a>
		</div>

		<div class="col-2"></div>
		<div class="col-4">
			<a href="usuarios.php" class="custom-card">
				<div class="card align-items-center">
					<img src="/img/menu/monitor.png" class="card-img-top imagen_main" alt="Administracion de usuarios">
					<div class="card-body">
						<h5 class="card-title">Usuarios</h5>
						<p class="card-text mb-3">Permite visualizar, editar, agregar y borrar usuarios de la plataforma en la base de datos del sistema.</p>
						<a href="usuarios.php" class="btn btn-paleta btn-rounded">Abrir nueva pestaña</a>
					</div>
				</div>
			</a>
		</div>
		<div class="col-1"></div>
	</div>
</div>






<?php else: //si no es_admin ?>
<p>Error 503 (acceso prohibido): usuario no es admin.</p>
<?php endif; //fin if es_admin ?>