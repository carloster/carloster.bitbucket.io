<?php 
include '../include/login/session.php';
if ($es_admin == true):
include '../include/login/conexion_db_usuarios.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
require_once('../include/header.php');

$inicio = false;
$pag_admin = false;
$sub_admin = true;
$banner="PROGESHI/Elqui - Administración de usuarios";
require_once('../include/banner.php');

if (isset($_GET['usuario'])) {
	$editar_usuario = $_GET['usuario'];
	$query = "SELECT usuario, nombre FROM usuarios WHERE usuario =:usuario ORDER BY usuario ASC;";

	try {
		$pdo = new PDO($dsn, $user, $pass, $options);
		$stmt = $pdo->prepare($query);
		$stmt->bindValue(':usuario',$editar_usuario);
		$stmt->execute();
		$usuario = $stmt->fetch(PDO::FETCH_ASSOC);
		$usuario_form = $usuario['usuario'];
		$nombre_form = $usuario['nombre'];
	} catch (\PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
} elseif (isset($_POST['submit'])) {
	try {
		$pdo = new PDO($dsn, $user, $pass, $options);
		if (isset($_POST['pass_editado']) && $_POST['pass_editado'] != '') {
			$usuario_editado = [
				"usuario" => $_POST['usuario_editado'],
				"nombre" => $_POST['nombre_editado'],
				"pass" => $_POST['pass_editado'],
				"usuario_antiguo" => $_POST['usuario_antiguo']
			];

		    $sql = "UPDATE usuarios
		            SET 
		              usuario = :usuario,
		              nombre = :nombre,
		              pass = crypt(:pass,gen_salt('bf',8))
		            WHERE usuario = :usuario_antiguo;";
		} else {
			$usuario_editado = [
				"usuario" => $_POST['usuario_editado'],
				"nombre" => $_POST['nombre_editado'],
				"usuario_antiguo" => $_POST['usuario_antiguo']
			];

		    $sql = "UPDATE usuarios
		            SET 
		              usuario = :usuario,
		              nombre = :nombre
		            WHERE usuario = :usuario_antiguo;";
		}
// insert into users(usuario,pass,nombre) values ('seba',crypt('seba',gen_salt('bf',8)),seba);

		$stmt = $pdo->prepare($sql);
		$stmt-> execute($usuario_editado);
		$usuario_form = $usuario_editado['usuario'];
		$nombre_form = $usuario_editado['nombre'];

		header('Location: /admin/usuarios.php');

	} catch(PDOException $e) {
		// echo $e->getCode();
		// echo "~~~~";
		echo "<h2 class='red-text'>Error: Ya existe un usuario '" . $_POST['usuario_editado'] . "', intente con otro nombre de usuario.</h2>";

		$usuario_form = $_POST['usuario_antiguo'];
		$nombre_form = $_POST['nombre_editado'];

		// throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
}

?>

<div class="container h-40">
	<div class="row h-20 py-3">
		<div class="col  align-pulento">Usuario</div>
		<div class="col  align-pulento">Nombre para mostrar</div>
		<div class="col  align-pulento">Contraseña (cambio es opcional)</div>
		<div class="col  align-pulento"></div>
	</div>
<hr>
<form action="/admin/editar_usuario.php" method="post">
	<div class="row align-pulento h-80">
		<div class="col">
			<input type="text" name="usuario_editado" value="<?php echo $usuario_form ?>">
		</div>
		<div class="col">
			<input type="text" name="nombre_editado" value="<?php echo $nombre_form ?>">
		</div>
		<div class="col">
			<input type="text" name="pass_editado" placeholder="cambiar contraseña?">
			<input type="hidden" name="usuario_antiguo" value="<?php echo $usuario_form ?>">
		</div>
<!-- 		<div class="col">
			es admin? <input type="checkbox" name="admin_editado">
		</div> -->
		<div class="col">
			<input type="submit" value="cambiar" name="submit">
		</div>
</form>
	</div>
<!-- asignar crud para cada usuario -->
<!-- agregar usuario con perfil admin? -->


</div>

<?php else: //si no es_admin ?>
<p>Error 503 (acceso prohibido): usuario no es admin.</p>
<?php endif; //fin if es_admin ?>