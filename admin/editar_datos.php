<?php 
include '../include/login/session.php';
if ($es_admin == true):
include '../include/conexion_db.php';
require_once('../data/funciones.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
require_once('../include/header.php');

// 'editar_dato.php?entrada=" . $id . "&tabla=" . $tabla . "'

$inicio = false;
$pag_admin = false;
$sub_admin = true;
$banner="PROGESHI/Elqui - Administración de usuarios";
require_once('../include/banner.php');

if (isset($_GET['entrada'])) {
	$entrada_fecha = $_GET['entrada']; //la entrada es la fecha, ya que la fecha es la llave primaria
	$tabla = $_GET['tabla'];
	$query = "SELECT * FROM " . $tabla . " WHERE fecha =:fecha;";
	// echo $query;

	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);
		$stmt = $pdo->prepare($query);
		$stmt->bindValue(':fecha',$entrada_fecha);
		$stmt->execute();
		$entrada = $stmt->fetch(PDO::FETCH_ASSOC);
	} catch (\PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}

	$cant_datos = count($entrada);
	$headers = array_keys($entrada);
	// print_r($headers);
} elseif (isset($_POST['cambiar'])) {
	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);

		$tabla = $_POST['tabla'];
		$entrada_antigua = $_POST['entrada_fecha'];


		// agregar de forma dinamica los nombres de las columnas 

		// print_r($_POST);
		$columnas = $_POST;
		if (($key = array_search('cambiar', $columnas)) !== false) {
			unset($columnas[$key]);
		}
		unset($columnas['tabla']);
		unset($columnas['entrada_fecha']);
		foreach ($columnas as $key => $value) {
			if ($value == '') {
				unset($columnas[$key]);
				# code...
			}
		}
		$headers = array_keys($columnas);
		$valores_check = array_values($columnas);
		$cant_datos_editar = count($headers);

		$sql = "UPDATE " . $tabla . "SET ";

		for ($i=0; $i < $cant_datos_editar; $i++) {
			if ($valores_check[$i] !== '' || $_POST["fecha"] !== $entrada_antigua) {
				$sql .= " " . $headers[$i] . " = :" . $headers[$i] . ",";
			} 
		}
		$sql = rtrim($sql,",");

		$sql .= " WHERE fecha = :fecha";

		echo $sql;
		print_r($columnas);
		$stmt = $pdo->prepare($sql);
		$stmt-> execute($columnas);
		header('Location: /admin/datos.php');
	} catch(PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
} else{
	echo "Error 400: para ingresar a esta página de edición de datos, debe elegir una entrada en la página datos.php.<br>\n";
	echo "<a href='/admin/datos.php'>Volver</a>";

}

?>
<div class="container-fluid h-40">
	<div class="row h-20 py-3">
		<?php for ($i=0; $i < $cant_datos; $i++) :?>
			<div class="col"><?php echo formatearHeader($headers[$i]) ?></div>
		<?php endfor; ?>
		<div class="col"></div>
	</div>
<hr>
<form action="/admin/editar_datos.php" method="post">
	<div class="row h-80">
		<?php for ($i=0; $i < $cant_datos; $i++) :?>
			<div class="col">
				<?php if ($i == 0): ?>
					<input class="align-pulento" type="text" name="<?php echo $headers[$i] ?>" value="<?php echo $entrada_fecha ?> ">
				<?php else: ?>
					<input class="align-pulento" type="number" step ="0.01" name="<?php echo $headers[$i] ?>">
				<?php endif ?>
			</div>
		<?php endfor; ?>
		<input type="hidden" name="tabla" value="<?php echo $tabla ?> ">
		<input type="hidden" name="entrada_fecha" value="<?php echo $entrada_fecha ?> ">
		<div class="col">
			<input type="submit" value="cambiar" name="cambiar">
		</div>
	</div>
</form>
<!-- asignar crud para cada usuario -->
<!-- agregar usuario con perfil admin? -->


</div>

<style>
	input{
		width: 100%;
	}
</style>
<?php else: //si no es_admin ?>
<p>Error 503 (acceso prohibido): usuario no es admin.</p>
<?php endif; //fin if es_admin ?>