<?php 
$pag_admin = true;
include '../include/login/session.php';
require_once('../include/header.php');
require_once('../data/funciones.php');
require_once('../include/conexion_db.php');

// caudales
// SELECT * FROM caudales ORDER BY FECHA DESC;
// precipitaciones
// SELECT * FROM precip ORDER BY FECHA DESC;
// volumen puclaro
// SELECT * FROM puclaro_31 ORDER BY anio DESC;
// volumen laguna
// SELECT * FROM laguna_31 ORDER BY anio DESC;
// datos laguna
// SELECT * FROM laguna ORDER BY anio DESC;


if (isset($_GET['borrar'])) {
	$entrada_a_borrar = $_GET['borrar'];
	$tabla = $_GET["tabla"];
	$query = "DELETE FROM " . $tabla . " WHERE fecha =:fecha;";
	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);
		$stmt2 = $pdo->prepare($query);
		$stmt2->bindValue(':fecha',$entrada_a_borrar);
		$stmt2->execute();
		echo "<h3 class='red-text'>Fila de datos borrada exitosamente..</h3>";
	} catch (\PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}



} elseif (isset($_GET['agregar'])) {
	$usuario_a_agregar = $_GET['agregar'];
	// $query = "INSERT INTO usuarios (usuario,nombre,pass) values (:usuario,:nombre,crypt(:pass,gen_salt('bf',8)));";
// print_r($_GET);
// return;
	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);

		$tabla = $_GET['tabla'];

		// agregar de forma dinamica los nombres de las columnas 

		// print_r($_GET);
		$columnas = $_GET;
		if (($key = array_search('Agregar', $columnas)) !== false) {
			unset($columnas[$key]);
		}
		unset($columnas['tabla']);
		foreach ($columnas as $key => $value) {
			if ($value == '') {
				unset($columnas[$key]);
				# code...
			}
		}

		$fecha = $_GET['fecha'];

		$es_31 = strcmp($tabla, 'puclaro_31') == 0 ? true : (strcmp($tabla, 'laguna_31') == 0 ? true : false);

		if ($es_31 == false) {
			if(strlen($fecha) == 4) $fecha = $fecha . '-01-01';
			elseif (strlen($fecha) == 7) $fecha = $fecha . '-01';
			$fecha = "\"" . $fecha . "\"";
		}

		$columnas['fecha'] = $fecha;

		$headers = array_keys($columnas);
		$valores_check = array_values($columnas);
		$cant_datos_editar = count($headers);

		$sql = "INSERT INTO " . $tabla . " (";

		foreach ($headers as $key => $value) {
			$sql .= $value . ",";
		}
		$sql = rtrim($sql,",");
		$sql .= ") values (";

		for ($i=0; $i < $cant_datos_editar; $i++) {
			if ($valores_check[$i] !== '') {
				$sql .= " " . $valores_check[$i] . ",";
			} 
		}
		$sql = rtrim($sql,",");
		$sql = $sql . ")";

		// $sql .= " WHERE fecha = :fecha";

		$stmt = $pdo->prepare($sql);
		$stmt-> execute();
		echo "<h3 class='green-text'>Datos agregados exitosamente..</h3>";		
		// header('Location: /admin/datos.php');
	} catch(PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
}




if (isset($_GET["submit"])) {

	$tabla = $_GET["tabla"];
	$string_agregar = '';

	if (($tabla == 'laguna') || ($tabla == 'caudales') || ($tabla == 'precip')) {
		$query2 = "SELECT TO_CHAR(fecha :: DATE, 'yyyy-mm'),* FROM " . $tabla . " ORDER BY fecha DESC;";
		$hide_flag = true; 
	} else{
		$query2 = "SELECT * FROM " . $tabla . " ORDER BY 1 DESC;";
		$hide_flag = false;
	}

	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);
		$rs = $pdo->query("SELECT * FROM " . $tabla . " LIMIT 0");
		$string_header = '';
		for ($i = 0; $i < $rs->columnCount(); $i++) {
		    $col = $rs->getColumnMeta($i);
		    $string_header .= "<div class='col border border-primary'>" . formatearHeader($col['name']) . "</div>";
		    // echo $col['name'] . "<br>\n";
		    if ($col['name'] == 'fecha' ) {
			    $string_agregar .= "<div class='col'> <input type='text' name='" . $col['name'] . "'></div>";
		    } else{
			    $string_agregar .= "<div class='col'> <input type='number' step='0.01' min='0' name='" . $col['name'] . "'></div>";
		    }
		}
		$string_titulo = "<div class='row fixme bg-primary'>" . $string_header . "<div class='col relleno border border-primary'>Editar fila</div><div class='col relleno border border-primary'>Borrar Fila</div></div>";
		$string_agregar = "<form action='/admin/datos.php' method='get' style='width: 100%'><div class='row fixme'>" . $string_agregar . "<div class='col'><input type='submit' value='Agregar' name='agregar'></div><div class='col relleno'> <input type='hidden' name='tabla' value='" . $tabla . "'> </div></div></form>";
		$string_datos = '';

		// print_r($agregar_fila_array);
		// return;
		// echo $string_agregar;

		$stmt = $pdo->query($query2);
		$stmt->execute();
		// $datos = $stmt->fetchAll(PDO::FETCH_NUM);

		foreach ($stmt as $row) {
			$string_datos .= "<div class='row border-dark border'>\n";
			$i = 0;
			foreach ($row as $val) {
				// echo $i . "--->";
				// echo $val . "<br>\n";
				$display = '';
				if ($tabla == 'puclaro_31' || $tabla == 'laguna_31') {
					if ($i == 0) {
						$id = $val;
						if ($hide_flag == true) $display = 'd-none';
					} 
				}else{
					if ($i == 1) {
						$id = $val;
						if ($hide_flag == true) $display = 'd-none';
					}
				}
				$string_datos .= "<div class='col border " . $display . " '>" . $val . "</div>\n";
				$i++;
			}
			$string_datos .= "<div class='col'> <a href='editar_datos.php?entrada=" . $id . "&tabla=" . $tabla . "'>Editar</a></div>
			<div class='col'> <a href='datos.php?borrar=" . $id . "&tabla=" . $tabla . "'>Borrar</a></div>
			</div>\n";
		}

		// echo json_encode($datos);
		// $stmt = $pdo->prepare($query);
		// $stmt->execute();
	} catch (\PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}

}

$pag_admin = true;
$inicio = false;
$banner="PROGESHI/Elqui - Administración de datos";
require_once('../include/banner.php');
if ($es_admin == true):
?>

<div class="container-fluid">
	<div class="row"></div>
	<form action="/admin/datos.php" method="get">
	<div class="row my-3">
		<div class="col">ver datos de:</div>
		<div class="col">
			<select name="tabla" id="tabla">
	<option value="caudales">caudales</option>
	<option value="precip">precipitaciones</option>
	<option value="puclaro_31">volumen embalse puclaro al 31 de agosto</option>
	<option value="laguna_31">volumen embalse la laguna al 31 de agosto</option>
	<option value="laguna">datos embalse la laguna</option>
</select>
		</div>
		<div class="col">
			<input type="submit" name="submit" value="Abrir">
		</div>
		<div class="col">
				<?php if (isset($_GET['submit'])): ?>
			<div id="boton_fila">
				<button id="agregar_fila_boton">Agregar Dato</button>
			</div>
				<?php endif; ?>
		</div>
	</div>
	</form>
		<?php if(isset($_GET["submit"])): ?>
			<?php echo $string_titulo ?>
			<div class="row" id="agregar_fila_datos">
				
			</div>
			<?php echo $string_datos ?>
		<?php endif ?>
</div>

<script>
	var fixmeTop = $('.fixme').offset().top;
	$("#agregar_fila_boton").on('click', function(event) {
		event.preventDefault();
		// let fila_header = "<?php //echo $string_agregar ?>"
		let una_fila = "<?php echo $string_agregar ?>"// $("#filas").append(fila_header);
		$("#agregar_fila_datos").append(una_fila);
		$("#boton_fila").empty();

			/* Act on the event */
		});
$(window).scroll(function() {
    var currentScroll = $(window).scrollTop();
    if (currentScroll >= fixmeTop) {
        $('.fixme').css({
            position: 'fixed',
            top: '0',
            left: '0'
        });
    } else {
        $('.fixme').css({
            position: 'static',
            zIndex : '5'
        });
    }
});
</script>
<style>
	form{
		width: 100%;
	}	
	input[type=number],input[type=text]{
		width: 100%;
	}
</style>


<?php else: //si no es_admin ?>
<p>Error 503 (acceso prohibido): usuario no es admin.</p>
<?php endif; //fin if es_admin ?>