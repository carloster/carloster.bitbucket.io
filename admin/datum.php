<?php 

header('Content-Type: text/plain');
error_reporting(E_ALL);

$conn = oci_connect("elq_report", "reporting12", "192.168.200.3/ELQRUBP");

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

// SITE_NAME
// NUMERIC_VALUE
// UPDATE_TIME

//SELECT to_char(EVENT_TIME, 'YYYY-MM') as fecha_mes, round(AVG(NUMERIC_VALUE),3) as promedio FROM v_event_log_ts WHERE v_event_log.TAG_NAME = 'FLOW_VAL' and v_event_log.SITE_TYPE = 'FLUMEGATE R' and SITE_NAME='AFORADOR CANAL SAN CARLOS' group by fecha_hora order by fecha_hora;

$currentLocale= setlocale(LC_ALL, 'es_CL.utf8');
$fecha = new DateTime();
$string_csv = "fecha, Caudal\n";
$handle = oci_parse($conn, "SELECT TO_DATE(EVENT_TIME, 'YYYY-MM-DD'), round(AVG(NUMERIC_VALUE),3) FROM v_event_log WHERE v_event_log.TAG_NAME = 'FLOW_VAL' and v_event_log.SITE_TYPE = 'FLUMEGATE R' and SITE_NAME='AFORADOR CANAL SAN CARLOS' group by TO_DATE(EVENT_TIME, 'YYYY-MM-DD') order by TO_DATE(EVENT_TIME, 'YYYY-MM-DD') ASC");

oci_execute($handle);

while($row = oci_fetch_array($handle, OCI_DEFAULT)) {
//print_r($row);

	$nombre = $row["TO_DATE(EVENT_TIME, 'YYYY-MM-DD')"];
//	$tiempo = $row['UPDATE_TIME'];
//	$fecha = DateTime::createFromFormat('d-M-Y G:i:s.u',$tiempo);
//	$fecha_formateada = strftime("%Y-%m-%d %H:%M:%S", $fecha->getTimestamp());
	$valor = $row["ROUND(AVG(NUMERIC_VALUE),3)"];
	$string_csv .= "\"" . $nombre . "\",";
	$string_csv .= $valor  ."\n" ;

}

echo $string_csv;

?>

