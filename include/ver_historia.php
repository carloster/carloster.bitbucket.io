
<?php

$incluye_highcharts=true;
require_once('conexion_db.php');

require_once('../include/header.php');
require_once('../include/headers_graficos.php');

$meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
$meses_string = '"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"';
//y por que lo carretero? bueno, se debe a que no necesariamente va a haber un locale instalado en la maquina objetivo, asi que porsiaca tenemos este array

/****funciones pulentas robadas de uno que otro lado****/

function days_in_month($month, $year)
{
// calculate number of days in a month
return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
}

// echo $_GET['nombre_db'];


if (isset($_GET['nombre_db']) && !(strcmp('undefined',$_GET['nombre_db'] ) == 0) ) {
	$nombre_db = $_GET['nombre_db'];
} else{
	$nombre_db = 'rio_turbio_varillar';
}

if (isset($_GET['subgrafico']) && !(strcmp('undefined',$_GET['subgrafico'] ) == 0) ) {
	$subgrafico = $_GET['subgrafico'];
} else{
	$subgrafico = 'mensual';
}

if (isset($_GET['tabla']) && !(strcmp('undefined',$_GET['tabla'] ) == 0) ) {
	$tabla = $_GET['tabla'];
} else{
	$tabla = 'caudales_temporada';
}

$saltar_db = false;
$tipo_grafico = 'spline';
$unidad_excedencia = "[mmH2O]";
$eje_x = 'Años';

// $tooltip = 'medicion';
$keys = array_keys(array_combine(array_keys($headers_precip), array_column($headers_precip, 'nombre_db')),$nombre_db);
$encontrado = $headers_precip;
$tabla = "precip_temporada";
$tipo_grafico = "column";
$header = "Precipitación";
$header_excedencia = "Precipitación";
$titulo = "Precipitación en ";
$unidad = "[mmH2O]";
$operador = "sum";
$eje_y = $header . " " . $unidad;
$multiplicador = 1;
$query = "SELECT to_char(fecha,'YYYY-MM') as fecha, " . $nombre_db . " FROM " . $tabla . " WHERE " . $nombre_db . " >= 0 ORDER BY FECHA ASC;";
if (count($keys) === 0) {
	$keys = array_keys(array_combine(array_keys($headers_caudales), array_column($headers_caudales, 'nombre_db')),$nombre_db);
	$encontrado = $headers_caudales;
	$tipo_grafico = "spline";
	$tabla = "caudales_temporada";
	$header = "Caudal medio mensual";
	$header_excedencia = "Volumen promedio mensual";
	$titulo = "Caudal medio mensual en ";
	$unidad = "[m<sup>3</sup>/s]";
	$unidad_excedencia = "[Millones de m<sup>3</sup>]";
	$eje_y = "Caudal medio mensual [m<sup>3</sup>/s]";
	$operador = "avg";

	if ($nombre_db == 'cmm') {
		$query = "SELECT to_char(fecha,'YYYY-MM') as fecha, cmm FROM laguna WHERE cmm >= 0 and cmm IS NOT NULL ORDER BY FECHA ASC;";
	} else{
		$query = "SELECT to_char(fecha,'YYYY-MM') as fecha, " . $nombre_db . " FROM " . $tabla . " WHERE " . $nombre_db . " >= 0 AND " . $nombre_db . " IS NOT NULL ORDER BY FECHA ASC;";
	}

	if (count($keys) === 0) {
		$keys = array_keys(array_combine(array_keys($headers_situacion), array_column($headers_situacion, 'nombre_db')),$nombre_db);
		$encontrado = $headers_situacion;
		$tabla = $nombre_db; //necesario? proq.prommra.cl
		$header = "volumen acumulado al 31 de agosto";
		$titulo = "Situación hídrica en ";
		$unidad = "[Millones de m<sup>3</sup>]";
		$multiplicador = 1;
		$operador = "avg";
		$query = "SELECT fecha, " . $nombre_db . "_agosto FROM " . $tabla . " WHERE " . $nombre_db . "_agosto >= 0 ORDER BY fecha ASC;";
		$tipo_grafico = 'column';
	}
}
$es_embalse = $nombre_db === "puclaro_31" || $nombre_db === "laguna_31" ? true : false;

if ($subgrafico == 'excedencia' && $es_embalse) {
	// echo  "SELECT fecha, " . $nombre_db . "_agosto FROM " . $tabla . " WHERE " . $nombre_db . "_agosto >= 0 ORDER BY fecha ASC;";


	$eje_x = 'Probabilidad de excedencia';

	$eje_y = $header_excedencia . " " . $unidad_excedencia;
	$query = "select distinct on (suma) * from (SELECT fecha,
    " . $operador . "(" . $tabla . "." . $nombre_db . "_agosto) AS suma
   FROM " . $tabla . "
   GROUP BY fecha
   ORDER BY " . $operador . "(" . $tabla . "." . $nombre_db . "_agosto) desc) as datos where suma is not null order by suma desc;";

	$header = "Probabilidad de excedencia";
	$titulo = "Probabilidad de excedencia de " . $header_excedencia . " en ";
	$tipo_grafico = 'column';



} elseif($subgrafico == 'excedencia' ){

	$eje_x = 'Probabilidad de excedencia';
	$multiplicador = 2.592; // volumen promedio del mes (caudal medio mensual * 30 dias en un mes (y si son 31, aaah? aahh? AAAHHH???!))

	$eje_y = $header_excedencia . " " . $unidad_excedencia;
	$query = "select distinct on (suma) * from (SELECT extract('year' from fecha) AS año,
    " . $operador . "(" . $tabla . "." . $nombre_db . ") AS suma
   FROM " . $tabla . "
   GROUP BY extract('year' from fecha)
   ORDER BY " . $operador . "(" . $tabla . "." . $nombre_db . ") desc) as datos where suma is not null order by suma desc;";
	if($tabla == 'caudales_temporada' && $nombre_db == 'cmm') {
		$query = "select distinct on (suma) * from (SELECT extract('year' from fecha) AS año,
	    " . $operador . "(laguna." . $nombre_db . ") AS suma
	   FROM laguna
	   GROUP BY extract('year' from fecha)
	   ORDER BY " . $operador . "(laguna." . $nombre_db . ") desc) as datos where suma is not null order by suma desc;";

	}
// ejemplo correcto:

//  		 select distinct on (suma) * from (SELECT date_part('year'::text, caudales.fecha) AS año,
// avg(caudales.rio_la_laguna_salida_embalse) AS suma
// FROM caudales 
// GROUP BY (date_part('year'::text, caudales.fecha)) 
// ORDER BY (avg(caudales.rio_la_laguna_salida_embalse)) desc) as datos where suma is not null order by suma desc;

	$header = "Probabilidad de excedencia";
	$titulo = "Probabilidad de excedencia de " . $header_excedencia . " en ";
	$tipo_grafico = 'column';
} 

if($subgrafico == 'excedencia_mes'){
	
	require_once('excedencia_85.php');
	
	$tabla = "caudales_temporada";
	$header = "Caudal medio mensual con 85% de Probabilidad de excedencia";
	$header_excedencia = "Caudal medio mensual con 85% de Probabilidad de excedencia";
	$titulo = "Caudal medio mensual con 85% de Probabilidad de excedencia en ";
	$unidad = "[m<sup>3</sup>/s]";
	$unidad_excedencia = "[m<sup>3</sup>/s]";
	$eje_y = "Caudal medio mensual [m<sup>3</sup>/s]";
	$operador = "avg";

$saltar_db = true;

} elseif ($subgrafico == 'temporada') {
	if ($tabla == 'precip_temporada' && $nombre_db !== 'cmm') { //precipitacion temporada
		$query = "SELECT EXTRACT('year' from fecha), sum(" . $nombre_db . ") as promedio FROM " . $tabla . " WHERE " . $nombre_db . " >= 0 AND " . $nombre_db . " IS NOT NULL GROUP BY EXTRACT('year' from fecha) ORDER BY EXTRACT('year' from fecha) ASC;";
		// $query = "SELECT EXTRACT('year' from fecha), sum(" . $nombre_db . ") as promedio FROM " . $tabla . " WHERE " . $nombre_db . " >= 0 AND " . $nombre_db . " IS NOT NULL AND EXTRACT('month' from fecha) >= 1 AND EXTRACT('month' from fecha) <= 8 GROUP BY EXTRACT('year' from fecha) ORDER BY EXTRACT('year' from fecha) ASC;";
	} elseif($tabla == 'caudales_temporada' && $nombre_db == 'cmm') {
		$query = "SELECT año,vtemporada FROM vtemporada ORDER BY año ASC;";
		$header = "volumen por temporadas";
		$titulo = "volumen por temporadas en ";
		$unidad = "[Mm<sup>3</sup>]";
		$eje_y = "Volumen [Mm<sup>3</sup>]";	} elseif($es_embalse) {
		$query = "SELECT fecha, avg(" . $nombre_db . "_agosto) as promedio FROM " . $tabla . " WHERE " . $nombre_db . "_agosto >= 0 AND " . $nombre_db . "_agosto IS NOT NULL GROUP BY fecha ORDER BY fecha ASC;";
	} else { // volumen temporada
		$query = "SELECT año, (" . $nombre_db . ") as volumen FROM volumen_temporada ORDER BY año ASC;";
		// $query = "SELECT año, (" . $nombre_db . ") as volumen FROM volumen_temporada WHERE " . $nombre_db . " >= 0 AND " . $nombre_db . " IS NOT NULL ORDER BY año ASC;";
// 		select fecha, 
// (rio_elqui_lsc / 1000000) as rio_elqui_lsc,

		// $query = "SELECT EXTRACT('year' from fecha), avg(" . $nombre_db . ") as promedio FROM " . $tabla . " WHERE " . $nombre_db . " >= 0 AND " . $nombre_db . " IS NOT NULL AND EXTRACT('month' from fecha) >= 1 AND EXTRACT('month' from fecha) <= 8 GROUP BY EXTRACT('year' from fecha) ORDER BY EXTRACT('year' from fecha) ASC;";
		$header = "volumen por temporadas";
		$titulo = "volumen por temporadas en ";
		$unidad = "[Mm<sup>3</sup>]";
		$eje_y = "Volumen [Mm<sup>3</sup>]";



	}
	$tipo_grafico = 'column';
	
	# code...
}
// echo $query;

// echo $query;
// echo "<br>";
$ubicacion = $keys[0];
$nombre_formal = $encontrado[$ubicacion]['formal'];
$titulo = $titulo . $nombre_formal;


// para prob de excedencia:
// 
// 
// select distinct suma,año from (SELECT date_part('year'::text, precip.fecha) AS "año",
//     sum(precip.la_ortiga) AS suma
//    FROM precip
//   GROUP BY (date_part('year'::text, precip.fecha))
//   ORDER BY (sum(precip.la_ortiga)) desc) as datos where suma is not null order by suma desc;



/*encontrar nombre_db para sacar formal*/


$headers = array("fecha", $header);

if ($saltar_db == false) {
	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);

			$cant_headers = count($headers);
			$string_col_0 = '';
			$string_col_1 = '';
			$string_col_2 = '';

			$stmt = $pdo->prepare($query);
			$stmt->execute();
		$i=0;
		$j=1;
		
		$filas=$stmt->rowCount();

		// CONTAR FILAS
		// $FILAS = FILAS + 1
		// PROB. EXCEDENCIA: $J/$FILAS
		// 
		// i=0: fecha
		// i=1: valor
		// i=2: nada, se tiene como placeholder
		$fecha_old = '';
		foreach ($stmt as $row) {
				foreach ($row as $val) {
					if ($i == 0) {

						$string_temp = $val;

// foreachs para guardar datos de la db
// 
// operando con la fecha....
// si el grafico es de excedencia (independiente de si es caudal, precip, vol. de embalses): no se toma la fecha, sino que se opera con contadores para calcular la probabilidad

						if ($subgrafico == 'excedencia') {
							// $string_col_0 .= "\"" . $j . "/" . ($filas+1) . "\",";
							if ($i == 0) {
								$string_temp = "\"" . (number_format(($j/($filas+1))*100,1)) . "%\",";
							}
						} else{
							if ($subgrafico == 'temporada') {
								// año
								$temp = $val+1;
								$val = $val . "/" . $temp;
							}
							$string_temp = "\"" . $val . "\",";
						}
					} 
					else if ($i == 1) {
						if (!is_null($val)) {
							// $val = "\"null\"";
							// $string_col_1 .= $val . ",";
							$val = number_format($val * $multiplicador,2,'.','');
							$string_col_0 .= $string_temp;
							$string_col_1 .= $val . ",";
						}
					} else if ($i == 2) {
// este i es opcional, pero el razonamiento es: mientras mas columnas seleccionadas de la db, mas i's
						$string_col_2 .= $val . ",";
					}









					$i++;
				}
			$j++;
			$i=0;
		}
			$string_col_0 = rtrim($string_col_0,',');
			$string_col_1 = rtrim($string_col_1,',');
			$string_col_2 = rtrim($string_col_2,',');

			// echo "~~~" . $j . "~~~";
			// echo $string_col_0;
			// echo "~~~" . 0 . "~~~";
			// echo $string_col_1;
			// echo "~~~" . 1 . "~~~";
			// echo $string_col_2;
			// echo "~~~" . 2 . "~~~";

	} catch (\PDOException $e) {
	     throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
}
?>

<div id="visor" style="height: 650px; width: 1200px;"></div>
<script>

var tabla, titulo;
    
    Highcharts.setOptions({
        lang: {
            thousandsSep: ',',
            decimalPoint: '.'
        }
    });
let chart = Highcharts.chart('visor', {
	chart:{
			type: '<?php echo $tipo_grafico ?>',
			zoomType: 'xy'
		}
		,title: {
			text: "<?php echo $titulo ?>"
		}
		,tooltip: {
	        formatter: function () {
	        	//this.x = fecha, o probabilidad
	        	//this.y = valor
	        	let string
	        	let valor = this.y
	        	valor = parseFloat(valor,2)
	        	<?php if($subgrafico == 'excedencia'): ?>
	        	string = 'Probabilidad de exceder ' + valor + " <?php echo $unidad_excedencia ?>: " + this.x
	        	<?php elseif($subgrafico == 'excedencia' && $tabla == 'caudales_temporada'): ?>
	        	string = '85% de probabilidad para exceder ' + valor + " <?php echo $unidad_excedencia ?>: " + this.x
	    		<?php else: ?>
	        	string = '<?php echo $header ?> en ' + this.x + ": " + parseFloat(valor,2) + " <?php echo $unidad ?>"
	    		<?php endif; ?>
	    		return string
	            // return 'The value for <b>' + this.x +
	                // '</b> is <b>' + this.y + '</b>';
	        }
	    }
    // tooltip: {
    //   formatter: function() {
    //     var series = this.series.chart.series,
    //       x = this.x,
    //       each = Highcharts.each,
    //       txt = '<span style="font-size: 10px">' + this.key + '</span><br/>';

    //     each(series, function(serie, i) {
				// 	each(serie.data, function(data, j){
    //       	if(data.x === x) {
    //         	txt += '<span style="color:' + data.color + '">\u25CF</span> ' + data.series.name + ': <b>' + data.y + '</b><br/>';
    //         }
    //       });
    //     });
        
    //     return txt;
    //   }
    // },





		,yAxis: [{
			title: {
				text: ' <?php echo $eje_y ?> '
			},
			// <?php //if($tabla == 'precip_temporada'): ?>
			// max: 6//00,
			// <?php //elseif($tabla == 'puclaro_31'): ?>
			// max: 2//11,
			// <?php //elseif($tabla == 'laguna_31'): ?>
			// max: 4//1,
			// <?php //endif; ?>
			min: 0,
			startOnTick: false,
			endOnTick: false,
			alignTicks: true,
			plotLines:[{
				color: '#FF0000',
				value: 0,
				width: 2,
				dashStyle: 'Solid'
			}]
		}]
		,xAxis: [{
				startOnTick: false,
				categories: [<?php echo $string_col_0 ?>],
				title: {
					text: ' <?php echo $eje_x ?> '
				}

		}]
		,plotOptions: {
			series: {
				visible: true
			}
		}

		//agregar año de referencia para las excedencias del 85
		,connectNulls: true
		,ignoreHiddenSeries : true
		,series: [{
			type: '<?php echo $tipo_grafico ?>',
			styledMode: true,
            formatter: {
                format: '{point.y:,.2f} [Lt/s]'
            },
			data: [<?php echo $string_col_1 ?>],
			name: "<?php echo $headers[1] ?>",
        	pointStart: 0
		<?php if ($string_col_2 > 0): ?>
			},{
			visible: false,
			showInLegend: true,
			data: [<?php echo $string_col_2 ?>],
			name: "<?php echo $headers[2] ?>",
        	pointStart: 0
        <?php endif; ?>        	
        }]
	});
</script>
