
<?php

function getHeaders($pdo_datos,$tabla){
	$headers = array();
	$stmt = $pdo_datos->prepare("SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_NAME = '" . $tabla . "'");
	$stmt->execute();

	print_r($stmt);

	foreach ($stmt as $row) {
		array_push($headers, $row['column_name']);
	}
	return $headers;
}

//TODO: REFORMATEAR INPUT SI HAY CHAR INVALIDO
//listo, desmarque.php se encarga de eso (falta check en esta pagina por algun vivo)
require_once('../include/conexion_db.php');

$incluye_highcharts=true;
require_once('../include/header.php');

$meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
$meses_string = '"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"';
//y por que lo carretero? bueno, se debe a que no necesariamente va a haber un locale instalado en la maquina objetivo, asi que porsiaca tenemos este array

$rios = array(
	'RTV' => 'rio_turbio_varillar',
	'REA' => 'rio_elqui_algarrobal',
	'RCR' => 'rio_claro_rivadavia',
	'RLLEE' => 'rio_laguna_entrada_embalse'
);

$rios_verbose = array(
	'RTV' => 'Rio Turbio en Varillar',
	'REA' => 'Rio Elqui en Algarrobal',
	'RCR' => 'Rio Claro en Rivadavia',
	'RLLEE' => 'Rio La Laguna entrada Embalse La Laguna'
);

$estacion_verbose = array(
	'cochiguaz' => 'Estacion Cochiguaz DGA',
	'la_ortiga' => 'Estacion La Ortiga DGA'
);


if (isset($_GET['tabla'])) {
	$tabla = $_GET['tabla'];
} else{
	$tabla = 'grupo';
}

if (isset($_GET['punto'])) {
	$punto = $_GET['punto'];
} else{
	$punto = 0;
}

if (isset($_GET['mes'])) {
	$mes = $_GET['mes'];

} else{
	$mes = 8;
}
	$nombre_mes = $meses[$mes-1];


if (isset($_GET['rio'])) {
	$rio = $_GET['rio'];
	$rio = strtoupper($rio);
} else{
	$rio = 'RLLEE';
}
	$nombre_rio = $rios[$rio];


if (isset($_GET['titulo'])) { //opcional
	$titulo = $_GET['titulo'];
}

if (isset($_GET['tam'])) { //opcional
	$tam = $_GET['tam'];
} else{
	$tam = 'embed';
}

if (isset($_GET['estacion'])) { //opcional
	$estacion = $_GET['estacion'];
} else{
	$estacion = 'cochiguaz';
}

$headers = array("año", "mes", "caudal medio");

// ToDo vvvv
// $query = "SELECT 
// 		date_part('year', t.fecha) AS anio,
// 	 	ROUND(" . $op . "(t." . $nombre[$cosa] . "), 2) AS " . $op_nombre . "
//  	FROM
//  		" . $tabla . " t
// 	WHERE";
// if ($op == 'AVG') {
// 	$query = $query . " date_part('month', t.fecha) = " . $mes ." AND";
// }

// $query = $query . "date_part('year', t.fecha) >= 1989
// 	GROUP BY
// 		date_part('year', t.fecha)
// 	ORDER BY anio ASC";

// $titulo = $titulo_medicion . ' en los meses de mayo (' . $cosa_verbose[$cosa] . ')';

// End ToDo

// presets para ahorrar condicionales en la generacion del chart
$db = true;
$percentiles_laguna = false;
$interactivo = true;

if ($tabla == 'ppeq') {

	$headers = array("año", "Precipitacion Equivalente");
	$query = "SELECT * from ppeq order by anio asc";
	$titulo = 'Precipitación Equivalente acumulada [mmH2O] en estación La Laguna embalse';
	$grafico = 'column';
	$query_percentiles = "SELECT
  percentile_cont(0.05) WITHIN GROUP (ORDER BY pp_equiv) AS percentil_5,
  percentile_cont(0.25) WITHIN GROUP (ORDER BY pp_equiv) AS percentil_25,
  percentile_cont(0.5) WITHIN GROUP (ORDER BY pp_equiv) AS mediana,
  percentile_cont(0.75) WITHIN GROUP (ORDER BY pp_equiv) AS percentil_75,
  percentile_cont(0.9) WITHIN GROUP (ORDER BY pp_equiv) AS percentil_90
FROM ppeq;";

} elseif ($tabla == 'alt_nieve') {
	
	$headers = array("año", "Altura de nieve");
	$query = "SELECT * FROM alt_nieve ORDER BY anio ASC";
	$titulo = 'Altura de Nieve [cm] en estación La Laguna embalse';
	$grafico = 'column';
	$query_percentiles = "
		SELECT
		  percentile_disc(0.05) WITHIN GROUP (ORDER BY alt_nieve) AS percentil_5,
		  percentile_disc(0.25) WITHIN GROUP (ORDER BY alt_nieve) AS percentil_25,
		  percentile_disc(0.5) WITHIN GROUP (ORDER BY alt_nieve) AS mediana,
		  percentile_disc(0.75) WITHIN GROUP (ORDER BY alt_nieve) AS percentil_75,
		  percentile_disc(0.9) WITHIN GROUP (ORDER BY alt_nieve) AS percentil_90
		FROM alt_nieve;
		";

} elseif ($tabla == 'cmm' && (isset($mes) && isset($rio))) {

	$headers = array("año", "Caudal medio mensual (" . $nombre_mes . ")");
	$query = "SELECT EXTRACT('year' FROM fecha)," . $nombre_rio . " FROM caudales WHERE EXTRACT('month' FROM fecha) = " . $mes . " AND EXTRACT('year' FROM fecha) > 1988 ORDER BY 1 asc;";
	// $query = "SELECT * FROM " . $nombre_rio . "_" . strtolower($nombre_mes) . " ORDER BY año asc;";
	$titulo = 'Caudales promedio mensuales en los meses de ' . $nombre_mes . ' (' . $rios_verbose[$rio] . ')';
	$grafico = 'column';
	// echo $query;
	$query_percentiles = "SELECT
  percentile_cont(0.05) WITHIN GROUP (ORDER BY caudal) AS percentil_5,
  percentile_cont(0.25) WITHIN GROUP (ORDER BY caudal) AS percentil_25,
  percentile_cont(0.5) WITHIN GROUP (ORDER BY caudal) AS mediana,
  percentile_cont(0.75) WITHIN GROUP (ORDER BY caudal) AS percentil_75,
  percentile_cont(0.9) WITHIN GROUP (ORDER BY caudal) AS percentil_90
FROM " . $nombre_rio . "_" . $nombre_mes . ";";

} elseif ($tabla == 'laguna') {

	$headers = array("año", "Caudal medio mensual (" . $nombre_mes . ")");
	$query = "SELECT EXTRACT('year' FROM fecha) as año,cmm FROM laguna WHERE EXTRACT('month' FROM fecha) = " . $mes . " AND EXTRACT('year' FROM fecha) > 1988 ORDER BY 1 asc;";
	// $query = "SELECT cmm FROM laguna WHERE extract('month' from fecha) = " . $mes . " ORDER BY fecha asc;";
	$titulo = 'Caudales promedio mensuales en los meses de ' . $nombre_mes . ' (' . $rios_verbose[$rio] . ')';
	$grafico = 'column';
	$query_percentiles = "SELECT
  percentile_cont(0.05) WITHIN GROUP (ORDER BY cmm) AS percentil_5,
  percentile_cont(0.25) WITHIN GROUP (ORDER BY cmm) AS percentil_25,
  percentile_cont(0.5) WITHIN GROUP (ORDER BY cmm) AS mediana,
  percentile_cont(0.75) WITHIN GROUP (ORDER BY cmm) AS percentil_75,
  percentile_cont(0.9) WITHIN GROUP (ORDER BY cmm) AS percentil_90
FROM (SELECT EXTRACT('year' FROM fecha) as año,cmm FROM laguna WHERE extract('month' from fecha) = " . $mes . ") as mes ;";

} elseif ($tabla == 'pp_acum' && isset($estacion)) {

	$headers = array("año", "Precipitacion Anual Acumulada [mmH2O]");
	$query = "SELECT extract('year' from fecha), SUM(" . $estacion . ") FROM precip WHERE extract('year' from fecha) > 1988 AND EXTRACT('month' from fecha) >= 1 AND EXTRACT('month' from fecha) < 9 GROUP BY extract('year' from fecha) ORDER BY extract('year' from fecha) ASC;";
	// echo $query;
	// $query = "SELECT 
	// 	date_part('year', t.fecha) AS anio,
	//  	ROUND(SUM(t." . $estacion . "), 2) AS precipitacion
 // 	FROM
 // 		pp_temporadas t
	// WHERE
	// 	date_part('year', t.fecha) > 1988
	// GROUP BY
	// 	date_part('year', t.fecha)
	// ORDER BY anio ASC";
	$titulo = 'Precipitacion Acumulada Anual (' . $estacion_verbose[$estacion] . ')';
	$grafico = 'column';
	$query_percentiles = "
SELECT
  percentile_cont(0.05) WITHIN GROUP (ORDER BY " . $estacion . ") AS percentil_5,
  percentile_cont(0.25) WITHIN GROUP (ORDER BY " . $estacion . ") AS percentil_25,
  percentile_cont(0.5) WITHIN GROUP (ORDER BY " . $estacion . ") AS mediana,
  percentile_cont(0.75) WITHIN GROUP (ORDER BY " . $estacion . ") AS percentil_75,
  percentile_cont(0.9) WITHIN GROUP (ORDER BY " . $estacion . ") AS percentil_90
FROM (SELECT SUM(" . $estacion . ") as " . $estacion . " FROM precip WHERE EXTRACT('year' from fecha) > 1988 AND " . $estacion . " > 0  AND EXTRACT('month' FROM fecha) >= 1 AND EXTRACT('month' from fecha) < 9 AND " . $estacion . " IS NOT NULL AND " . $estacion . " > 0 GROUP BY EXTRACT('year' FROM fecha)) AS percentiles;";
// echo $query_percentiles; 

} elseif ($tabla == 'ppElqui') {

	$headers = array("año", "Precipitacion Anual Acumulada [mmH2O]");
	$query = "SELECT * from pisco_elqui_ceaza ORDER BY anio ASC";
	// $query = "SELECT 
	// 	date_part('year', t.fecha) AS anio,
	//  	ROUND(SUM(t.pisco_elqui_ceaza), 2) AS precipitacion
 // 	FROM
 // 		pp_temporadas t
	// WHERE
	// 	date_part('year', t.fecha) > 2003
	// GROUP BY
	// 	date_part('year', t.fecha)
	// ORDER BY anio ASC";
	$titulo = 'Precipitacion Acumulada Anual (Estacion Pisco Elqui CEAZA)';
	$grafico = 'column';
		$query_percentiles = "
SELECT
  percentile_cont(0.05) WITHIN GROUP (ORDER BY precipitacion) AS percentil_5,
  percentile_cont(0.25) WITHIN GROUP (ORDER BY precipitacion) AS percentil_25,
  percentile_cont(0.5) WITHIN GROUP (ORDER BY precipitacion) AS mediana,
  percentile_cont(0.75) WITHIN GROUP (ORDER BY precipitacion) AS percentil_75,
  percentile_cont(0.9) WITHIN GROUP (ORDER BY precipitacion) AS percentil_90
FROM pisco_elqui_ceaza  WHERE
precipitacion > 0;";

} elseif ($tabla == 'vtemporada') {

	// $headers = array("año", "caudal medio");
	$headers = array("año", "volumen");
	// $query = "SELECT date_part('year', laguna.fecha) AS anio,vtemporada FROM public.laguna WHERE vtemporada IS NOT NULL ORDER BY fecha ASC";
	$query = "SELECT * FROM vtemporada ORDER BY año ASC";
	$titulo	= "Volumen temporada afluente embalse La Laguna [Millones de m3]";
	$grafico = 'column';
	$interactivo = true;
	$query_percentiles = "
SELECT
  percentile_cont(0.05) WITHIN GROUP (ORDER BY vtemporada) AS percentil_5,
  percentile_cont(0.25) WITHIN GROUP (ORDER BY vtemporada) AS percentil_25,
  percentile_cont(0.5) WITHIN GROUP (ORDER BY vtemporada) AS mediana,
  percentile_cont(0.75) WITHIN GROUP (ORDER BY vtemporada) AS percentil_75,
  percentile_cont(0.9) WITHIN GROUP (ORDER BY vtemporada) AS percentil_90
FROM vtemporada;";

} elseif ($tabla == 'laguna_31_agosto') {

	// $headers = array("año", "caudal medio");
	$headers = array("año", "volumen");
	// $query = "SELECT fecha,cmm FROM public.laguna ORDER BY fecha ASC";
	$query = "SELECT * FROM laguna_31 ORDER BY fecha ASC";
	$titulo	= "Volumen de Embalse La Laguna al 31 de agosto [Millones de m3]";
	$grafico = 'column';
	$interactivo = true;
	$query_percentiles = "
SELECT
  percentile_cont(0.05) WITHIN GROUP (ORDER BY laguna_31_agosto) AS percentil_5,
  percentile_cont(0.25) WITHIN GROUP (ORDER BY laguna_31_agosto) AS percentil_25,
  percentile_cont(0.5) WITHIN GROUP (ORDER BY laguna_31_agosto) AS mediana,
  percentile_cont(0.75) WITHIN GROUP (ORDER BY laguna_31_agosto) AS percentil_75,
  percentile_cont(0.9) WITHIN GROUP (ORDER BY laguna_31_agosto) AS percentil_90
FROM laguna_31;";

} elseif ($tabla == 'puclaro_31_agosto') {

	// $headers = array("año", "caudal medio");
	$headers = array("año", "volumen");
	// $query = "SELECT fecha,cmm FROM public.laguna ORDER BY fecha ASC";
	$query = "SELECT * FROM puclaro_31 ORDER BY fecha ASC";
	$titulo	= "Volumen de Embalse Puclaro al 31 de agosto [Millones de m3]";
	$grafico = 'column';
	$interactivo = true;
	$query_percentiles ="
SELECT
  percentile_cont(0.05) WITHIN GROUP (ORDER BY puclaro_31_agosto) AS percentil_5,
  percentile_cont(0.25) WITHIN GROUP (ORDER BY puclaro_31_agosto) AS percentil_25,
  percentile_cont(0.5) WITHIN GROUP (ORDER BY puclaro_31_agosto) AS mediana,
  percentile_cont(0.75) WITHIN GROUP (ORDER BY puclaro_31_agosto) AS percentil_75,
  percentile_cont(0.9) WITHIN GROUP (ORDER BY puclaro_31_agosto) AS percentil_90
FROM puclaro_31;";

} else{

	$query = "SELECT date_part('month',fecha) as mes, AVG(distinct cmm) FROM public.laguna GROUP BY mes";
	$titulo	= "Caudales promedio mensuales [m³/s] (registro histórico agrupado por mes)";
	$grafico = 'column';
	$interactivo = false;
}

$json=false; //json porque por que no?

if ($db == true) {

	try {
		$pdo_datos = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);

			$cant_headers = count($headers);
			$string_col_0 = '';
			$string_col_1 = '';
			$string_col_2 = '';

		if ($json == true) {
			$stmt = $pdo_datos->prepare($query);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_NUM);
			$results = $headers + $results;
			$return_json = json_encode($results);
			echo $return_json;

			$stmt_percentiles = $pdo_datos->prepare($query_percentiles);
			$stmt_percentiles->execute();
			$results_percentiles = $stmt_percentiles->fetchAll(PDO::FETCH_NUM);


		} else{
			$stmt = $pdo_datos->prepare($query);
			$stmt->execute();

			if ($percentiles_laguna == false) {
				$stmt_percentiles = $pdo_datos->prepare($query_percentiles);
				$stmt_percentiles->execute();
				$percentiles_db = array();
				$percentiles_colores = array("#FF0000", "#FF7F00", "#FEFF00", "#7FFF00", "#008A12");
				$percentiles_etiqueta = array(5,25,50,75,90);

				foreach ($stmt_percentiles as $row) {
					foreach ($row as $percval) {
						array_push($percentiles_db, $percval);
					}
				}
			}
		}

		$i=0;
		$j=0;
		// array auxiliar para calcular promedio, etc.
		$puntos = array();
		// devolver datos

// print_r($percentiles_db);

		foreach ($stmt as $row) {
				foreach ($row as $val) {
					if ($tabla == 'total') {
						if ($val == '') {
							$val = 'NULL';
							# code...
						}
						if ($i == 0) {
							// $string_col_0 .= $val . "-";
							// $temp = $val . "-";
							$string_col_0 .= "\"" .$val . "\",";
							$ultimo_anio = $val;
						} else if ($i == 1) {
							// $temp .= $val . "-1";
							// $temp_ts = strtotime($temp);
							// $string_col_0 .= $val . "-1,";
							$string_col_1 .= $val . ",";
							array_push($puntos, $val);
						} else if ($i == 2) {
							$string_col_2 .= $val . ",";
						}
					} else{
						if ($i == 0) {
							$string_col_0 .= "\"" .$val . "\",";
							$ultimo_anio = $val;
							// echo $ultimo_anio;
						} else if ($i == 1) {
							array_push($puntos, $val);
							$string_col_1 .= $val . ",";
						} else if ($i == 2) {
							$string_col_2 .= $val . ",";
						}
					}
					$i++;
				}
			$j++;
			$i=0;
		}
		// echo $query;
			if ($interactivo == true) {
				$ultimo_anio = $ultimo_anio + 1;
				$string_col_0 .= $ultimo_anio;
				$string_col_1 .= $punto;
			}

			if ($tabla == 'grupo') {
				$string_col_0 = $meses_string;
				# code...
			} else{
				$string_col_0 = rtrim($string_col_0,',');
			}
			
			$string_col_1 = rtrim($string_col_1,',');
			$string_col_2 = rtrim($string_col_2,',');
			// $percentiles_string = rtrim($percentiles_string,',');

			if(count($puntos)) {
			    $puntos = array_filter($puntos);
			    $promPuntos = array_sum($puntos)/count($puntos);
			}
	// percentiles

	} catch (\PDOException $e) {

		echo "\n\n<br><br>";
		echo $query;
		echo "\n\n<br><br>";
		echo $query_percentiles;
		echo "\n\n<br><br>";
	     throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
}


// print_r($percentiles_db);

?>
<!-- <script src="https://unpkg.com/simple-statistics@7.0.2/dist/simple-statistics.min.js"></script> -->
<body style="text-align: center; height: 350px; width: 1200px">
	<div id="visor" style="height: 350px; width: 1200px; text-align: center"></div>
</body>
<script>


<?php if ($tabla == 'total'): ?>
	let chart = Highcharts.stockChart('visor', {
<?php else: ?>
	let chart = Highcharts.chart('visor', {
<?php endif; ?>
		chart:{
			type: '<?php echo $grafico ?>',
			// type: 'column'
			zoomType: 'xy'
		},
		title: {
			text: "<?php echo $titulo ?>"
		},
		credits:{
			enabled: false
		}, 
		yAxis: [{
			title: {
				<?php if($tabla == 'pp_acum' || $tabla == 'ppElqui'): ?>
				text: 'precipitacion [mmH2O]'
				<?php elseif($tabla == 'ppeq'): ?>
				text: 'precipitacion equivalente [mmH2O]'
				<?php elseif($tabla == 'alt_nieve'): ?>
				text: 'altura de nieve [cm]'
				<?php elseif(($tabla == 'vtemporada') || ($tabla == 'laguna_31_agosto' || $tabla == 'puclaro_31_agosto')): ?>
				text: 'Volumen [Mm<sup>3</sup>]'
				<?php else: ?>
				text: 'caudal [m<sup>3</sup>]'
				<?php endif; ?>
			},
			gridLineColor: '#fff',

			<?php if($tabla == 'agosto'): ?>
			min: -2,
			<?php else: ?>
			min: 0,
			<?php endif; ?>

			<?php // if($tabla == 'cmm'): ?>
			// max: 20,
			<?php // else: ?>
			// max: <?php // echo ($percentiles_db[4]*1.5) ?>,
			<?php // endif; ?>

			max: <?php echo ($percentiles_db[4]*2.5) ?>,

			//max: <?php //echo ($percentiles_db[4]*1.5) ?>,
			startOnTick: true,
			endOnTick: false,
			alignTicks: true,

		<?php if($percentiles_laguna == true): ?>
			plotBands: [
			<?php for ($i=0; $i < $cant_perc-1; $i++): ?>
				<?php 
				if($i == 0):
					$perc_temp = $perc_valor[$i];
					$color_temp = $perc_color[$i];
				?>
				{
					color: 'rgba(255,0,0,0.6)', // Color value
					label:{
						text: "percentil <?php echo $perc_etiqueta[$i] ?>: <?php echo round($perc_valor[$i],2) ?>",
						align: 'left'
					},
					from: 0, // Start of the plot band
					to: <?php echo $perc_temp ?> // End of the plot band
				}

				<?php else: ?>
					,{
					color: '<?php echo $color_temp ?>', // Color value
					from: <?php echo $perc_temp ?>, // Start of the plot band
					to: <?php echo $perc_valor[$i] ?>, // End of the plot band
					label:{
						text: "percentil <?php echo $perc_etiqueta[$i] ?>: <?php echo round($perc_valor[$i],2) ?>",
						align: 'left'
					}
				}
				<?php endif; ?>
				<?php 
					$perc_temp = $perc_valor[$i];
					$color_temp = $perc_color[$i];
				 ?>

			<?php endfor; ?>
			],
			<?php else: ?>
			plotBands: [
			<?php for($o = 0; $o < 5; $o++): ?>
				{
					color: '<?php echo $percentiles_colores[$o] ?>',
					from: <?php echo $o == 0 ? 0 : $percentiles_db[$o-1] ?>,
					to: <?php echo $percentiles_db[$o] ?>,
					label:{
						text: '<?php echo ($o == 2 ? "Mediana: " :  "Percentil " . $percentiles_etiqueta[$o] . ": ") . number_format($percentiles_db[$o],2) ?>',
						align: '<?php echo $o % 2 == 0 ? "right" : "left" ?>',
						verticalAlign: 'top'
					}
				} <?php echo $i < 4 ? ',' : '' ?>
			<?php endfor; ?>
			],
		<?php endif; ?>

			plotLines:[{
				color: 'black',
				dashStyle: 'solid',
				width: 2,
				zIndex: 5,
				value: <?php echo $promPuntos ?>,
				label:{
					text: 'Promedio: ' + <?php echo number_format($promPuntos,2) ?>,
					align: 'center'
				}
			}]
			<?php if($percentiles_laguna == true ): ?>
				,
    			categories: [<?php
				for ($i=0; $i < $cant_perc-1; $i++) { 
					echo  $perc_etiqueta[$i];
					if ($i < ($cant_perc-2)) {
						echo ",";
					}
				}
			 ?>],
			title: {
				text:'Percentiles'
			},
			alignTicks: false,
			opposite: true
<?php endif; ?>
		}],
		xAxis: [{
			startOnTick: true,
			type: "datetime",
			dateTimeLabelFormats: {
			    year: "%Y"
			},
			<?php if($db == true): ?>
			categories: [<?php echo $string_col_0 ?>],
			<?php endif; ?>
		labels:{
			step: 1
		},
			title: {
				<?php if ($tabla == 'grupo') : ?>
				text: 'Mes'
				<?php else: ?>
				text: 'Años'
				<?php endif; ?>
			}
		}

		],
		plotOptions: {
			series: {
				// color: 'rgba(0, 0, 255, 0.50)',
				visible: true,
				connectNulls: true
			}
		},
		ignoreHiddenSeries : true,
		<?php if($db == true): ?>
		series: [
		{
			styledMode: true,
			data: [<?php echo $string_col_1 ?>],
			name: "<?php echo $headers[1] ?>",
			color: 'rgba(124, 181, 236,0.9)',
         pointStart: 0
		<?php if ($string_col_2 > 0): ?>
		},{
			type: 'spline',
			data: [<?php echo $string_col_2 ?>],
			name: "<?php echo $headers[2] ?>",
         pointStart: 0
	<?php endif; ?>
		}
	]

	<?php endif; ?>
	});

let largoDatos = chart.series[0].data.length - 1;

// ingresar valor dado por el usuario: 
    chart.series[0].data[largoDatos].update({color: '#E8450C'}, true);

    chart.series[0].data[largoDatos].graphic.attr('fill', '#E8450C')
</script>

<style>
	.highcharts-xaxis-grid .highcharts-grid-line {
	stroke-width: 2px;
	stroke: #d8d8d8;
}
.highcharts-xaxis .highcharts-tick {
	stroke-width: 2px;
	stroke: #d8d8d8;
}
.highcharts-minor-grid-line {
	stroke-dasharray: 2, 2;
}
</style>
