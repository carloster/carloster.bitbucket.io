
<?php

$incluye_highcharts=true;
require_once('conexion_db.php');

require_once('../include/header.php');
require_once('../include/headers_graficos.php');

$meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
$meses_string = '"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"';
//y por que lo carretero? bueno, se debe a que no necesariamente va a haber un locale instalado en la maquina objetivo, asi que porsiaca tenemos este array

if (isset($_GET['nombre_db'])) {
	$nombre_db = $_GET['nombre_db'];
} else{
	$nombre_db = 'rio_elqui_lsc';
}

$keys = array_keys(array_combine(array_keys($headers_precip), array_column($headers_precip, 'nombre_db')),$nombre_db);
$encontrado = $headers_precip;
$tabla = "precip";
$grafico = "spline";
$header = "precipitacion";
$titulo = "Precipitación en ";
if (count($keys) === 0) {
	$keys = array_keys(array_combine(array_keys($headers_caudales), array_column($headers_caudales, 'nombre_db')),$nombre_db);
	$encontrado = $headers_caudales;
	$tabla = "caudales";
	$header = "caudal medio";
	$titulo = "Caudal medio mensual en ";
	if (count($keys) === 0) {
		$keys = array_keys(array_combine(array_keys($headers_situacion), array_column($headers_situacion, 'nombre_db')),$nombre_db);
		$encontrado = $headers_situacion;
		$tabla = "embalses"; //necesario? proq.prommra.cl
		$header = "volumen";
		$titulo = "Situación hídrica en ";
	}
	$grafico = "column";
}

$ubicacion = $keys[0];
$nombre_formal = $encontrado[$ubicacion]['formal'];
$titulo = $titulo . $nombre_formal;
// return;


/*encontrar nombre_db para sacar formal*/

$query = "SELECT fecha, " . $nombre_db . " FROM " . $tabla . " WHERE " . $nombre_db . " >= 0 ORDER BY FECHA ASC;";

$headers = array("fecha", $header);

try {
	$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);

		$cant_headers = count($headers);
		$string_col_0 = '';
		$string_col_1 = '';
		$string_col_2 = '';

		$stmt = $pdo->prepare($query);
		$stmt->execute();
	$i=0;
	$j=0;
	// devolver datos
	foreach ($stmt as $row) {
			foreach ($row as $val) {
				if ($tabla == 'total') {
					if ($i == 0) {
						// $string_col_0 .= $val . "-";
						// $temp = $val . "-";
						$string_col_0 .= "\"" .$val . "\",";
					} else if ($i == 1) {
						// $temp .= $val . "-1";
						// $temp_ts = strtotime($temp);
						// $string_col_0 .= $val . "-1,";
						$string_col_1 .= $val . ",";
					} else if ($i == 2) {
						$string_col_2 .= $val . ",";
					}
				} else{
					if ($i == 0) {
						$string_col_0 .= "\"" .$val . "\",";
					} else if ($i == 1) {
						$string_col_1 .= $val . ",";
					} else if ($i == 2) {
						$string_col_2 .= $val . ",";
					}
				}
				$i++;
			}
		$j++;
		$i=0;
	}
		$string_col_0 = rtrim($string_col_0,',');
		$string_col_1 = rtrim($string_col_1,',');
		$string_col_2 = rtrim($string_col_2,',');

// percentiles

// echo $string_perc_etiqueta;
// echo $string_perc_valor;

} catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

?>
<div id="visor" style="height: 650px; width: 1200px;"></div>
<script>

var tabla, titulo;

function hexToRGB(h) {
  let r = 0, g = 0, b = 0;

  // 3 digits
  if (h.length == 4) {
    r = "0x" + h[1] + h[1];
    g = "0x" + h[2] + h[2];
    b = "0x" + h[3] + h[3];

  // 6 digits
  } else if (h.length == 7) {
    r = "0x" + h[1] + h[2];
    g = "0x" + h[3] + h[4];
    b = "0x" + h[5] + h[6];
  }
  
  return "rgb("+ +r + "," + +g + "," + +b + ")";
}



<?php if ($tabla == 'total'): ?>
	let chart = Highcharts.stockChart('visor', {
<?php else: ?>
	let chart = Highcharts.chart('visor', {
<?php endif; ?>
		chart:{
			// type: 'spline'
			zoomType: 'xy'
		},
		title: {
			text: "<?php echo $titulo ?>"
		},
		yAxis: [{
			title: {
				text: 'caudal [m<sup>3</sup>]'
			},
			<?php if($tabla == 'precip'): ?>
			max: 300,
			<?php elseif($tabla == 'caudal'): ?>
			max: 30,
			<?php else: ?>
			max: 100,
			<?php endif; ?>
			min: 0,
			startOnTick: false,
			endOnTick: false,
			alignTicks: true,
						plotLines:[{
							color: '#FF0000',
							value: 0,
							width: 2,
							dashStyle: 'Solid'
						}
			],
		}
			<?php if(($tabla != 'total') || ($tabla != 'pp')  || ($tabla != 'mayo')  || ($tabla != 'agosto') ): ?>
		,{
    			// categories: [],
			title: {
				text:'Percentiles'
			},
			// startOnTick: true,
			// endOnTick: true,
			alignTicks: false,
			// lineWidth: 3,
			opposite: true
<?php endif; ?>
		}],
		xAxis: [{
			// type: 'logarithmic',
			startOnTick: false,
			categories: [<?php echo $string_col_0 ?>],
			title: {
				<?php if ($tabla == 'grupo') : ?>
				text: 'Mes'
				<?php else: ?>
				text: 'Años'
				<?php endif; ?>
			}
		}],
		plotOptions: {
			series: {
				visible: true
			}
		},
		connectNulls: true,
		ignoreHiddenSeries : true,
		series: [{
			type: 'spline',
			styledMode: true,
			data: [<?php echo $string_col_1 ?>],
			name: "<?php echo $headers[1] ?>",
        	pointStart: 0
		<?php if ($string_col_2 > 0): ?>
			},{
			type: 'spline',
			data: [<?php echo $string_col_2 ?>],
			name: "<?php echo $headers[2] ?>",
        	pointStart: 0
        <?php endif; ?>
        }]
	});
</script>
