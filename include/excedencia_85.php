<?php

//script para la curva temporada del 85% de cada mes
// ._.
// 
// 

// primero, recorrer los datos....
// ._____.

// tabla = caudales (este script, y la curva del 85%, solo son necesarios en caudales)
// variable get = el rio
// variable iterable = el mes

function sacarCercano($buscar, $arr) {
   $cercano = null;
   foreach ($arr as $item) {
   	// echo $item . "<br>\n";
      if ($cercano === null || abs($buscar - $cercano) > abs($item - $buscar)) {
         $cercano = $item;
      }
   }
   return $cercano;
}

if (isset($_GET['nombre_db']) && $_GET['nombre_db'] !== 'undefined') {
	$nombre_db = $_GET['nombre_db'];
} else{
	$nombre_db = 'rio_elqui_lsc';
}

if ($nombre_db == 'cmm') {
	$tabla = 'caudales_temporada';
	$nombre_db = 'cmm';
	# code...
} else{
	$tabla = 'caudales_temporada';
}

$curva = array_fill(1, 12, 0);

for ($i=1; $i < 13; $i++) {
	if ($nombre_db == 'cmm') {
		$tabla = 'laguna';
	}
	$sql = "SELECT avg(" . $nombre_db . ") as promedio,EXTRACT('year' FROM fecha) as año FROM " . $tabla . " WHERE EXTRACT('month' FROM fecha) = " . $i . " AND " . $nombre_db . " IS NOT NULL AND " . $nombre_db . " > 0 GROUP BY año ORDER BY promedio DESC";

	$resultados[$i] = array();
	$percentiles[$i] = array();

	// generar query, recibir datos y determinar el percentil 85



	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);

		$string_col_0 = '';
		$string_col_1 = '';
		$string_col_2 = '';
		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$j=1;
		$k=0;

		// k = 0 -> promedio
		// k = 1 -> año
		
		$filas=$stmt->rowCount();

		// CONTAR FILAS
		// $FILAS = FILAS + 1
		// PROB. EXCEDENCIA: $J/$FILAS
		foreach ($stmt as $row) {
				foreach ($row as $val) {
					if ($k == 0) {
						array_push($resultados[$i], number_format($val,3));
						array_push($percentiles[$i], (number_format(($j/($filas+1)),2)));
					}
					if ($k == 1) {
						$string_col_2 .= "\"" . $val . "\",";

					}
					$k++;
				}
			$j++;
			$k=0;
		}

		$string_col_2 = rtrim($string_col_2,',');
			// echo "~~~" . $j . "~~~";
			// echo $string_col_2;

	// percentiles

	$cercano = sacarCercano(0.85,$percentiles[$i]);
	$valor_percentil = array_search($cercano, $percentiles[$i]);
	$cercanoEncontrado = $resultados[$i][$valor_percentil];
	$curva[$i] = $cercanoEncontrado;

	// echo $string_perc_etiqueta;
	// echo $string_perc_valor;

	} catch (\PDOException $e) {

	     throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}

}

// print_r($curva);
// $meses =           array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
// $meses_temporada = array('Septiembre','Octubre','Noviembre','Diciembre','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto');
$meses_temporada = "\"Septiembre\",\"Octubre\",\"Noviembre\",\"Diciembre\",\"Enero\",\"Febrero\",\"Marzo\",\"Abril\",\"Mayo\",\"Junio\",\"Julio\",\"Agosto\"";
$valores = '';
$reordenar_curva = array_fill(1,12,0);
$j = 9;

for ($i=1; $i < 13; $i++) { 
	$valores .= $curva[$i] . ',';
}

// esto reordena los meses, es decir, hace el cambio de septiembre a "mes 1" y agosto a "mes 12"
// for ($i=1; $i < 13; $i++) { 
// 	if ($j == 13) {
// 		$j = 1;
// 	}

// 	$reordenar_curva[$i] = $curva[$j];
// 	$valores .= $curva[$j] . ',';
// 	$j++;
// }

$valores = rtrim($valores,',');

$string_col_0 = $meses_temporada;
$string_col_1 = $valores;
$tipo_grafico = 'spline';
$eje_y = "Caudal medio mensual [m<sup>3</sup>/s]";
$eje_x = "Meses";
$unidad_excedencia = "[m<sup>3</sup>/s]";

// echo $sql;

// print_r($reordenar_curva);


// return;
