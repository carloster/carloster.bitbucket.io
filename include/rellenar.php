<?php 

// devuelve el ultimo valor no nulo de una medicion, implementado para rellenar de forma inicial los inputs en desmarque.php
require_once 'conexion_db.php';

if (isset($_GET['tabla'])) {
	$tabla = $_GET['tabla'];
} else{
	$tabla = 'cmm';
}

if (isset($_GET['mes'])) {
	$mes = $_GET['mes'];

} else{
	$mes = 8;
}

if (isset($_GET['rio'])) {
	$rio = $_GET['rio'];
	$rio = strtoupper($rio);
} else{
	$rio = 'RLLEE';
}

if (isset($_GET['estacion'])) { //opcional
	$estacion = $_GET['estacion'];
} else{
	$estacion = 'cochiguaz';
}

try{


	if ($tabla == 'ppeq') {

		$query = " SELECT pp_equiv from ppeq where pp_equiv IS NOT NULL and pp_equiv > -1 order by anio desc limit 1;";
	} elseif ($tabla == 'alt_nieve') {
				
		$query = " SELECT alt_nieve from laguna where alt_nieve IS NOT NULL and alt_nieve > -1 order by fecha desc limit 1;";
	} elseif ($tabla == 'cmm' && (isset($mes) && isset($rio))) {

		$query = "SELECT " . $nombre_rio . " FROM caudales t WHERE " . $nombre_rio . " IS NOT NULL AND " . $nombre_rio . " > -1 AND EXTRACT('month' from fecha) = " .$mes . " ORDER BY fecha DESC limit 1;";
	} elseif ($tabla == 'laguna') {

		$query = "SELECT cmm FROM laguna WHERE cmm IS NOT NULL AND cmm > -1 and EXTRACT('month' FROM fecha) = " . $mes . " ORDER BY fecha DESC limit 1;";

	} elseif ($tabla == 'pp_acum' && isset($estacion)) {

		$query = "SELECT " . $estacion . " FROM pp_acum t WHERE " . $estacion . " IS NOT NULL AND " . $estacion . " > -1 > 1988 and " . $estacion . " = (select MAX(" . $estacion . ") AS precipitacion FROM pp_temporadas WHERE date_part('year', fecha) > 1988);";
	} elseif ($tabla == 'vtemporada') {

		$query = " SELECT vtemporada from laguna where vtemporada IS NOT NULL and vtemporada > -1 order by fecha desc limit 1;";
	} elseif ($tabla == 'laguna_31_agosto') {
	
		$query = " SELECT laguna_31_agosto from laguna_31 where laguna_31_agosto IS NOT NULL and laguna_31_agosto > -1 order by fecha desc limit 1;";
	} elseif ($tabla == 'puclaro_31_agosto') {

		$query = " SELECT puclaro_31_agosto from puclaro_31 where puclaro_31_agosto IS NOT NULL and puclaro_31_agosto > -1 order by fecha desc limit 1;";
	}
} catch(Exception $e){
   echo 'Message: ' .$e->getMessage();
}

	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);
		$stmt = $pdo->prepare($query);
		$stmt->execute();

		foreach ($stmt as $row) {
			echo $row['año'] . "," . $row['maximo'];
		}


	} catch (\PDOException $e) {

		echo "\n\n<br><br>";
		echo $query;
		echo "\n\n<br><br>";
	     throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
?>