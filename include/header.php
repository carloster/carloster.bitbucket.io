<?php 
$boton_volver = false;
 ?>
	<meta charset="UTF-8">
	<!-- viewport (responsive?) -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- **********************LIBRERIAS CSS*********************** -->
	<link rel="stylesheet" href="<?php __DIR__ ?>/font/metropolis/stylesheet.css"> <!-- webfont -->
	<!-- Font Awesome-->
	<link rel="stylesheet" href="<?php __DIR__ ?>/fontawesome/css/all.css">
	<!-- Bootstrap core CSS -->
	<link href="<?php __DIR__ ?>/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="<?php __DIR__ ?>/css/mdb.css" rel="stylesheet">
	<!-- el estilo custom (scss compilado) -->
	<link rel="stylesheet" href="<?php __DIR__ ?>/css/style_sasi.css">
	<link rel='stylesheet' media='(max-width: 1919px)' href="<?php __DIR__ ?>/css/small.css">
	<!-- ***********************LIBRERIAS JS*********************** -->
	<!-- JQuery -->
	<script type="text/javascript" src="<?php __DIR__ ?>/js/jquery-3.4.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="/js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="<?php __DIR__ ?>/js/bootstrap.min.js"></script>
<?php if (isset($incluye_highcharts) && ($incluye_highcharts == true)): ?>
	<script src="<?php __DIR__ ?>/js/highstock.js"></script>
	<script src="<?php __DIR__ ?>/js/data.js"></script>
<?php endif ?>
<?php if (isset($incluye_rop) && ($incluye_rop == true)): ?>
	<script type="text/javascript" src="/js/html2canvas.min.js"></script>
	<!-- <script type="text/javascript" src="/js/jspdf.min.js"></script> -->
	<script type="text/javascript" src="/js/jspdf.debug.js"></script>
	<script src="<?php __DIR__ ?>/js/arboles.js"></script>
	<script src="<?php __DIR__ ?>/js/rop.js"></script>
<?php endif ?>
