<?php 
// plantilla de banner

 ?>
<?php if (isset($inicio) && ($inicio == false)): ?>
<?php if (isset($fondo) && $fondo =='blanco'): ?>
	<div class="card card-image overflow-hidden h-20">	
		<?php $texto = 'texto-admin'; ?>
<?php else: ?>
		<?php $texto = 'texto-item'; ?>
	<div class="titulo h-10">
<?php endif ?>
		<div class="row h-100">
			<div class="col-3">
				<div class="text-center h-100 align-pulento">
					<?php if((($pag_admin == false) && !isset($sub_admin)) && $boton_volver === false  ): ?>
					<a role="button" class="d-inline-block align-pulento btn btn-elegant btn-rounded btn-inicio" href="/menu.php"> &lt;&lt; Inicio</a>
					<?php elseif((isset($sub_admin) && $sub_admin == true) || $boton_volver === true): ?>
					<a role="button" class="d-inline-block align-pulento btn btn-elegant btn-rounded btn-inicio" onClick="javascript:history.go(-1)"> &lt;&lt; Volver</a>
					<?php else: ?>
					<a role="button" class="d-inline-block align-pulento btn btn-elegant btn-rounded btn-inicio" href="/admin/index.php"> &lt;&lt; Volver</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-6">
				<h2 class="align-pulento">
					<span class="d-inline-block h1 <?php echo $texto ?>"><?=$banner;?></span>
				</h2>				
			</div>
			<div class="col-2">
				<form action="/index.php" class="h-100 align-pulento" method="POST">
					<h4 class="d-inline-block <?php echo $texto ?> align-pulento">
						<?php if(isset($nombre)): ?>
						Bienvenido(a) <?=$nombre;?> <input class="d-inline-block btn-paleta" type="submit" value="Salir" name="salir" id="salir">
					<?php endif; ?>
					</h4>
						
				</form>
			</div>
		</div>
	</div>
<?php endif ?>

<style>
	.boton_salida{
		display: inline;
	}
</style>

<!-- <div class="row">
	<div class="col-8"></div>
	<div class="col-4">

	</div>
</div>
<hr>
 -->
