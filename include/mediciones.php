<?php 
require_once('../include/conexion_db.php'); //necesario? hacer una llamada a otro php?


if (isset($_GET['tabla'])) {
	$tabla = $_GET['tabla'];
}
// $tabla = 'laguna';
	$query = "SELECT * FROM " . $tabla . " ORDER BY 1 ASC;";

	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);
		$stmt = $pdo->query($query);
		$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode($datos);
		// $stmt = $pdo->prepare($query);
		// $stmt->execute();
	} catch (\PDOException $e) {
		throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}

// }