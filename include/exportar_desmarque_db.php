<?php

include 'conexion_db.php';

$data = json_decode(stripslashes($_POST['data']));

$nombresModeloHidrologico = array("volumen_afluente", "Vt1", "dm1", "volumen_puclaroProyectado1");
$nombresArbolMet = array("altura_nieve_laguna", "precipitacion_laguna", "precipitacion_cochiguaz", "precipitacion_ortiga", "Vt2", "dm2", "volumen_puclaroProyectado2");
$nombresArbolHidro = array("rtv_agosto", "rle_mayo", "rea_agosto", "rea_mayo", "Vt3", "dm3", "volumen_puclaroProyectado3");
$nombresArbolHidroMet = array("precipitacion_laguna", "altura_nieve_laguna", "rcr_agosto", "precipitacion_cochiguaz", "rle_agosto", "rcr_mayo", "rle_mayo", "Vt4", "dm4", "volumen_puclaroProyectado4");
$usuario = $data[0];

$cant_arboles = count($data);

for ($i=1; $i < $cant_arboles; $i++) {
    $cant_datos = count($data[$i]);
    $string[$i-1] = '';
    for ($j=0; $j < $cant_datos; $j++) {
        if (gettype($data[$i][$j]) == 'string') {@
            $string[$i-1] .= "\"" . $data[$i][$j] . "\",";
        } else{
            $string[$i-1] .= $data[$i][$j] . ",";
        }
    }
    $string[$i-1]=rtrim($string[$i-1],',');
}

$tiempo = time();



$query[0]   = "INSERT INTO modelohidro(tiempo,usuario,volumen_entrada,volumen_temporada,desmarque,vol_puclaro_proyectado) values(" . $tiempo  . ",'" . $usuario . "'," . $string[0] . ") "; //esto cambia segun arbol...

$query[1]      = "INSERT INTO arbolmet(tiempo,usuario,altura_nieve_laguna, precipitacion_laguna, precipitacion_cochiguaz, precipitacion_ortiga,volumen_temporada,desmarque,vol_puclaro_proyectado) values(" . $tiempo . ",'" . $usuario . "'," . $string[1] . ") "; //esto cambia segun arbol...

$query[2]    = "INSERT INTO arbolhidro(tiempo,usuario,rio_turbio_varillar_agosto, rio_la_laguna_mayo, rio_elqui_algarrobal_agosto, rio_elqui_algarrobal_mayo,volumen_temporada,desmarque,vol_puclaro_proyectado) values(" . $tiempo . ",'" . $usuario . "'," . $string[2] . ") "; //esto cambia segun arbol...

$query[3] = "INSERT INTO arbolhidromet(tiempo,usuario,volumen_entrada,precipitacion_laguna, altura_nieve_laguna, rio_claro_rivadavia_agosto, precipitacion_cochiguaz, rio_la_laguna_agosto, rio_claro_rivadavia_mayo, rio_la_laguna_mayo,desmarque,vol_puclaro_proyectado) values(" . $tiempo . ",'" . $usuario . "'," . $string[3] . ") "; //esto cambia segun arbol...

$cant_arboles = $cant_arboles - 1;
echo ($query[0] . "\n");
echo ($query[1] . "\n");
echo ($query[2] . "\n");
echo ($query[3] . "\n");

$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);

for ($i=0; $i < $cant_arboles; $i++) { 
    try {
        $stmt = $pdo->prepare($query[$i]);
        $stmt->execute();
    } catch (\PDOException $e) {
        echo "\n\n<br><br>";
        echo $query[$i];
        echo "\n\n<br><br>";
         throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
}

