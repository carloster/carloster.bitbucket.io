<?php 

require_once 'conexion_db.php';


$meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');

$rios = array(
	'RTV' => 'rio_turbio_varillar',
	'REA' => 'rio_elqui_algarrobal',
	'RCR' => 'rio_claro_rivadavia',
	'RLLEE' => 'rio_laguna_entrada_embalse'
);

$rios_verbose = array(
	'RTV' => 'Rio Turbio en Varillar',
	'REA' => 'Rio Elqui en Algarrobal',
	'RCR' => 'Rio Claro en Rivadavia',
	'RLLEE' => 'Rio La Laguna entrada Embalse La Laguna'
);

$estacion_verbose = array(
	'cochiguaz' => 'Estacion Cochiguaz DGA',
	'la_ortiga' => 'Estacion La Ortiga DGA'
);

if (isset($_GET['tabla'])) {
	$tabla = $_GET['tabla'];
} else{
	$tabla = 'cmm';
}

if (isset($_GET['mes'])) {
	$mes = $_GET['mes'];

} else{
	$mes = 8;
}

if (isset($_GET['rio'])) {
	$rio = $_GET['rio'];
	$rio = strtoupper($rio);
} else{
	$rio = 'RLLEE';
}

if (isset($_GET['estacion'])) { //opcional
	$estacion = $_GET['estacion'];
} else{
	$estacion = 'cochiguaz';
}

$nombre_mes = $meses[$mes-1];
$nombre_rio = $rios[$rio];

try{


	if ($tabla == 'ppeq') {

		$query = "
		SELECT
		date_part('year', laguna.fecha) AS año,
		pp_equiv as maximo
		from
			public.laguna
		where
			pp_equiv = (
				select
					MAX(pp_equiv)
				from
					public.laguna
				where
					pp_equiv IS NOT NULL
				);";
		
	} elseif ($tabla == 'alt_nieve') {
		
		$query = "SELECT date_part('year', laguna.fecha) AS año, alt_nieve as maximo from public.laguna where alt_nieve = (select MAX(alt_nieve) from public.laguna where alt_nieve IS NOT NULL);";
		
	} elseif ($tabla == 'cmm' && (isset($mes) && isset($rio))) {

		$query = "
	SELECT 
		 	date_part('year', fecha) as año,
		 	" . $nombre_rio . " as maximo
	 	FROM
	 		caudales t
		WHERE
			" . $nombre_rio . "= (select MAX(" . $nombre_rio . ") from caudales WHERE
			date_part('month', fecha) = " . $mes . "
		AND
			date_part('year', fecha) >= 1989
	);";
	
	} elseif ($tabla == 'laguna') {

		$query = "
	SELECT 
		 	date_part('year', fecha) as año,
		 	cmm as maximo
	 	FROM
	 		laguna t
		WHERE
			cmm= (select MAX(cmm) from laguna WHERE
			date_part('month', fecha) = " . $mes . "
		AND
			date_part('year', fecha) >= 1989
	);";
		
	} elseif ($tabla == 'pp_acum' && isset($estacion)) {

		$query = "SELECT *
FROM(
	SELECT 
		EXTRACT('year' FROM fecha) AS año,
		SUM(" . $estacion . ") AS maximo
	FROM precip 
	WHERE EXTRACT('year' FROM fecha) > 1988 
	AND EXTRACT('month' FROM fecha) >= 1 
	AND EXTRACT('month' FROM fecha) < 9 
	GROUP BY EXTRACT('year' FROM fecha) 
	ORDER BY EXTRACT('year' FROM fecha) ASC --año, suma
) AS temporadas
WHERE maximo = (
	SELECT
		MAX(suma) AS precipitacion
 	FROM
 	(
 		SELECT 
 			EXTRACT('year' FROM fecha) AS año,
			SUM(" . $estacion . ") AS suma
		FROM precip 
		WHERE EXTRACT('year' FROM fecha) > 1988 
		AND EXTRACT('month' FROM fecha) >= 1 
		AND EXTRACT('month' FROM fecha) < 9 
		GROUP BY EXTRACT('year' FROM fecha) 
		ORDER BY EXTRACT('year' FROM fecha) ASc
	) AS temp_maximos
);";


	} elseif ($tabla == 'ppElqui') {

		$query = "SELECT 
			anio AS año,
		 	precipitacion AS maximo
	 	FROM
	 		pisco_elqui_ceaza
		WHERE
			anio > 1988
		and
			precipitacion = (select MAX(precipitacion)
	 	FROM
	 		pisco_elqui_ceaza
		WHERE
			anio > 1988
			);";

	} elseif ($tabla == 'vtemporada') {

		$query = "SELECT año,vtemporada as maximo FROM vtemporada WHERE vtemporada = (select max(vtemporada) FROM vtemporada) order by año desc limit 1";
	} elseif ($tabla == 'laguna_31_agosto') {
	
		$query = "SELECT fecha as año,laguna_31_agosto as maximo FROM public.laguna_31 WHERE laguna_31_agosto = (select max(laguna_31_agosto) FROM public.laguna_31) order by año desc limit 1";
	} elseif ($tabla == 'puclaro_31_agosto') {

		$query = "SELECT fecha as año,puclaro_31_agosto as maximo FROM public.puclaro_31 WHERE puclaro_31_agosto = (select max(puclaro_31_agosto) FROM public.puclaro_31 WHERE puclaro_31_agosto IS NOT NULL) order by año desc limit 1";
	}
} catch(Exception $e){
   echo 'Message: ' .$e->getMessage();
}

	try {
		$pdo = new PDO($dsn_datos, $user_datos, $pass_datos, $options_datos);
		$stmt = $pdo->prepare($query);
		$stmt->execute();

		foreach ($stmt as $row) {
			echo $row['año'] . "," . $row['maximo'];
		}


	} catch (\PDOException $e) {

		echo "\n\n<br><br>";
		echo $query;
		echo "\n\n<br><br>";
	     throw new \PDOException($e->getMessage(), (int)$e->getCode());
	}
?>