<?php 

if (isset($_GET['nombre_compuerta'])) {
	$nombre_compuerta = $_GET['nombre_compuerta'];
} else{
	echo 'error, no se pudo recibir un nombre de compuerta.';
	return;
}

foreach (json_decode(file_get_contents('../data/csv/compuertas.json'),true) as $fila) if ($fila['nombre_entrada'] === $nombre_compuerta) echo $fila['lat'] . ', ' . $fila['lon'];