<?php 

// headers, placeholders...

// para buscar en amplia profundidad de array (dic. array multidimensional), esto sirve 
// $keys = array_keys(array_combine(array_keys($headers_caudales), array_column($headers_caudales, 'nombre_db')),"rio_elqui_algarrobal"); 
// $prueba = $keys 
// print_r($prueba) : Array ( [0] => 3 ) // esta en el array [3] 


$headers_situacion = array(
	array(
		"nombre_db" => "puclaro_31",
		"formal" => "Embalse Puclaro"),
	array(
		"nombre_db" => "laguna_31",
		"formal" => "Embalse La Laguna")
);

$headers_precip = array(
	array(
		"nombre_db" => "la_ortiga",
		"formal" => "La Ortiga"),
	array(
		"nombre_db" => "vicuna_inia",
		"formal" => "Vicuña (Inia)"),
	array(
		"nombre_db" => "lsc_escuela_agricola",
		"formal" => "La Serena (Escuela Agricola)"),
	array(
		"nombre_db" => "monte_grande",
		"formal" => "Monte Grande"),
	array(
		"nombre_db" => "pisco_elqui_dmc",
		"formal" => "Pisco Elqui DMC"),
	array(
		"nombre_db" => "cochiguaz",
		"formal" => "Cochiguaz"),
	array(
		"nombre_db" => "la_laguna_embalse",
		"formal" => "La Laguna Embalse"),
);

$headers_caudales = array(
	// array(
	// 	"nombre_db" => "rio_elqui_lsc",
	// 	"formal" => "Rio Elqui En La Serena"),
	array(
		"nombre_db" => "rio_turbio_varillar",
		"formal" => "Rio Turbio En Varillar"),
	array(
		"nombre_db" => "rio_elqui_almendral",
		"formal" => "Rio Elqui En Almendral"),
	array(
		"nombre_db" => "rio_elqui_algarrobal",
		"formal" => "Rio Elqui En Algarrobal"),
	array(
		"nombre_db" => "rio_cochiguaz_penon",
		"formal" => "Rio Cochiguaz En El Peñon"),
	array(
		"nombre_db" => "rio_la_laguna_salida_embalse",
		"formal" => "Rio La Laguna En Salida Embalse La Laguna"),
	array(
		"nombre_db" => "cmm",
		"formal" => "Rio La Laguna En Entrada Embalse La Laguna"),
	array(
		"nombre_db" => "estero_derecho_alcohuaz",
		"formal" => "Estero Derecho En Alcohuaz")
);



$headers_global = array(
	array(
		"nombre_db" => "la_ortiga",
		"formal" => "***Precipitaciones***",
		"tabla" => "precip_temporada"),
	array(
		"nombre_db" => "la_ortiga",
		"formal" => "La Ortiga",
		"tabla" => "precip_temporada"),
	array(
		"nombre_db" => "vicuna_inia",
		"formal" => "Vicuña (Inia)",
		"tabla" => "precip_temporada"),
	array(
		"nombre_db" => "lsc_escuela_agricola",
		"formal" => "La Serena (Escuela Agricola)",
		"tabla" => "precip_temporada"),
	array(
		"nombre_db" => "monte_grande",
		"formal" => "Monte Grande",
		"tabla" => "precip_temporada"),
	array(
		"nombre_db" => "pisco_elqui_dmc",
		"formal" => "Pisco Elqui DMC",
		"tabla" => "precip_temporada"),
	array(
		"nombre_db" => "cochiguaz",
		"formal" => "Cochiguaz",
		"tabla" => "precip_temporada"),
	array(
		"nombre_db" => "la_laguna_embalse",
		"formal" => "La Laguna Embalse",
		"tabla" => "precip_temporada"),
	array(
		"nombre_db" => "rio_elqui_lsc",
		"formal" => "***Caudales***",
		"tabla" => "caudales_temporada"),
	array(
		"nombre_db" => "rio_elqui_lsc",
		"formal" => "Rio Elqui En La Serena",
		"tabla" => "caudales_temporada"),
	array(
		"nombre_db" => "rio_turbio_varillar",
		"formal" => "Rio Turbio En Varillar",
		"tabla" => "caudales_temporada"),
	array(
		"nombre_db" => "rio_elqui_almendral",
		"formal" => "Rio Elqui En Almendral",
		"tabla" => "caudales_temporada"),
	array(
		"nombre_db" => "rio_elqui_algarrobal",
		"formal" => "Rio Elqui En Algarrobal",
		"tabla" => "caudales_temporada"),
	array(
		"nombre_db" => "rio_cochiguaz_penon",
		"formal" => "Rio Cochiguaz En El Peñon",
		"tabla" => "caudales_temporada"),
	array(
		"nombre_db" => "rio_la_laguna_salida_embalse",
		"formal" => "Rio La Laguna En Salida Embalse La Laguna",
		"tabla" => "caudales_temporada"),
	array(
		"nombre_db" => "cmm",
		"formal" => "Rio La Laguna En Entrada Embalse La Laguna",
		"tabla" => "caudales_temporada"),
	array(
		"nombre_db" => "estero_derecho_alcohuaz",
		"formal" => "Estero Derecho En Alcohuaz",
		"tabla" => "caudales_temporada"),
	array(
		"nombre_db" => "puclaro_31",
		"formal" => "***Situación de embalses***"),
	array(
		"nombre_db" => "puclaro_31",
		"formal" => "Embalse Puclaro"),
	array(
		"nombre_db" => "laguna_31",
		"formal" => "Embalse La Laguna")
);



$cant_caudales = count($headers_caudales);
$cant_precip = count($headers_precip);
$cant_situacion = count($headers_situacion);
$cant_global = count($headers_global);
