/* jshint esversion: 6, asi: true, -W069, -W010, -W033 */


function main(volumen_afluente, altura_nieve_laguna, precipitacion_laguna, precipitacion_cochiguaz, precipitacion_ortiga, rtv_agosto, rle_mayo, rea_agosto, rea_mayo, rcr_agosto, rle_agosto, rcr_mayo, rtv_mayo){
    // console.log(precipitacion_ortiga + '~' + precipitacion_cochiguaz + '~' + precipitacion_laguna + '~' + altura_nieve_laguna);

    let Vt1, Vt2, Vt3, Vt4;
    let pendienteLLEE, pendienteREA, pendienteRTV;
    pendienteLLEE = rle_agosto - rle_mayo;
    pendienteREA = rea_agosto - rea_mayo;
    pendienteRTV = rtv_agosto - rtv_mayo;
    Vt1 = ModeloHidro(volumen_afluente);
    Vt2 = arbolMet(altura_nieve_laguna, precipitacion_laguna, precipitacion_cochiguaz, precipitacion_ortiga);
    Vt3 = arbolHidro(pendienteREA, rtv_agosto, pendienteLLEE, rle_mayo, rea_agosto, rea_mayo);
    Vt4 = arbolHidroMet(precipitacion_laguna, altura_nieve_laguna, pendienteRTV, pendienteREA, rcr_agosto, precipitacion_cochiguaz, rle_agosto, rcr_mayo, rle_mayo);

    

// console.log(volumen_afluente);


    let Vt = new Object()
        Vt['Vt1'] = Vt1;
        Vt['Vt2'] = Vt2;
        Vt['Vt3'] = Vt3;
        Vt['Vt4'] = Vt4;
    return Vt;
}


function ModeloHidro(volumen_afluente) {
    // console.log('calculando segun modelo hidrologico...');
    if (volumen_afluente > 40) {
        volumen_afluente = 40;
    }
    expo = 0.0135 * volumen_afluente;
    Vt1 = Math.round(137.23 * Math.pow(Math.E,expo) * 1000)/1000;
    Vt1 = Vt1.toFixed(1);
    // console.log('valor Vt1: ' + Vt1);
    return Vt1;
}


/////////////////////////
// arbol meteorologico //
/////////////////////////

// alternativa de ternarios...no pescar
// function tArbolMet(precipitacion_ortiga,precipitacion_cochiguaz,precipitacion_laguna,altura_nieve_laguna) {//   let Vt1; //     Vt2 = precipitacion_ortiga < 300 //     ? altura_nieve_laguna < 174 //      ? precipitacion_laguna < 66 //             ? 170 //            : altura_nieve_laguna >= 154 //                 ? 176 //                : precipitacion_cochiguaz >= 28 //                  ? precipitacion_laguna < 108 //                        ? 207 //                        : 237 //                    : 290 //        : altura_nieve_laguna < 336 //          ? precipitacion_ortiga < 232 //                 ? altura_nieve_laguna >= 182 //                     ? precipitacion_cochiguaz >= 172 //                         ? 291 //                        : 333 //                    : 379 //                : 383 //            : 466 //    : 1005 //   return Vt2 // }

function arbolMet(altura_nieve_laguna, precipitacion_laguna, precipitacion_cochiguaz, precipitacion_ortiga) {

    let Vt2;

    // console.log('calculando segun arbol meteorologico...');

    // posibles salidas:
    // 160, 204, 210, 220, 260, 309, 340, 417, 406, 586, 467, 568, 765, 1244

                                if (altura_nieve_laguna < 422) {
        if (altura_nieve_laguna < 174) {
            if (precipitacion_laguna < 61) {
                if (altura_nieve_laguna < 60)          Vt2 = 160; else Vt2 = 204;
            } else {
                if (precipitacion_laguna >= 115)          Vt2 = 210; else {
                    if (precipitacion_laguna < 108)       Vt2 = 220; else {
                        if (precipitacion_cochiguaz >= 53) Vt2 = 260; else Vt2 = 309;
                    }
                }
            }
        } else{
            if (altura_nieve_laguna < 336) {
                if (altura_nieve_laguna >= 187) {
                    if (precipitacion_ortiga < 232)    Vt2 = 340; else Vt2 = 417;
                } else {
                    if (altura_nieve_laguna < 182)     Vt2 = 406; else Vt2 = 586;
                }
            } else{
                if (altura_nieve_laguna >= 386)        Vt2 = 467; else Vt2 = 568
            }
        }
    } else {
        if (altura_nieve_laguna < 720)                 Vt2 = 765; else Vt2 = 1244;
    }

    // console.log('valor Vt2: ' + Vt2);

    return Vt2;
}

///////////////////////
// arbol hidrologico //
///////////////////////

function arbolHidro(pendienteREA, rtv_agosto, pendienteLLEE, rle_mayo, rea_agosto, rea_mayo) {
    let Vt3;
//go nuts

    // posibles salidas:
    // 173, 213, 291, 229, 302, 239, 338, 368, 437, 1005
    // console.log('calculando segun arbol hidrologico...');

                                                    if (pendienteREA < 4.9) {
                            if (rtv_agosto < 2.7) {
        if (pendienteLLEE < -0.11)

    Vt3 = 170; else Vt3 = 206;
                                                } else {
                                            if (pendienteLLEE < -0.2) {
                        if (rle_mayo < 1.7) {
                                                if (pendienteREA >= -1.1)
    Vt3 = 217; else Vt3 = 256;
                                            } else {
                                                        if (pendienteLLEE < -0.42)


    Vt3 = 322; else Vt3 = 386;
                                                    }
                                                                        } else {
                        if (rle_mayo < 1.1) {
        if (rea_agosto < 6.1)

    Vt3 = 349; else Vt3 = 429;
                                                } else{
                                                                    if (pendienteREA < -0.77) {
        if (rea_mayo >= 11)

    Vt3 = 406; else Vt3 = 467;
    } else Vt3 = 577;
                                                                    }
                                                    }
                                            }
                                                                            } else {
        if (rea_mayo >= 5.6)
    Vt3 = 765; else Vt3 = 1244;
                                                                                    }
    


    // console.log('valor Vt3: ' + Vt3);

    return Vt3;
}

/////////////////////
// arbol combinado //
/////////////////////

function arbolHidroMet(precipitacion_laguna, altura_nieve_laguna, pendienteRTV, pendienteREA, rcr_agosto, precipitacion_cochiguaz, rle_agosto, rcr_mayo, rle_mayo) {

    let Vt4;
    // posibles salidas:
    // 173, 228, 290, 379, 291, 333, 383, 466, 765, 1244
    // console.log('calculando segun arbol hidrometeorologico...');
    // console.log('RTV  ' + pendienteRTV);

    if (precipitacion_laguna < 292) {
        if (altura_nieve_laguna < 177) {
            if (pendienteRTV >= -2.3) {
                if (pendienteREA >= -1.1) {
                    if (rcr_agosto < 1.1) Vt4 = 148; else {
                        if (precipitacion_cochiguaz < 32) {
                            if (altura_nieve_laguna < 96) Vt4 = 173; else Vt4 = 190;
                        } else {
                            if (rle_agosto < 0.94) {
                                if (rcr_agosto < 1.6) Vt4 = 197; else Vt4 = 212;
                            } else Vt4 = 216;
                        }
                    }
                } else {
                    if (rcr_mayo >= 2.6) Vt4 = 249; else Vt4 = 269;
                }
            } else {
                if (altura_nieve_laguna < 139) Vt4 = 309; else Vt4 = 406;
            }
        } else {
            if (rle_mayo < 1.6) {
                if (rcr_agosto >= 2.1) {
                    if (precipitacion_ortiga < 232) {
                        if (rcr_agosto >= 3.1) Vt4 = 336; else Vt4 = 359;
                    } else {
                        if (altura_nieve_laguna >= 237) Vt4 = 405; else Vt4 = 429;
                    }
                } else Vt4 = 467;
            } else {
                if (altura_nieve_laguna >= 272) Vt4 = 568; else Vt4 = 586;
            }
        }
    } else {
        if (rcr_mayo >= 2.4) Vt4 = 765; else Vt4 = 1244;
    }

    // console.log('valor Vt4: ' + Vt4);

    return Vt4;
}

//