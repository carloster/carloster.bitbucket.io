/* jshint esversion: 6, asi: true, -W069, -W010, -W033 */
const accionesAgua = 799187835
const millones = 1000000

function rop(volumenTemporada, sumaVolumenEmbalses){

	// volumentemporada = 185.686
	// suma = 220.1

	let evaporacion, recuperacion

	let desmarquePreliminar1
	let desmarquePreliminar2
	let VEP
	let diferencia
	let desmarque
	let volumenFaltante

	sumaVolumenEmbalses = sumaVolumenEmbalses*millones
	volumenTemporada = volumenTemporada * millones

	let desmarqueObjetivo = 0.375
	let volumenDesmarqueObjetivo = 300*millones
	evaporacion = sumaVolumenEmbalses * 0.04


	if (volumenTemporada < (250*millones)) {
		recuperacion = volumenTemporada * 0.2
	} else{
		recuperacion = volumenTemporada * 0.3
	}
	// VEP: volumen embalse proyectado

	// console.log('calculando desmarque...');
	// console.log('volumenTemporada...' + volumenTemporada);
	// console.log('Volumen embalses...' + sumaVolumenEmbalses);

	desmarquePreliminar1 = volumenTemporada/accionesAgua
	// console.log('desmarque preliminar 1: ' + desmarquePreliminar1);
	if (desmarquePreliminar1 > desmarqueObjetivo) {
		// ejemplo: con suma = 200 millones..
		if (sumaVolumenEmbalses < 240*millones) {

			// volumenfaltante = 40 millones
			volumenFaltante = 240*millones - sumaVolumenEmbalses
			// diferencia = 350 millones - 300 millones
			diferencia = volumenTemporada - volumenDesmarqueObjetivo
			// excedente = 50 millones - 40 millones
			excedente = diferencia - volumenFaltante
			if (excedente < 0) {
				desmarquePreliminar2 = 0.375 - 0.035
			} else{

			//  suma = 200 millones + 40 millones
			sumaVolumenEmbalses = sumaVolumenEmbalses + volumenFaltante
			// volumen = 300 millones + 10 millones
			volumenDesmarqueObjetivo = volumenDesmarqueObjetivo + excedente
			// desmarqueprelim2 = 0.3879
			desmarquePreliminar2 = parseFloat(volumenDesmarqueObjetivo / accionesAgua)
			}
		} else {
			desmarquePreliminar2 = desmarquePreliminar1
		}
		VEP = proyeccionTerminoTemporada(volumenTemporada, sumaVolumenEmbalses, desmarquePreliminar2)
		desmarque = desmarquePreliminar2
	} else{
		diferencia = volumenDesmarqueObjetivo - volumenTemporada
		if (sumaVolumenEmbalses > diferencia) {
			desmarquePreliminar2 = desmarqueObjetivo
			VEP = proyeccionTerminoTemporada(volumenTemporada, sumaVolumenEmbalses, desmarquePreliminar2)
			desmarque = desmarquePreliminar2 - 0.035
		} else {
			desmarquePreliminar2 = (volumenTemporada + sumaVolumenEmbalses)/accionesAgua

			if (desmarquePreliminar2 >= 0.35) {
				desmarque = desmarquePreliminar2 - 0.03
			} else {
				if (desmarquePreliminar2 >= 0.3) {
					desmarque = 0.3
				} else {
					desmarque = desmarquePreliminar2
				}
			}
		}
	}
	// console.log(desmarquePreliminar2)
	// console.log('valor desmarque: ' + desmarque)
	// debugger

	return desmarque
}

function proyeccionTerminoTemporada(volumenTemporada, volumenEntrada, desmarque){

	desmarque = desmarque / 100
	volumenTemporada = volumenTemporada * millones
	volumenEntrada = volumenEntrada * millones

	let proyectado
	let recuperacion
	let evaporacion = volumenEntrada * 0.04

	if (volumenTemporada < (250*millones)) {
		recuperacion = 0.2
	} else{
		recuperacion = 0.3
	}

	proyectado = volumenTemporada + volumenEntrada - (desmarque * accionesAgua) - evaporacion + (volumenTemporada * recuperacion)
	proyectado = proyectado / millones
	proyectado = proyectado.toFixed(1)
	if (proyectado > 210) proyectado = 210

	return proyectado
}
