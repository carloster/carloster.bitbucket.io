<?php 


  include 'include/login/session.php';
 ?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <?php 
    $incluye_rop = true;
    $pag_admin = false;
  require_once('include/header.php');
// print_r($_GET);

$valores = $_GET;
unset($valores['enviar']);
unset($valores['precipElqui']);

// $valores = array_values($valores);


$nombres = array(
"precipitacion_laguna" => "Precipitación equivalente acumulada [mm] - Embalse La Laguna",
"altura_nieve_laguna" => "Altura de nieve [cm] - Embalse La Laguna",
"precipitacion_cochiguaz" => "Precipitación acumulada - Estación Cochiguaz (DGA)",
"precipitacion_ortiga" => "Precipitación acumulada - Estación La Ortiga (DGA)",
"volumen_afluente" => "Volumen temporada afluente embalse La Laguna [Millones de m<sup>3</sup>]",
"rea_mayo" => "Caudal medio mensual - Río Elqui en Algarrobal - Mayo",
"rea_agosto" => "Caudal medio mensual - Río Elqui en Algarrobal - Agosto",
"rle_mayo" => "Caudal medio mensual - Río La Laguna en entrada Embalse La Laguna - Mayo",
"rle_agosto" => "Caudal medio mensual - Río La Laguna en entrada Embalse La Laguna - Agosto",
"rtv_mayo" => "Caudal medio mensual - Río Turbio en Varillar - Mayo",
"rtv_agosto" => "Caudal medio mensual - Río Turbio en Varillar  - Agosto",
"rcr_mayo" => "Caudal medio mensual - Río Claro en Rivadavia - Mayo",
"rcr_agosto" => "Caudal medio mensual - Río Claro en Rivadavia - Agosto",
"volumen_laguna" => " Volumen de Embalse al 31 de agosto - La Laguna",
"volumen_puclaro" => " Volumen de Embalse al 31 de agosto - Puclaro"
);

// Array ( 
// [precipitacion_laguna] => 1 
// [altura_nieve_laguna] => 1 
// [precipitacion_cochiguaz] => 3.79 
// [precipitacion_ortiga] => 5.70 
// [precipElqui] => 1 
// [volumen_afluente] => 1 
// [rea_mayo] => 1 
// [rea_agosto] => 1 
// [rle_mayo] => 1 
// [rle_agosto] => 1 
// [rtv_mayo] => 1 
// [rtv_agosto] => 1 
// [rcr_mayo] => 1 
// [rcr_agosto] => 1 
// [volumen_laguna] => 1 
// [volumen_puclaro] => 1 
// [enviar] => enviar 
// ) 




http://localhost/decision_desmarque.php?precipitacion_laguna=1&altura_nieve_laguna=1&precipitacion_cochiguaz=3.79&precipitacion_ortiga=5.70&precipElqui=1&volumen_afluente=1&rea_mayo=1&rea_agosto=1&rle_mayo=1&rle_agosto=1&rtv_mayo=1&rtv_agosto=1&rcr_mayo=1&rcr_agosto=1&volumen_laguna=1&volumen_puclaro=1&enviar=enviar
















// return;
    ?>
    <title>Regla operacional JVRE</title>
  </head>

  <body>
<div id="imprimir">
    

    <?php 
    $inicio = false;
    $boton_volver=true;
    $banner="<span class=''>PROGESHI/Elqui</span> - <span class='px-2'>Resultado</span> <span class='px-2'>de</span> <span class='px-2'>Desmarques</span>";
    require_once('include/banner.php');
    ?>
  <?php foreach ($valores as $key => $val): ?>
        <input type="hidden" class="entrada_valores" name="<?php echo $key ?>" id="<?php echo $key ?>" value="<?php echo $val ?>">
  <?php endforeach; ?>
    <input type="hidden" name ="usuario" id="usuario" value="<?php echo $nombre ?>">

  <script>

function estiloDesmarque(dm){
    let umbral, clase;
    dm = parseFloat(dm);
    clase = 'text-danger';
    umbral = 37.5;
    umbral = dm - umbral;
    if (umbral > 12.5) clase = 'text-success'; else
    if ((umbral > 7.5) && (umbral < 12.5)) clase = 'text-warning';
    return clase;
}

function validarNumero (val) {
    let sujeto = parseInt(val,10)
    if (isNaN(sujeto)){
        return false
    } else if (sujeto === '') {
        return false
    } else {
        return true
    }
}

  $(document).ready(function() {
    let volEmbalses = 0;
    let volumen_afluente = parseFloat($("#volumen_afluente").val());
    let altura_nieve_laguna = parseFloat($("#altura_nieve_laguna").val());
    let precipitacion_laguna = parseFloat($("#precipitacion_laguna").val());
    let precipitacion_cochiguaz = parseFloat($("#precipitacion_cochiguaz").val());
    let precipitacion_ortiga = parseFloat($("#precipitacion_ortiga").val());
    let rea_mayo = parseFloat($("#rea_mayo").val());
    let rea_agosto = parseFloat($("#rea_agosto").val());
    let rcr_mayo = parseFloat($("#rcr_mayo").val());
    let rcr_agosto = parseFloat($("#rcr_agosto").val());
    let rtv_mayo = parseFloat($("#rtv_mayo").val());
    let rtv_agosto = parseFloat($("#rtv_agosto").val());
    let rle_mayo = parseFloat($("#rle_mayo").val());
    let rle_agosto = parseFloat($("#rle_agosto").val());
    let volumen_laguna = parseFloat($("#volumen_laguna").val());
    let arrayValores = new Array()
    let arrayNombres = new Array()
    let flagAbort = false
    let volumen_puclaro = parseFloat($("#volumen_puclaro").val());
    let temporada = $("#temporada").val();


    // volEmbalses = volumen_laguna + volumen_puclaro;
    $(".span_temporada").html(temporada);

    let temporada_1 = temporada.split("/");

    let anio1_temporada_1 = parseInt(temporada_1[0]);
    let anio2_temporada_1 = parseInt(temporada_1[1]);
    let anio1_temporada_2 = parseInt(temporada_1[0]);
    let anio2_temporada_2 = parseInt(temporada_1[1]);

    anio1_temporada_1 = anio1_temporada_1 + 1
    anio2_temporada_1 = anio2_temporada_1 + 1

    anio1_temporada_2 = anio1_temporada_1 + 1
    anio2_temporada_2 = anio2_temporada_1 + 1

    anio1_temporada_1 = anio1_temporada_1.toString();
    anio2_temporada_1 = anio2_temporada_1.toString();

    anio1_temporada_2 = anio1_temporada_2.toString();
    anio2_temporada_2 = anio2_temporada_2.toString();

    let str_temporada_1 = anio1_temporada_1 + "/" + anio2_temporada_1;
    let str_temporada_2 = anio1_temporada_2 + "/" + anio2_temporada_2;

    $(".span_temporada_1").html(str_temporada_1);
    $(".span_temporada_2").html(str_temporada_2);



    $('.entrada_valores').each(function(element,index, array){
        let esLegal = validarNumero($(this).val())
        if (esLegal == false) {
          alert("por favor, ingrese un valor numérico positivo en:\n " + $(this).attr('data-fullname') )
          flagAbort = true
        }
        arrayNombres.push($(this).prop('id'))
            if ($(this).val().length > 0) {arrayValores.push($(this).val())}
        })

        if (flagAbort === true) arrayValores.length = 0

        if ($('#volumen_laguna').val().length > 0) {
          volEmbalses += parseFloat($('#volumen_laguna').val())
        } else {
          console.log('aaah error, falta volumen de embalse la laguna')
          return
        }

        if ($('#volumen_puclaro').val().length > 0) {
          volEmbalses += parseFloat($('#volumen_puclaro').val())
        } else{
          console.log('aaah error, falta volumen de embalse puclaro')
          return
        }

        if ( volEmbalses < 0) {
            console.log('aaah error, no hay volumen de embalses')
            return
        } else{
            if (arrayValores.length <= 2) {
                console.log('aaah error, no estan todos los datos ingresados')
                return
        } else{
            volumen_t = main(volumen_afluente, altura_nieve_laguna, precipitacion_laguna, precipitacion_cochiguaz, precipitacion_ortiga, rtv_agosto, rle_mayo, rea_agosto, rea_mayo, rcr_agosto, rle_agosto, rcr_mayo, rtv_mayo)

            volumen_t1 = parseFloat(volumen_t['Vt1'])
            volumen_t2 = parseFloat(volumen_t['Vt2'])
            volumen_t3 = parseFloat(volumen_t['Vt3'])
            volumen_t4 = parseFloat(volumen_t['Vt4'])

            let volumen_td_startup = volumen_t1 + volumen_t2 + volumen_t3 + volumen_t4 // ¬_¬
            volumen_td_startup = parseFloat(volumen_td_startup / 4).toFixed(1)

            $("#volumen_td").val(volumen_td_startup)


            let desmarque_t1_raw = (rop(volumen_t1, volEmbalses) * 100).toFixed(1)
            let desmarque_t2_raw = (rop(volumen_t2, volEmbalses) * 100).toFixed(1)
            let desmarque_t3_raw = (rop(volumen_t3, volEmbalses) * 100).toFixed(1)
            let desmarque_t4_raw = (rop(volumen_t4, volEmbalses) * 100).toFixed(1)

            let desmarque_t1 = desmarque_t1_raw.toString() + '%'
            let desmarque_t2 = desmarque_t2_raw.toString() + '%'
            let desmarque_t3 = desmarque_t3_raw.toString() + '%'
            let desmarque_t4 = desmarque_t4_raw.toString() + '%'

            $('#volumen_t1').html(volumen_t1)
            $('#volumen_t2').html(volumen_t2)
            $('#volumen_t3').html(volumen_t3)
            $('#volumen_t4').html(volumen_t4)

            $("div>span").removeClass('text-danger')
            $("div>span").removeClass('text-warning')
            $("div>span").removeClass('text-success')

            let clasedesmarque_t1 = estiloDesmarque(desmarque_t1)
            let clasedesmarque_t2 = estiloDesmarque(desmarque_t2)
            let clasedesmarque_t3 = estiloDesmarque(desmarque_t3)
            let clasedesmarque_t4 = estiloDesmarque(desmarque_t4)

            $('.dato_desmarque_t1').html(desmarque_t1).addClass(clasedesmarque_t1)
            $('.dato_desmarque_t2').html(desmarque_t2).addClass(clasedesmarque_t2)
            $('.dato_desmarque_t3').html(desmarque_t3).addClass(clasedesmarque_t3)
            $('.dato_desmarque_t4').html(desmarque_t4).addClass(clasedesmarque_t4)

            let vol_puclaro_t1 = parseFloat(proyeccionTerminoTemporada(volumen_t1, volumen_puclaro, desmarque_t1_raw))
            let vol_puclaro_t2 = parseFloat(proyeccionTerminoTemporada(volumen_t2, volumen_puclaro, desmarque_t2_raw))
            let vol_puclaro_t3 = parseFloat(proyeccionTerminoTemporada(volumen_t3, volumen_puclaro, desmarque_t3_raw))
            let vol_puclaro_t4 = parseFloat(proyeccionTerminoTemporada(volumen_t4, volumen_puclaro, desmarque_t4_raw))

            $('#vol_puclaro_t1').html(vol_puclaro_t1)
            $('#vol_puclaro_t2').html(vol_puclaro_t2)
            $('#vol_puclaro_t3').html(vol_puclaro_t3)
            $('#vol_puclaro_t4').html(vol_puclaro_t4)

            $("#desmarque_td").val(30)
            cambiarVpDec()
            cambiarVpDec1();
        }
    }
});

function chequearDm(){

    let volumen_td1 = parseFloat($("#volumen_td1").val());
    let desmarque_td1 = parseFloat($("#desmarque_td1").val());
    if (desmarque_td1 !== NaN) {
        $("#desmarque_td1").prop('disabled', false);
    } else{
        $("#desmarque_td1").prop('disabled', true);
    }
    if (volumen_td1 !== NaN) {
        $("#volumen_td1").prop('disabled', false);
    } else{
        $("#volumen_td1").prop('disabled', true);
    }
}

$(document).ready(function() {

    $("*").on('input',function(){
        chequearDm()
    });

    $("#sinColores").change(function() {
        event.preventDefault();
        if ($(this).is(':checked')) {
            $(".caja").addClass('transparente')
        } else{
            $(".caja").removeClass('transparente');
        }
        /* Act on the event */
    });

    // $('input[type=number]').on('input propertychange', function(e) {
    //     console.warn('ola');
    //     chequearDm()
    // })

    $("#guardar").on('click', function() {
        event.preventDefault();
        let usuario = $("#usuario").val()

        let volumen_afluente = parseFloat($("#volumen_afluente").val());
        let altura_nieve_laguna = parseFloat($("#altura_nieve_laguna").val());
        let precipitacion_laguna = parseFloat($("#precipitacion_laguna").val());
        let precipitacion_cochiguaz = parseFloat($("#precipitacion_cochiguaz").val());
        let precipitacion_ortiga = parseFloat($("#precipitacion_ortiga").val());
        let rea_mayo = parseFloat($("#rea_mayo").val());
        let rea_agosto = parseFloat($("#rea_agosto").val());
        let rcr_mayo = parseFloat($("#rcr_mayo").val());
        let rcr_agosto = parseFloat($("#rcr_agosto").val());
        let rtv_mayo = parseFloat($("#rtv_mayo").val());
        let rtv_agosto = parseFloat($("#rtv_agosto").val());
        let rle_mayo = parseFloat($("#rle_mayo").val());
        let rle_agosto = parseFloat($("#rle_agosto").val());
        let volumen_laguna = parseFloat($("#volumen_laguna").val());
        let volumen_puclaro = parseFloat($("#volumen_puclaro").val());
        let temporada = $("#temporada").val();
        let volumen_td = parseFloat($("#volumen_td").val());
        let desmarque_td = parseFloat($("#desmarque_td").val());

        let volumen_t1 = parseFloat($("#volumen_t1").html());
        let volumen_t2 = parseFloat($("#volumen_t2").html());
        let volumen_t3 = parseFloat($("#volumen_t3").html());
        let volumen_t4 = parseFloat($("#volumen_t4").html());
        let vol_puclaro_td = parseFloat($("#vol_puclaro_td").html());
        let vol_puclaro_t1 = parseFloat($("#vol_puclaro_t1").html());
        let vol_puclaro_t2 = parseFloat($("#vol_puclaro_t2").html());
        let vol_puclaro_t3 = parseFloat($("#vol_puclaro_t3").html());
        let vol_puclaro_t4 = parseFloat($("#vol_puclaro_t4").html());
        let desmarque_t1 = ($("#desmarque_t1").html());
        let desmarque_t2 = ($("#desmarque_t2").html());
        let desmarque_t3 = ($("#desmarque_t3").html());
        let desmarque_t4 = ($("#desmarque_t4").html());

        desmarque_t1 = parseFloat(desmarque_t1.split('%')[0])
        desmarque_t2 = parseFloat(desmarque_t2.split('%')[0])
        desmarque_t3 = parseFloat(desmarque_t3.split('%')[0])
        desmarque_t4 = parseFloat(desmarque_t4.split('%')[0])



        $.get('include/subir_desmarque.php', { usuario : usuario, volumen_afluente : volumen_afluente, altura_nieve_laguna : altura_nieve_laguna, precipitacion_laguna : precipitacion_laguna, precipitacion_cochiguaz : precipitacion_cochiguaz, precipitacion_ortiga : precipitacion_ortiga, rea_mayo : rea_mayo, rea_agosto : rea_agosto, rcr_mayo : rcr_mayo, rcr_agosto : rcr_agosto, rtv_mayo : rtv_mayo, rtv_agosto : rtv_agosto, rle_mayo : rle_mayo, rle_agosto : rle_agosto, volumen_laguna : volumen_laguna, volumen_puclaro : volumen_puclaro, temporada : temporada, volumen_t1 : volumen_t1, volumen_t2 : volumen_t2, volumen_t3 : volumen_t3, volumen_t4 : volumen_t4, volumen_td : volumen_td, desmarque_t1 : desmarque_t1, desmarque_t2 : desmarque_t2, desmarque_t3 : desmarque_t3, desmarque_t4 : desmarque_t4, desmarque_td : desmarque_td, vol_puclaro_t1 : vol_puclaro_t1, vol_puclaro_t2 : vol_puclaro_t2, vol_puclaro_t3 : vol_puclaro_t3, vol_puclaro_t4 : vol_puclaro_t4, vol_puclaro_td : vol_puclaro_td }, function(data){
            console.log(data);
        });

    });

    $('#cmd').click(function () {
        $('.btn-inicio').css('display', 'none',' important');

        let pdf = new jsPDF('l', 'pt',[1920,928]);
        let canvas = pdf.canvas;
        pdf.html(document.getElementById('imprimir'), {
            callback: function (pdf) {
                pdf.output('dataurlnewwindow');
            }
        });
        $('.btn-inicio').css('display', 'inline-block', 'important');

    });

    $("input[type=number]").on('change', function() {
        event.preventDefault();
        cambiarVpDec()
        cambiarVpDec1()
        /* Act on the event */
    });


});

function cambiarVpDec(){
    let volumen_td = parseFloat($("#volumen_td").val());
    let desmarque_td = parseFloat($("#desmarque_td").val());
    let volumen_puclaro = parseFloat($("#volumen_puclaro").val());
    let evaporacion = 0.04
    let recuperacion

    if (volumen_td < (250000000)) {
        recuperacion = 0.2
    } else{
        recuperacion = 0.3
    }

    if (volumen_td !== '') {


        if(((volumen_td+volumen_puclaro) - (volumen_puclaro*evaporacion)-(desmarque_td/100*799.187835)) + (volumen_td * (recuperacion))>0){
            vol_puclaro_td = ((volumen_td+volumen_puclaro) - (volumen_puclaro*evaporacion)-(desmarque_td/100*799.187835)) + (volumen_td * (recuperacion))
        } else{
            vol_puclaro_td = 0
        }

        if (vol_puclaro_td > 208) {
            vol_puclaro_td = 208
        }
        $('#vol_puclaro_td').html(vol_puclaro_td.toFixed(1))
        let volumen_td1 = parseFloat($("#volumen_td1").val())
        let desmarque_td1 = rop(volumen_td1,volumen_puclaro)
    }
}

function cambiarVpDec1(){
    let volumen_td1 = parseFloat($("#volumen_td1").val())
    let desmarque_td = parseFloat($("#desmarque_td").val())
    let desmarque_td1 = parseFloat($("#desmarque_td1").val())
   let  vol_puclaro_td = parseFloat($("#vol_puclaro_td").html())
    if (volumen_td !== '' && volumen_td1 !== '' && desmarque_td !== '' && desmarque_td1 !== '') {
    let evaporacion = 0.04
        let recuperacion

        if (volumen_td1 < (250000000)) {
            recuperacion = 0.2
        } else{
            recuperacion = 0.3
        }

        let vol_puclaro_td1 = volumen_td1 + vol_puclaro_td - vol_puclaro_td * evaporacion - (desmarque_td1/100)*799.187835 + volumen_td1 * recuperacion

        if(((volumen_td1 + vol_puclaro_td) - (vol_puclaro_td*evaporacion)-(desmarque_td1/100*799.187835)) + (volumen_td1 * (recuperacion))>0){
            vol_puclaro_td1 = volumen_td1 + vol_puclaro_td - vol_puclaro_td * evaporacion - (desmarque_td1/100)*799.187835 + volumen_td1 * recuperacion
        } else{
            vol_puclaro_td1 = 0
        }

        if (vol_puclaro_td1 > 208) {
            vol_puclaro_td1 = 208
        }

        $('#vol_puclaro_td1').html(vol_puclaro_td1.toFixed(1))
    }
}

// #ffbb33 amarillo
// #4285f4 azul
// #00c851 verde
// #00bcd4 celeste
</script>

<style type="text/css">
    .caja{
        border-radius: 2%;
        box-shadow: 4px 4px #777;
    }

    .transparente{
        background-color: transparent !important;
        border: 1px solid black;
    }

    .inputDm{
        color:#17555f;
        text-shadow: 1px 1px
        #000;
        background:
        transparent;
        border: 1px solid
        black;
        border-radius: 4%;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-8">
            <div class="row">
                <div class="col text-center">
                    <h2><span class="">Volumen </span><span class="">proyectado </span><br><span class="">Temporada </span><span class=" span_temporada">2019/20</span></h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col text-center caja bg-warning p-3 m-1"> <div class="a1"><span class="">Modelo</span> <span class="">Río</span> <span class="">La</span> <span class="">Laguna</span><br>
                    <span class="highlight-paleta"><span class="dato_volumen_t1" id="volumen_t1"></span> <b>[Mm<sup>3</sup>]</b></span>
                </div></div>
                <div class="col text-center caja bg-primary p-3 m-1"> <div class="a2"><span class="">Arbol</span> <span class="">Meteorológico</span><br><br>
                    <span class="highlight-paleta"><span class="dato_volumen_t2" id="volumen_t2"></span> <b>[Mm<sup>3</sup>]</b></span>
                </div></div>
                <div class="col text-center caja bg-success p-3 m-1"> <div class="a3"><span class="">Arbol</span> <span class="">Hidrológico</span><br><br>
                    <span class="highlight-paleta"><span class="dato_volumen_t3" id="volumen_t3"></span> <b>[Mm<sup>3</sup>]</b></span>
                </div></div>
                <div class="col text-center caja bg-info p-3 m-1"> <div class="a4"><span class="">Arbol</span> <span class="">Hidrometeorológico</span><br><br>
                    <span class="highlight-paleta"><span class="dato_volumen_t4" id="volumen_t4"></span> <b>[Mm<sup>3</sup>]</b></span>
                </div></div>
                <div class="col text-center caja caja5 p-3 m-1 deep-purple accent-1"><span class="vol_decision ">Decisión</span><br><br>
                    <span class="highlight-paleta"><span class="dato_volumen_td ad"><input type="number" class="inputDm" lang="en" step="0.1" name="volumen_td" id="volumen_td"></span> <b>[Mm<sup>3</sup>]</b></span></div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <hr>
                    <h2><span class="">Desmarque</span> <span class="">propuesto</span><br><span class="">Temporada</span> <span class="span_temporada ">2019/20</span></h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col text-center caja p-3 m-1 bg-warning"><br><span class="resultados"><span class="dato_desmarque_t1" id="desmarque_t1"></span></span><br></div>
                <div class="col text-center caja p-3 m-1 bg-primary"><br><span class="resultados"><span class="dato_desmarque_t2" id="desmarque_t2"></span></span></div>
                <div class="col text-center caja p-3 m-1 bg-success"><br><span class="resultados"><span class="dato_desmarque_t3" id="desmarque_t3"></span></span></div>
                <div class="col text-center caja p-3 m-1 bg-info"><br><span class="resultados"><span class="dato_desmarque_t4" id="desmarque_t4"></span></span></div>
                <div class="col text-center caja p-3 m-1 deep-purple accent-1"><br><span class="resultados"><span class=" dato_desmarque_td ad"><input type="number" class="inputDm" lang="en" step="0.1" value="30" name="desmarque_td" id="desmarque_td"></span></span><span style="font-size: 28px">%</span></div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <hr>
                    <h2><span class="">Volumen </span><span class="">Embalse </span><span class="">Puclaro </span><span class="">proyectado <br>para el inicio de la </span><span class="">Temporada </span><span class="span_temporada_1 ">2019/20</span></h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col text-center caja p-3 m-1 bg-warning"><span class="highlight-paleta"><span class="dato_vp1" id="vol_puclaro_t1"></span> <b>[Mm<sup>3</sup>]</b></span></div>
                <div class="col text-center caja p-3 m-1 bg-primary"><span class="highlight-paleta"><span class="dato_vp2" id="vol_puclaro_t2"></span> <b>[Mm<sup>3</sup>]</b></span></div>
                <div class="col text-center caja p-3 m-1 bg-success"><span class="highlight-paleta"><span class="dato_vp3" id="vol_puclaro_t3"></span> <b>[Mm<sup>3</sup>]</b></span></div>
                <div class="col text-center caja p-3 m-1 bg-info"><span class="highlight-paleta"><span class="dato_vp4" id="vol_puclaro_t4"></span> <b>[Mm<sup>3</sup>]</b></span></div>
                <div class="col text-center caja p-3 m-1 deep-purple accent-1"><span class="highlight-paleta"><span class="dato_vol_puclaro_td ad" id="vol_puclaro_td"></span> <b>[Mm<sup>3</sup>]</b></span></div>
            </div>
        </div>
        <div class="col-4">
            <div class="row">
                <div class="col text-center">
                    <h2><span class="">Volumen</span> <span class="">proyectado</span><br><span class="">Temporada</span> <span class="span_temporada_1 ">2020/21</span></h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-3"></div>
                <div class="col text-center caja m-1 p-3 deep-orange accent-1"><span class="vol_decision">Decisión</span><br><br><span class="highlight-paleta"><span class="dato_volumen_td1"><input type="number" class="inputDm" lang="en" step="0.1" name="volumen_td1" id="volumen_td1" value="144" disabled></span> <b>[Mm<sup>3</sup>]</b></span></div>
                <div class="col-3"></div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <hr>
                    <h2><span class="">Desmarque</span> <span class="">planteado</span><br><span class="">Temporada</span>  <span class="span_temporada_1 ">2020/21</span></h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-3"></div>
                <div class="col text-center caja m-1 p-3 deep-orange accent-1"><br><span class="resultados"><span class="dato_desmarque_td1"><input type="number" class="inputDm" lang="en" step="0.1" name="desmarque_td1" id="desmarque_td1" disabled></span></span><span style="font-size: 28px">%</span></div>
                <div class="col-3"></div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <hr>
                    <h2><span class="">Volumen</span> <span class="">Embalse</span> <span class="">Puclaro</span><br><span class="">Inicio temporada</span><span class="span_temporada_2 ">2020/21</span></h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-3"></div>
                <div class="col text-center caja p-3 m-1 deep-orange accent-1"><span class="highlight-paleta"><span class="dato_vol_puclaro_td1" id="vol_puclaro_td1"></span> <b>[Mm<sup>3</sup>]</b></span></div>
                <div class="col-3"></div>
            </div>
            </div>
        </div>
  
<br><br>
<div class="row">
    <div class="col-3">
        <button type="button" class="btn btn-cien btn-paleta btn-block subgrafico" id="cmd">Generar PDF</button>
    </div>
    <div class="col-3">
        <button type="button" class="btn btn-cien btn-paleta btn-block subgrafico" id="guardar">Guardar en base de datos</button>
    </div>
    <div class="col-2"><input type="checkbox" id="sinColores"> Quitar colores?</div>
    <div class="col-4"></div>
</div>
  </div>
</div>

  </body>
  <style type="text/css">
      
  .resultados{
    font-size: 2rem;
    border: 1px solid #17555f;
    padding: 5px;
    margin-top: 4px;
    border-radius: 8px;
  }
  input[type=number]{
    width: 100px;
    height: 45px;
    border: none;
    border-color: transparent;
    padding: 0;

  }
  </style>
</html>